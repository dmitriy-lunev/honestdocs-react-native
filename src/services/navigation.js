import React from 'react';

export const isReadyRef = React.createRef(false);

export const navigationRef = React.createRef(null);

export function navigate(name, params) {
  if (isReadyRef.current && navigationRef) {
    navigationRef.current.navigate(name, params);
  } else {
    // You can decide what to do if the app hasn't mounted
    // You can ignore this, or add these actions to a queue you can call later
  }
}
