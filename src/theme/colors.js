const colors = {
  //main
  mainText: '#2d2d2d',
  basicText: '#757575',
  primaryText: '#303030',
  greyText: '#666666',
  blue: '#0FACEB',

  //black
  emptyColor: 'rgba(117, 117, 117, .25)',
  basicTextOpacity: 'rgba(117, 117, 117, .7)',
  secondaryTextOpacity: 'rgba(117, 117, 117, .8)',
  basicBgOpacity: 'rgba(117, 117, 117, .59)',
  primaryColorOpacity: 'rgba(48, 48, 48, .8)',
  dark: '#333',
  darkOpacity: 'rgba(0,0,0,0.7)',
  black: '#000',
  blackOpacity: 'rgba(0,0,0, .25)',
  lightGrey: '#575757',
  tintColor: 'grey',
  lightDark: 'rgba(0,0,0,.1)',
  silver: '#BFBFBF',
  silverSandColor: '#c1c4c7',
  concrete: '#F2F2F2',
  greyOpacity: 'rgba(216, 216, 216, .25)',
  extraLightGrey: '#BCBCBC',
  slategrey: '#8e8e8e',
  dimGray: '#959595',
  mainTextOpacity: 'rgba(45, 45, 45, .8)',
  mainTextLightOpacity: 'rgba(45, 45, 45, .25)',
  activeBorderColor: 'rgba(0, 175, 145, 0.1)',
  ultraGreyColor: '#525252',
  ultraGreyColorOpacity: 'rgba(82,82,82, .8)',
  superGrey: '#E1E1E1',
  layoutGreyColor: '#f4f8f9',
  grayColor: '#919191',
  textGreyColor: '#79787F',
  shuttleGreyColor: '#5a6266',
  pearlgray: '#A7A6AC',
  boulderGreyColor: '#777777',
  alabasterGreyColor: '#F8F7F7',

  // green
  lightgreen: 'rgba(0, 175, 145, 0.3)',
  extraLightGreen: '#c0ebe3',
  darkgreen: '#09846E',
  extraDarkGreen: '#007864',
  green: '#00AF91',
  extraGreen: '#008C74',
  greenColorOpacity: 'rgba(33,186,160,0.1)',
  greenColorLightOpacity: 'rgba(0, 175, 145, .8)',
  loaderColor: 'rgba(255,255,255,0.6)',
  rollerColor: '#4addba',
  cupColor: 'rgba(0, 120, 100, .5)',
  cupBorderColor: '#00937a',
  cupLayoutColor: 'rgba(0,0,0,0.4)',
  disableGreen: '#EBFAF7',
  pineGreen: '#00836D',
  greyBgColor: '#F7F7FA',
  tabColor: '#b2e6dd',
  seaGreenColor: '#31a14c',

  //blue
  darkBlue: '#10A3E8',
  lightBlue: 'rgba(16, 163, 232, .1)',
  extraLightBlue: '#daf1fb',
  bluesky: '#687784',
  bluewater: 'rgb(16, 163, 232)',
  cornflowerblue: '#4AA0E2',

  // other
  transparent: 'rgba(0,0,0,0)',
  grey: '#ccc',
  white: '#fff',
  whiteOpacity: 'rgba(255, 255, 255, .7)',
  whiteLightColor: '#fafafa',
  yellow: '#ffc659',
  lightYellow: '#FFF1D7',
  disableRed: '#ffe5e5',
  red: '#EF3B1E',

  //error
  invalidColor: 'firebrick',
  failureColor: '#EF8585',
  errorColor: '#ff0000',

  //shadow
  shadowColorLightTheme: 'rgba(0, 0, 0, .18)',
  shadowColorDarkTheme: 'rgba(0, 0, 0, .75)',
  tabShadowColorLightTheme: 'rgba(0, 120, 100, .1)',
  cupShadowColor: 'rgba(0, 120, 100, 1)',
  healthCoinsShadowColor: 'rgba(0, 120, 100, 0.55)',

  //separator
  separatorColorWhiteTheme: 'rgba(122, 122, 122, .05)',
  separatorColorDarkTheme: 'rgba(255, 255, 255, .05)',

  // placeholder
  placeholderColorWhiteTheme: 'rgba(117,117, 117, .8)',
  placeholderColorDarkTheme: 'rgba(255, 255, 255, .6)',

  //border
  borderColor: 'rgba(122, 122, 122, .1)',
  navbarBorder: '#dfdfdf',

  //doctorStatus
  doctorStatusLight: 'rgba(82,82, 82, .8)',
  doctorStatusDark: 'rgba(255, 255, 255, .8)',

  //slider
  sliderBgColor: 'rgba(122,122,122, 0.05)',

  //priceIcon
  priceIconBg: '#eee',

  //cart footer
  cartFooterBg: 'rgba(250, 250, 250, .9)',
};
export default colors;
