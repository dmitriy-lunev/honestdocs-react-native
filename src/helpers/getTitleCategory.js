const categories = {
  shop_health: 'Health Checkup',
  shop_dental: 'Dental',
  shop_beauty: 'Beauty',
};

export const getTitleCategory = (category) => categories[category];
