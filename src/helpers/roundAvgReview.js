export const roundAvgReview = (mark) => {
  const middleMark = Math.floor(mark) + 0.5;

  if (mark < middleMark - 0.25) return middleMark - 0.5;
  else if (mark >= middleMark + 0.25) return middleMark + 0.5;
  else return middleMark;
};
