export const calculateTimeAgo = (day) => {
  let delay = new Date().getTime() - new Date(day).getTime();

  const hours = Math.floor(delay / (1000 * 60 * 60));
  const days = Math.floor(delay / (1000 * 60 * 60 * 24));
  const weeks = Math.floor(delay / (1000 * 60 * 60 * 24 * 7));
  const months = Math.floor(delay / (1000 * 60 * 60 * 24 * 30));

  if (hours === 1) {
    return `1 hour ago`;
  }
  if (hours < 24) {
    return `${hours} hours ago`;
  }

  if (days === 1) {
    return `1 day ago`;
  }
  if (days < 7) {
    return `${days} days ago`;
  }

  if (weeks === 1) {
    return `1 week ago`;
  }
  if (weeks < 4) {
    return `${delay} weeks ago`;
  }

  if (months === 1) {
    return `1 month ago`;
  }

  return `${months} months ago`;
};
