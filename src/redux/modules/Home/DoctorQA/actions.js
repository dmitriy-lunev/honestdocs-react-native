import homeAPI from '../../../../api/home-api';
import { ACTION_TYPES } from './doctorQA';

export const clearDoctorQA = () => ({
  type: ACTION_TYPES.CLEAR_DOCTORQA,
});

function loadHomepageDoctorQA() {
  return {
    type: ACTION_TYPES.LOAD_HOMEPAGE_DOCTOR_QA,
  };
}

function loadHomepageDoctorQASuccess(homepageDoctorQA) {
  return {
    type: ACTION_TYPES.LOAD_HOMEPAGE_DOCTOR_QA_SUCCESS,
    payload: { homepageDoctorQA },
  };
}

function loadHomepageDoctorQAFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_HOMEPAGE_DOCTOR_QA_FAILURE,
    payload: { error },
  };
}

export function loadHomepageDoctorQAFetch() {
  return (dispatch) => {
    dispatch(loadHomepageDoctorQA());
    return homeAPI
      .getHomepageDoctorQA()
      .then((homepageDoctorQA) => {
        const data = homepageDoctorQA.data.data.map((item) => {
          const {
            id,
            attributes: { url, title, answer },
          } = item;
          return { id, url, title, answer };
        });
        dispatch(loadHomepageDoctorQASuccess(data));
      })
      .catch((error) => {
        dispatch(loadHomepageDoctorQAFailure(error));
      });
  };
}
