const STATE_KEY = 'home/doctorQA';

export const ACTION_TYPES = {
  CLEAR_DOCTORQA: `${STATE_KEY}/CLEAR_DOCTORQA`,

  LOAD_HOMEPAGE_DOCTOR_QA: `${STATE_KEY}/LOAD_HOMEPAGE_DOCTOR_QA`,
  LOAD_HOMEPAGE_DOCTOR_QA_SUCCESS: `${STATE_KEY}/LOAD_HOMEPAGE_DOCTOR_QA_SUCCESS`,
  LOAD_HOMEPAGE_DOCTOR_QA_FAILURE: `${STATE_KEY}/LOAD_HOMEPAGE_DOCTOR_QA_FAILURE`,
};

const initialState = {
  homepageDoctorQA: [],
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR_DOCTORQA:
      return initialState;
    case ACTION_TYPES.LOAD_HOMEPAGE_DOCTOR_QA:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_HOMEPAGE_DOCTOR_QA_SUCCESS:
      return {
        ...state,
        homepageDoctorQA: action.payload.homepageDoctorQA,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_HOMEPAGE_DOCTOR_QA_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    default:
      return state;
  }
}
