const STATE_KEY = 'home/homepage';

export const ACTION_TYPES = {
  CLEAR_HOMEPAGE: `${STATE_KEY}/CLEAR_HOMEPAGE`,

  LOAD_BANNERS: `${STATE_KEY}/LOAD_BANNERS`,
  LOAD_BANNERS_SUCCESS: `${STATE_KEY}/LOAD_BANNERS_SUCCESS`,
  LOAD_BANNERS_FAILURE: `${STATE_KEY}/LOAD_BANNERS_FAILURE`,

  LOAD_SHOP_CATEGORIES: `${STATE_KEY}/LOAD_SHOP_CATEGORIES`,
  LOAD_SHOP_CATEGORIES_SUCCESS: `${STATE_KEY}/LOAD_SHOP_CATEGORIES_SUCCESS`,
  LOAD_SHOP_CATEGORIES_FAILURE: `${STATE_KEY}/LOAD_SHOP_CATEGORIES_FAILURE`,
};

const initialState = {
  banners: [],
  shopCategories: [],
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR_HOMEPAGE:
      return initialState;
    case ACTION_TYPES.LOAD_BANNERS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_BANNERS_SUCCESS:
      return {
        ...state,
        banners: action.payload.banners,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_BANNERS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_SHOP_CATEGORIES:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_SHOP_CATEGORIES_SUCCESS:
      return {
        ...state,
        shopCategories: action.payload.shopCategories,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_SHOP_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    default:
      return state;
  }
}
