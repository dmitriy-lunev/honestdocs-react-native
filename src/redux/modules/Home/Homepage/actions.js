import homeAPI from '../../../../api/home-api';
import { getTitleCategory } from '../../../../helpers/getTitleCategory';
import { ACTION_TYPES } from './homepage';

export const clearHomepage = () => ({
  type: ACTION_TYPES.CLEAR_HOMEPAGE,
});

function loadBanners() {
  return {
    type: ACTION_TYPES.LOAD_BANNERS,
  };
}

function loadBannersSuccess(banners) {
  return {
    type: ACTION_TYPES.LOAD_BANNERS_SUCCESS,
    payload: { banners },
  };
}

function loadBannersFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_BANNERS_FAILURE,
    payload: { error },
  };
}

export function loadBannersFetch() {
  return (dispatch) => {
    dispatch(loadBanners());
    return homeAPI
      .getBanners()
      .then((banners) => {
        const data = banners.data.data.map(
          (item) => item.attributes.image,
        );
        dispatch(loadBannersSuccess(data));
      })
      .catch((error) => {
        dispatch(loadBannersFailure(error));
      });
  };
}

function loadShopCategories() {
  return {
    type: ACTION_TYPES.LOAD_SHOP_CATEGORIES,
  };
}

function loadShopCategoriesSuccess(shopCategories) {
  return {
    type: ACTION_TYPES.LOAD_SHOP_CATEGORIES_SUCCESS,
    payload: { shopCategories },
  };
}

function loadShopCategoriesFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_SHOP_CATEGORIES_FAILURE,
    payload: { error },
  };
}

export function loadShopCategoriesFetch() {
  return (dispatch) => {
    dispatch(loadShopCategories());
    return homeAPI
      .getShopCategories()
      .then((shopCategories) => {
        const data = shopCategories.data.data.map(
          ({ attributes: { page_type, i18n_key, image, category } }) => {
            return {
              title: getTitleCategory(category),
              pageType: page_type,
              i18nKey: i18n_key,
              image: 'https://staging.honestdocs.co' + image,
            };
          },
        );
        dispatch(loadShopCategoriesSuccess(data));
      })
      .catch((error) => {
        dispatch(loadShopCategoriesFailure(error));
      });
  };
}
