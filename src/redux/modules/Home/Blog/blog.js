const STATE_KEY = 'home/blog';

export const ACTION_TYPES = {
  CLEAR_BLOG: `${STATE_KEY}/CLEAR_BLOG`,

  CLEAR_BLOG_ARTICLES: `${STATE_KEY}/CLEAR_BLOG_ARTICLES`,

  LOAD_TOP_ARTICLES: `${STATE_KEY}/LOAD_TOP_ARTICLES`,
  LOAD_TOP_ARTICLES_SUCCESS: `${STATE_KEY}/LOAD_TOP_ARTICLES_SUCCESS`,
  LOAD_TOP_ARTICLES_FAILURE: `${STATE_KEY}/LOAD_TOP_ARTICLES_FAILURE`,

  LOAD_BLOG_ARTICLES: `${STATE_KEY}/LOAD_BLOG_ARTICLES`,
  LOAD_BLOG_ARTICLES_SUCCESS: `${STATE_KEY}/LOAD_BLOG_ARTICLES_SUCCESS`,
  LOAD_BLOG_ARTICLES_FAILURE: `${STATE_KEY}/LOAD_BLOG_ARTICLES_FAILURE`,

  LOAD_BLOG_ARTICLES_BY_TAG: `${STATE_KEY}/LOAD_BLOG_ARTICLES_BY_TAG`,

  LOAD_BLOG_ARTICLES_TAGS: `${STATE_KEY}/LOAD_BLOG_ARTICLES_TAGS`,
  LOAD_BLOG_ARTICLES_TAGS_SUCCESS: `${STATE_KEY}/LOAD_BLOG_ARTICLES_TAGS_SUCCESS`,
  LOAD_BLOG_ARTICLES_TAGS_FAILURE: `${STATE_KEY}/LOAD_BLOG_ARTICLES_TAGS_FAILURE`,

  SET_FOLLOWING_TAGS: `${STATE_KEY}/SET_FOLLOWING_TAGS`,

  LOAD_ARTICLE: `${STATE_KEY}/LOAD_ARTICLE`,
  LOAD_ARTICLE_SUCCESS: `${STATE_KEY}/LOAD_ARTICLE_SUCCESS`,
  LOAD_ARTICLE_FAILURE: `${STATE_KEY}/LOAD_ARTICLE_FAILURE`,

  LOAD_ARTICLE_COMMENTS: `${STATE_KEY}/LOAD_ARTICLE_COMMENTS`,
  LOAD_ARTICLE_COMMENTS_SUCCESS: `${STATE_KEY}/LOAD_ARTICLE_COMMENTS_SUCCESS`,
  LOAD_ARTICLE_COMMENTS_FAILURE: `${STATE_KEY}/LOAD_ARTICLE_COMMENTS_FAILURE`,
};

const initialState = {
  topArticles: [],
  blogArticles: [],
  page: 1,
  allLoaded: false,
  articlesTags: {},
  followingTagsId: ['40', '27', '39'],
  article: {},
  comments: [],
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.SET_FOLLOWING_TAGS:
      return {
        ...state,
        followingTagsId: action.payload.followingTagsId,
      };
    case ACTION_TYPES.CLEAR_BLOG:
      return initialState;
    case ACTION_TYPES.CLEAR_BLOG_ARTICLES:
      return {
        ...state,
        loading: false,
        loaded: false,
        blogArticles: [],
        page: 1,
        allLoaded: false,
      };
    case ACTION_TYPES.LOAD_TOP_ARTICLES:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_TOP_ARTICLES_SUCCESS:
      return {
        ...state,
        topArticles: action.payload.topArticles,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_TOP_ARTICLES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_BLOG_ARTICLES:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_BLOG_ARTICLES_SUCCESS:
      return {
        ...state,
        blogArticles: [...state.blogArticles, ...action.payload.blogArticles],
        page: state.page + 1,
        promoted: action.payload?.promoted || state.promoted,
        allLoaded: action.payload.blogArticles.length === 0,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_BLOG_ARTICLES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_BLOG_ARTICLES_TAG:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_BLOG_ARTICLES_TAGS_SUCCESS:
      return {
        ...state,
        articlesTags: action.payload.articlesTags,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_BLOG_ARTICLES_TAG_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_ARTICLE:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_ARTICLE_SUCCESS:
      return {
        ...state,
        article: action.payload.article,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_ARTICLE_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_ARTICLE_COMMENTS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_ARTICLE_COMMENTS_SUCCESS:
      return {
        ...state,
        comments: action.payload.comments,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_ARTICLE_COMMENTS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    default:
      return state;
  }
}
