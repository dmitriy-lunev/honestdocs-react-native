import homeAPI from '../../../../api/home-api';
import { ACTION_TYPES } from './blog';

export const setFollowingTagsId = (followingTagsId) => ({
  type: ACTION_TYPES.SET_FOLLOWING_TAGS,
  payload: { followingTagsId },
});

export const clearBlog = () => ({
  type: ACTION_TYPES.CLEAR_BLOG,
});

export const clearBlogArticles = () => ({
  type: ACTION_TYPES.CLEAR_BLOG_ARTICLES,
});

function loadBlogArticles() {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES,
  };
}

function loadBlogArticlesSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_SUCCESS,
    payload: { ...data },
  };
}

function loadBlogArticlesFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_FAILURE,
    payload: { error },
  };
}

export function loadBlogArticlesFetch() {
  return (dispatch, getState) => {
    dispatch(loadBlogArticles());
    const { page } = getState().home.blog;
    return homeAPI
      .getBlogArticles(page)
      .then((blogArticles) => {
        const articles = blogArticles.data.data.map((item) => {
          const {
            id,
            attributes: { main_title: title, url, image },
          } = item;
          return { id, title, url, image };
        });
        let data = { blogArticles: articles };
        if (page === 1) {
          const {
            id,
            attributes: { title, description, image },
          } = blogArticles.data.meta.ads.data;
          data.promoted = { id, title, description, image };
        }
        dispatch(loadBlogArticlesSuccess(data));
      })
      .catch((error) => {
        dispatch(loadBlogArticlesFailure(error));
      });
  };
}

function loadBlogArticlesTags() {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_TAGS,
  };
}

function loadBlogArticlesTagsSuccess(articlesTags) {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_TAGS_SUCCESS,
    payload: { articlesTags },
  };
}

function loadBlogArticlesTagsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_FAILURE,
    payload: { error },
  };
}

export function loadBlogArticlesTagsFetch() {
  return (dispatch) => {
    dispatch(loadBlogArticlesTags());
    return homeAPI
      .getBlogArticlesTags()
      .then((articlesTags) => {
        let tags = {};
        articlesTags.data.data.map((item) => {
          tags[item.id.toString()] = item.attributes.name;
        });
        dispatch(loadBlogArticlesTagsSuccess(tags));
      })
      .catch((error) => {
        dispatch(loadBlogArticlesTagsFailure(error));
      });
  };
}

function loadTopArticles() {
  return {
    type: ACTION_TYPES.LOAD_TOP_ARTICLES,
  };
}

function loadTopArticlesSuccess(topArticles) {
  return {
    type: ACTION_TYPES.LOAD_TOP_ARTICLES_SUCCESS,
    payload: { topArticles },
  };
}

function loadTopArticlesFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_TOP_ARTICLES_FAILURE,
    payload: { error },
  };
}

export function loadTopArticlesFetch() {
  return (dispatch) => {
    dispatch(loadTopArticles());
    return homeAPI
      .getBlogArticles(1)
      .then((topArticles) => {
        const data = topArticles.data.data.map((item) => {
          const {
            id,
            attributes: { main_title: title, url, image },
          } = item;
          return { id, title, url, image };
        });
        dispatch(loadTopArticlesSuccess(data));
      })
      .catch((error) => {
        dispatch(loadTopArticlesFailure(error));
      });
  };
}

function loadArticle() {
  return {
    type: ACTION_TYPES.LOAD_ARTICLE,
  };
}

function loadArticleSuccess(article) {
  return {
    type: ACTION_TYPES.LOAD_ARTICLE_SUCCESS,
    payload: { article },
  };
}

function loadArticleFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_FAILURE,
    payload: { error },
  };
}

export function loadArticleFetch(uuid) {
  return (dispatch) => {
    dispatch(loadArticle());
    return homeAPI
      .getBlogArticle(uuid)
      .then((article) => {
        const {
          id,
          attributes: {
            main_title: mainTitle,
            url,
            body,
            title,
            image,
            category,
            written_by: writtenBy,
            reviewed_by: reviewedBy,
            summary,
          },
        } = article.data.data;
        dispatch(
          loadArticleSuccess({
            id,
            mainTitle,
            title,
            url,
            body,
            image,
            category,
            writtenBy,
            reviewedBy,
            summary,
          }),
        );
      })
      .catch((error) => {
        dispatch(loadArticleFailure(error));
      });
  };
}

function loadArticleComments() {
  return {
    type: ACTION_TYPES.LOAD_ARTICLE_COMMENTS,
  };
}

function loadArticleCommentsSuccess(comments) {
  return {
    type: ACTION_TYPES.LOAD_ARTICLE_COMMENTS_SUCCESS,
    payload: { comments },
  };
}

function loadArticleCommentsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_BLOG_ARTICLES_COMMENTS_FAILURE,
    payload: { error },
  };
}

export function loadArticleCommentsFetch(uuid) {
  return (dispatch) => {
    dispatch(loadArticleComments());
    return homeAPI
      .getComments('blog_article', uuid)
      .then((comments) => {
        const data = comments.data.data.map((item) => {
          const {
            id,
            attributes: {
              text,
              user_id: userId,
              user_name: userName,
              user_age: userAge,
              user_gender: userGender,
            },
          } = item;
          return { id, text, userId, userName, userAge, userGender };
        });
        dispatch(loadArticleCommentsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadArticleCommentsFailure(error));
      });
  };
}

export function loadBlogArticlesByTagFetch(tag) {
  return (dispatch, getState) => {
    dispatch(loadBlogArticles());
    const { page } = getState().home.blog;
    return homeAPI
      .getBlogArticlesByTag(page, tag)
      .then((blogArticles) => {
        const articles = blogArticles.data.data.map((item) => {
          const {
            id,
            attributes: { main_title: title, url, image },
          } = item;
          return { id, title, url, image };
        });
        let data = { blogArticles: articles };
        if (page === 1) {
          const {
            id,
            attributes: { title, description, image },
          } = blogArticles.data.meta.ads.data;
          data.promoted = { id, title, description, image };
        }
        dispatch(loadBlogArticlesSuccess(data));
      })
      .catch((error) => {
        dispatch(loadBlogArticlesFailure(error));
      });
  };
}
