const STATE_KEY = 'home/hospitals';

export const ACTION_TYPES = {
  CLEAR_HOSPITALS: `${STATE_KEY}/CLEAR_HOSPITALS`,

  LOAD_HOMEPAGE_HOSPITALS: `${STATE_KEY}/LOAD_HOMEPAGE_HOSPITALS`,
  LOAD_HOMEPAGE_HOSPITALS_SUCCESS: `${STATE_KEY}/LOAD_HOMEPAGE_HOSPITALS_SUCCESS`,
  LOAD_HOMEPAGE_HOSPITALS_FAILURE: `${STATE_KEY}/LOAD_HOMEPAGE_HOSPITALS_FAILURE`,

  LOAD_HOSPITALS: `${STATE_KEY}/LOAD_HOSPITALS`,
  LOAD_HOSPITALS_SUCCESS: `${STATE_KEY}/LOAD_HOSPITALS_SUCCESS`,
  LOAD_HOSPITALS_FAILURE: `${STATE_KEY}/LOAD_HOSPITALS_FAILURE`,

  LOAD_HOSPITAL: `${STATE_KEY}/LOAD_HOSPITAL`,
  LOAD_HOSPITAL_SUCCESS: `${STATE_KEY}/LOAD_HOSPITAL_SUCCESS`,
  LOAD_HOSPITAL_FAILURE: `${STATE_KEY}/LOAD_HOSPITAL_FAILURE`,
};

const initialState = {
  homepageHospitals: [],
  hospitals: [],
  hospital: {},
  page: 1,
  allLoaded: false,
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR_HOSPITALS:
      return initialState;
    case ACTION_TYPES.LOAD_HOMEPAGE_HOSPITALS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_HOMEPAGE_HOSPITALS_SUCCESS:
      return {
        ...state,
        homepageHospitals: action.payload.homepageHospitals,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_HOMEPAGE_HOSPITALS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_HOSPITALS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_HOSPITALS_SUCCESS:
      return {
        ...state,
        hospitals: [...state.hospitals, ...action.payload.hospitals],
        page: state.page + 1,
        allLoaded: action.payload.hospitals.length === 0,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_HOSPITALS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    default:
      return state;
  }
}
