import homeAPI from '../../../../api/home-api';
import { roundAvgReview } from '../../../../helpers/roundAvgReview';
import { ACTION_TYPES } from './hospitals';

export const clearHospitals = () => ({
  type: ACTION_TYPES.CLEAR_HOSPITALS,
});

function loadHomepageHospitals() {
  return {
    type: ACTION_TYPES.LOAD_HOMEPAGE_HOSPITALS,
  };
}

function loadHomepageHospitalsSuccess(homepageHospitals) {
  return {
    type: ACTION_TYPES.LOAD_HOMEPAGE_HOSPITALS_SUCCESS,
    payload: { homepageHospitals },
  };
}

function loadHomepageHospitalsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_HOMEPAGE_HOSPITALS_FAILURE,
    payload: { error },
  };
}

export function loadHomepageHospitalsFetch() {
  return (dispatch) => {
    dispatch(loadHomepageHospitals());
    return homeAPI
      .getHomepageHospitals()
      .then((shopCategories) => {
        const data = shopCategories.data.data.map((item) => {
          const {
            id,
            attributes: { name, phone, image, address },
          } = item;
          return { id, name, phone, address, image };
        });
        dispatch(loadHomepageHospitalsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadHomepageHospitalsFailure(error));
      });
  };
}

function loadHospitals() {
  return {
    type: ACTION_TYPES.LOAD_HOSPITALS,
  };
}

function loadHospitalsSuccess(hospitals) {
  return {
    type: ACTION_TYPES.LOAD_HOSPITALS_SUCCESS,
    payload: { hospitals },
  };
}

function loadHospitalsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_HOSPITALS_FAILURE,
    payload: { error },
  };
}

export function loadHospitalsFetch() {
  return (dispatch, getState) => {
    dispatch(loadHospitals());
    const {
      home: {
        hospitals: { page },
      },
    } = getState();
    return homeAPI
      .getHospitals(page)
      .then((hospitals) => {
        const data = hospitals.data.data.map((item) => {
          const {
            id,
            attributes: { name, avg_review, image },
          } = item;
          const avgReview = roundAvgReview(avg_review);
          return { id, name, avgReview, image };
        });
        dispatch(loadHospitalsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadHospitalsFailure(error));
      });
  };
}
