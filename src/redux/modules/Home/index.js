import { combineReducers } from 'redux';
import blogReducer from './Blog/blog';
import doctorQAReducer from './DoctorQA/doctorQA';
import homepageReducer from './Homepage/homepage';
import hospitalsReducer from './Hospitals/hospitals';

export default combineReducers({
  blog: blogReducer,
  doctorQA: doctorQAReducer,
  homepage: homepageReducer,
  hospitals: hospitalsReducer,
});
