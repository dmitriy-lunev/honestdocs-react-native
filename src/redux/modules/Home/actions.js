import * as blogActions from './Blog/actions';
import * as doctorQAActions from './DoctorQA/actions';
import * as homepageActions from './Homepage/actions';
import * as hospitalsActions from './Hospitals/actions';

export default {
  ...blogActions,
  ...doctorQAActions,
  ...homepageActions,
  ...hospitalsActions,
};
