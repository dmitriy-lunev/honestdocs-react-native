const STATE_KEY = 'conditions';

export const ACTION_TYPES = {
  CLEAR_CONDITIONS: `${STATE_KEY}/CLEAR_CONDITIONS`,

  LOAD_CATEGORIES: `${STATE_KEY}/LOAD_CATEGORIES`,
  LOAD_CATEGORIES_SUCCESS: `${STATE_KEY}/LOAD_CATEGORIES_SUCCESS`,
  LOAD_CATEGORIES_FAILURE: `${STATE_KEY}/LOAD_CATEGORIES_FAILURE`,

  LOAD_MOST_SEARCHED_CONDITIONS: `${STATE_KEY}/LOAD_MOST_SEARCHED_CONDITIONS`,
  LOAD_MOST_SEARCHED_CONDITIONS_SUCCESS: `${STATE_KEY}/LOAD_MOST_SEARCHED_CONDITIONS_SUCCESS`,
  LOAD_MOST_SEARCHED_CONDITIONS_FAILURE: `${STATE_KEY}/LOAD_MOST_SEARCHED_CONDITIONS_FAILURE`,

  LOAD_CONDITIONS_BY_CATEGORIES: `${STATE_KEY}/LOAD_CONDITIONS_BY_CATEGORIES`,
  LOAD_CONDITIONS_BY_CATEGORIES_SUCCESS: `${STATE_KEY}/LOAD_CONDITIONS_BY_CATEGORIES_SUCCESS`,
  LOAD_CONDITIONS_BY_CATEGORIES_FAILURE: `${STATE_KEY}/LOAD_CONDITIONS_BY_CATEGORIES_FAILURE`,

  LOAD_CONDITION: `${STATE_KEY}/LOAD_CONDITION`,
  LOAD_CONDITION_SUCCESS: `${STATE_KEY}/LOAD_CONDITION_SUCCESS`,
  LOAD_CONDITION_FAILURE: `${STATE_KEY}/LOAD_CONDITION_FAILURE`,

  LOAD_CONDITION_COMMENTS: `${STATE_KEY}/LOAD_CONDITION_COMMENTS`,
  LOAD_CONDITION_COMMENTS_SUCCESS: `${STATE_KEY}/LOAD_CONDITION_COMMENTS_SUCCESS`,
  LOAD_CONDITION_COMMENTS_FAILURE: `${STATE_KEY}/LOAD_CONDITION_COMMENTS_FAILURE`,

  SET_RATING: `${STATE_KEY}/SET_CONDITION_RATING`,

  SET_CONDITION_BOOKMARK: `${STATE_KEY}/SET_CONDITION_BOOKMARK`,
};

const initialState = {
  categories: [],
  mostSearchedConditions: [],
  mostSearchedConditionsPage: 1,
  conditions: [],
  condition: {},
  conditionComments: [],
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR_CONDITIONS:
      return {
        ...state,
        conditions: [],
      };
    case ACTION_TYPES.LOAD_CATEGORIES:
      return {
        ...state,
      };
    case ACTION_TYPES.LOAD_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.payload.categories,
      };
    case ACTION_TYPES.LOAD_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case ACTION_TYPES.LOAD_MOST_SEARCHED_CONDITIONS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_MOST_SEARCHED_CONDITIONS_SUCCESS:
      return {
        ...state,
        mostSearchedConditions: [
          ...state.mostSearchedConditions,
          ...action.payload.mostSearchedConditions,
        ],
        mostSearchedConditionsPage: state.mostSearchedConditionsPage + 1,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_MOST_SEARCHED_CONDITIONS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_CONDITIONS_BY_CATEGORIES:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_CONDITIONS_BY_CATEGORIES_SUCCESS:
      return {
        ...state,
        conditions: action.payload.conditions,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_CONDITIONS_BY_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_CONDITION:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_CONDITION_SUCCESS:
      return {
        ...state,
        condition: action.payload.condition,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_CONDITION_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_CONDITION_COMMENTS:
      return {
        ...state,
      };
    case ACTION_TYPES.LOAD_CONDITION_COMMENTS_SUCCESS:
      return {
        ...state,
        conditionComments: action.payload.conditionComments,
      };
    case ACTION_TYPES.LOAD_CONDITION_COMMENTS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case ACTION_TYPES.SET_RATING:
      return {
        ...state,
        condition: {
          ...state.condition,
          currentUserRating: action.payload.rating,
        },
      };
    case ACTION_TYPES.SET_BOOKMARK:
      return {
        ...state,
        condition: {
          ...state.condition,
          bookmarked: !state.condition.bookmarked,
        },
      };
    default:
      return state;
  }
}
