import bookmarksAPI from '../../../api/bookmarks-api';
import commentsAPI from '../../../api/comments-api';
import conditionsAPI from '../../../api/conditions-api';
import ratingAPI from '../../../api/rating-api';
import { ACTION_TYPES } from './index';

export const clearConditions = () => ({
  type: ACTION_TYPES.CLEAR_CONDITIONS,
});

function loadConditionsCategories() {
  return {
    type: ACTION_TYPES.LOAD_CATEGORIES,
  };
}

function loadConditionsCategoriesSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_CATEGORIES_SUCCESS,
    payload: { categories: data },
  };
}

function loadConditionsCategoriesFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_CATEGORIES_FAILURE,
    payload: { error },
  };
}

export function loadConditionsCategoriesFetch() {
  return (dispatch) => {
    dispatch(loadConditionsCategories());
    return conditionsAPI
      .getCategoryConditions()
      .then((categories) => {
        const data = categories.data.data.map((item) => {
          const {
            id,
            attributes: { name },
          } = item;
          return {
            id,
            name,
          };
        });
        dispatch(loadConditionsCategoriesSuccess(data));
      })
      .catch((error) => {
        dispatch(loadConditionsCategoriesFailure(error));
      });
  };
}

function loadMostSearchedConditions() {
  return {
    type: ACTION_TYPES.LOAD_MOST_SEARCHED_CONDITIONS,
  };
}

function loadMostSearchedConditionsSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_MOST_SEARCHED_CONDITIONS_SUCCESS,
    payload: { mostSearchedConditions: data },
  };
}

function loadMostSearchedConditionsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_MOST_SEARCHED_CONDITIONS_FAILURE,
    payload: { error },
  };
}

export function loadMostSearchedConditionsFetch() {
  return (dispatch, getState) => {
    dispatch(loadMostSearchedConditions());
    const { mostSearchedConditionsPage } = getState().conditions;
    return conditionsAPI
      .getMostSearchedConditions(mostSearchedConditionsPage)
      .then((mostSearchedConditions) => {
        const data = mostSearchedConditions.data.data.map((item) => {
          const {
            id,
            attributes: { name, conditions },
          } = item;
          return {
            id,
            name,
            conditions,
          };
        });
        dispatch(loadMostSearchedConditionsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadMostSearchedConditionsFailure(error));
      });
  };
}

function loadConditionsByCategory() {
  return {
    type: ACTION_TYPES.LOAD_CONDITIONS_BY_CATEGORIES,
  };
}

function loadConditionsByCategorySuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_CONDITIONS_BY_CATEGORIES_SUCCESS,
    payload: { conditions: data },
  };
}

function loadConditionsByCategoryFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_CONDITIONS_BY_CATEGORIES_FAILURE,
    payload: { error },
  };
}

export function loadConditionsByCategoryFetch(categoryId) {
  return (dispatch) => {
    dispatch(loadConditionsByCategory());
    return conditionsAPI
      .getConditionsByCategory(categoryId)
      .then((conditions) => {
        const data = conditions.data.data.map((item) => {
          const {
            id,
            attributes: { name, main_title: mainTitle },
          } = item;
          return {
            id,
            name,
            mainTitle,
          };
        });
        dispatch(loadConditionsByCategorySuccess(data));
      })
      .catch((error) => {
        dispatch(loadConditionsByCategoryFailure(error));
      });
  };
}

function loadCondition() {
  return {
    type: ACTION_TYPES.LOAD_CONDITION,
  };
}

function loadConditionSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_CONDITION_SUCCESS,
    payload: { condition: data },
  };
}

function loadConditionFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_CONDITION_FAILURE,
    payload: { error },
  };
}

export function loadConditionFetch(drugId) {
  return (dispatch) => {
    dispatch(loadCondition());
    return conditionsAPI
      .getCondition(drugId)
      .then((drug) => {
        const {
          id,
          attributes: {
            name,
            url,
            main_title: mainTitle,
            body,
            category_drug_id: categoryDrugId,
            image,
            rating,
            written_by: writtenBy,
            reviewed_by: reviewedBy,
            reviewed_date: reviewedDate,
            sponsor_name: sponsorName,
            sponsor_logo: sponsorLogo,
            man_image: manImage,
            woman_image: womanImage,
            current_user_rating: currentUserRating,
            bookmarked,
            references,
          },
        } = drug.data.data;
        const {
          id: promotedId,
          attributes: {
            url: promotedIdUrl,
            title,
            slug,
            description,
            image: promotedIdImage,
          },
        } = drug.data.meta.ads.data[0];
        dispatch(
          loadConditionSuccess({
            id,
            name,
            url,
            mainTitle,
            body,
            categoryDrugId,
            image,
            rating,
            writtenBy,
            reviewedBy,
            reviewedDate,
            sponsorName,
            sponsorLogo,
            manImage,
            womanImage,
            currentUserRating,
            bookmarked,
            references,
            promoted: {
              promotedId,
              promotedIdUrl,
              slug,
              title,
              description,
              promotedIdImage,
            },
          }),
        );
      })
      .catch((error) => {
        dispatch(loadConditionFailure(error));
      });
  };
}

function loadConditionComments() {
  return {
    type: ACTION_TYPES.LOAD_CONDITION_COMMENTS,
  };
}

function loadConditionCommentsSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_CONDITION_COMMENTS_SUCCESS,
    payload: { conditionComments: data },
  };
}

function loadConditionCommentsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_CONDITION_COMMENTS_FAILURE,
    payload: { error },
  };
}

export function loadConditionCommentsFetch(commentableId) {
  return (dispatch) => {
    dispatch(loadConditionComments());
    return commentsAPI
      .getComments('condition', commentableId)
      .then((comments) => {
        const data = comments.data.data.map((item) => {
          const {
            id,
            attributes: {
              text,
              commentable_id: commentableId,
              created_at: createdAt,
              user_id: userId,
              user_name: userName,
              user_age: userAge,
              user_gender: userGender,
              comments_count: commentsCount,
              answered_by_doctor: answeredByDoctor,
              user_photo: userPhoto,
            },
          } = item;
          return {
            id,
            text,
            commentableId,
            createdAt,
            userId,
            userName,
            userAge,
            userGender,
            userPhoto,
            commentsCount,
            answeredByDoctor,
          };
        });
        dispatch(loadConditionCommentsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadConditionCommentsFailure(error));
      });
  };
}

function setConditionRating(data) {
  return {
    type: ACTION_TYPES.SET_CONDITION_RATING,
    payload: { rating: data },
  };
}

export function setRatingFetch(rating, ratingableId) {
  return (dispatch, getState) => {
    const {
      userToken,
      userInfo: { id },
    } = getState().authentication;
    return ratingAPI
      .setRating(id, userToken, rating, 'condition', ratingableId)
      .then(() => {
        dispatch(setConditionRating(ratingableId));
      })
      .catch(() => {});
  };
}

function setConditionBookmark() {
  return {
    type: ACTION_TYPES.SET_CONDITION_BOOKMARK,
  };
}

export function setConditionBookmarkFetch(drugId) {
  return (dispatch, getState) => {
    const {
      userToken,
      userInfo: { id: userId },
    } = getState().authentication;
    return bookmarksAPI
      .createBookmark(userId, userToken, 'condition', drugId)
      .then(() => {
        dispatch(setConditionBookmark());
      })
      .catch(() => {});
  };
}
