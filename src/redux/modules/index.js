export { default as home } from './Home';
export { default as authentication } from './Authentication';
export { default as user } from './User';
export { default as doctorQuestions } from './DoctorQuestions';
export { default as drugs } from './Drugs';
export { default as conditions } from './Conditions';
