const STATE_KEY = 'login';

export const ACTION_TYPES = {
  SEND_OTP: `${STATE_KEY}/SEND_OTP`,
  CHECK_OTP: `${STATE_KEY}/CHECK_OTP`,
  LOG_OUT: `${STATE_KEY}/LOG_OUT`,
};

const initialState = {
  userMobile: null,
  userToken: null,
  userInfo: {},
  isSignedIn: false,
};

const Authentication = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPES.SEND_OTP:
      return {
        ...state,
        userMobile: action.payload.userMobile,
      };
    case ACTION_TYPES.CHECK_OTP:
      return {
        ...state,
        userToken: action.payload.userToken,
        userInfo: action.payload.userInfo,
        isSignedIn: true,
      };
    case ACTION_TYPES.LOG_OUT:
      return initialState;
    default:
      return state;
  }
};

export default Authentication;
