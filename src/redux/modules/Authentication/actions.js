import { ACTION_TYPES } from '.';
import loginAPI from '../../../api/login-api';
import { clearUser } from '../User/actions';

const sendOtp = (userMobile) => ({
  type: ACTION_TYPES.SEND_OTP,
  payload: { userMobile },
});

export const sendOtpFetch = (userMobile) => {
  return (dispatch) => {
    return loginAPI
      .sendOtp(userMobile)
      .then(() => dispatch(sendOtp(userMobile)));
  };
};

const checkOtp = (data) => ({
  type: ACTION_TYPES.CHECK_OTP,
  payload: { userToken: data.token, userInfo: data.user },
});

export const checkOtpFetch = (userOtp) => {
  return (dispatch, getState) => {
    const { userMobile } = getState().authentication;
    return loginAPI
      .checkOtp(userMobile, userOtp)
      .then((response) => dispatch(checkOtp(response.data)));
  };
};

const logOut = () => ({ type: ACTION_TYPES.LOG_OUT });

export const logOutFetch = () => {
  return (dispatch, getState) => {
    const { userMobile, userToken } = getState().authentication;
    return loginAPI
      .deleteToken(userMobile, userToken)
      .then(() => dispatch(logOut()))
      .then(() => dispatch(clearUser()));
  };
};
