import { ACTION_TYPES } from './index';
import userAPI from '../../../api/user-api';

const sections = {
  basic_info: 'basicInfo',
  emergency: 'emergency',
  my_conditions: 'myConditions',
  my_drugs: 'myDrugs',
  patient_info: 'patientInfo',
};

export function clearUser() {
  return {
    type: ACTION_TYPES.CLEAR,
  };
}

function loadUserInfo() {
  return {
    type: ACTION_TYPES.LOAD_USER_INFO,
  };
}

function loadUserInfoSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_USER_INFO_SUCCESS,
    payload: data,
  };
}

function loadUserInfoFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_USER_INFO_FAILURE,
    payload: { error },
  };
}

export function loadUserInfoFetch(section) {
  return (dispatch, getState) => {
    dispatch(loadUserInfo());
    const {
      userToken,
      userInfo: { id: userInfo },
    } = getState().authentication;
    return userAPI
      .getUserInfo(userInfo, userToken, section)
      .then((response) => {
        dispatch(
          loadUserInfoSuccess({
            [sections[section]]: response.data.data.attributes,
          }),
        );
      })
      .catch((error) => loadUserInfoFailure(error));
  };
}
