const STATE_KEY = 'home/homepage';

export const ACTION_TYPES = {
  CLEAR: `${STATE_KEY}/CLEAR`,

  LOAD_USER_INFO: `${STATE_KEY}/LOAD_USER_INFO`,
  LOAD_USER_INFO_SUCCESS: `${STATE_KEY}/LOAD_USER_INFO_SUCCESS`,
  LOAD_USER_INFO_FAILURE: `${STATE_KEY}/LOAD_USER_INFO_FAILURE`,
};

const initialState = {
  basicInfo: {},
  loading: false,
  loaded: false,
  error: null,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR:
      return initialState;
    case ACTION_TYPES.LOAD_USER_INFO:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_USER_INFO_SUCCESS:
      return {
        ...state,
        [Object.keys(action.payload)[0]]:
          action.payload[Object.keys(action.payload)[0]],
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_USER_INFO_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    default:
      return state;
  }
}
