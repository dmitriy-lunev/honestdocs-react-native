import { ACTION_TYPES } from './index';
import drugsAPI from '../../../api/drugs-api';
import ratingAPI from '../../../api/rating-api';
import commentsAPI from '../../../api/comments-api';
import bookmarksAPI from '../../../api/bookmarks-api';

export const clearDrugs = () => ({
  type: ACTION_TYPES.CLEAR_DRUGS,
});

function loadDrugsCategories() {
  return {
    type: ACTION_TYPES.LOAD_CATEGORIES,
  };
}

function loadDrugsCategoriesSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_CATEGORIES_SUCCESS,
    payload: { categories: data },
  };
}

function loadDrugsCategoriesFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_CATEGORIES_FAILURE,
    payload: { error },
  };
}

export function loadDrugsCategoriesFetch() {
  return (dispatch) => {
    dispatch(loadDrugsCategories());
    return drugsAPI
      .getCategoryDrugs()
      .then((categories) => {
        const data = categories.data.data.map((item) => {
          const {
            id,
            attributes: { name },
          } = item;
          return {
            id,
            name,
          };
        });
        dispatch(loadDrugsCategoriesSuccess(data));
      })
      .catch((error) => {
        dispatch(loadDrugsCategoriesFailure(error));
      });
  };
}

function loadMostSearchedDrugs() {
  return {
    type: ACTION_TYPES.LOAD_MOST_SEARCHED_DRUGS,
  };
}

function loadMostSearchedDrugsSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_MOST_SEARCHED_DRUGS_SUCCESS,
    payload: { mostSearchedDrugs: data },
  };
}

function loadMostSearchedDrugsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_MOST_SEARCHED_DRUGS_FAILURE,
    payload: { error },
  };
}

export function loadMostSearchedDrugsFetch() {
  return (dispatch, getState) => {
    dispatch(loadMostSearchedDrugs());
    const { mostSearchedDrugsPage } = getState().drugs;
    return drugsAPI
      .getMostSearchedDrugs(mostSearchedDrugsPage)
      .then((mostSearchedDrugs) => {
        const data = mostSearchedDrugs.data.data.map((item) => {
          const {
            id,
            attributes: { name, drugs },
          } = item;
          return {
            id,
            name,
            drugs,
          };
        });
        dispatch(loadMostSearchedDrugsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadMostSearchedDrugsFailure(error));
      });
  };
}

function loadDrugsByCategory() {
  return {
    type: ACTION_TYPES.LOAD_DRUGS_BY_CATEGORIES,
  };
}

function loadDrugsByCategorySuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_DRUGS_BY_CATEGORIES_SUCCESS,
    payload: { drugs: data },
  };
}

function loadDrugsByCategoryFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_DRUGS_BY_CATEGORIES_FAILURE,
    payload: { error },
  };
}

export function loadDrugsByCategoriesFetch(categoryId) {
  return (dispatch) => {
    dispatch(loadDrugsByCategory());
    return drugsAPI
      .getDrugsByCategory(categoryId)
      .then((drugs) => {
        const data = drugs.data.data.map((item) => {
          const {
            id,
            attributes: { name, main_title: mainTitle },
          } = item;
          return {
            id,
            name,
            mainTitle,
          };
        });
        dispatch(loadDrugsByCategorySuccess(data));
      })
      .catch((error) => {
        dispatch(loadDrugsByCategoryFailure(error));
      });
  };
}

function loadDrug() {
  return {
    type: ACTION_TYPES.LOAD_DRUG,
  };
}

function loadDrugSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_DRUG_SUCCESS,
    payload: { drug: data },
  };
}

function loadDrugFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_DRUG_FAILURE,
    payload: { error },
  };
}

export function loadDrugFetch(drugId) {
  return (dispatch) => {
    dispatch(loadDrug());
    return drugsAPI
      .getDrug(drugId)
      .then((drug) => {
        console.log(drug);
        const {
          id,
          attributes: {
            name,
            url,
            main_title: mainTitle,
            body,
            category_drug_id: categoryDrugId,
            image,
            rating,
            written_by: writtenBy,
            reviewed_by: reviewedBy,
            reviewed_date: reviewedDate,
            sponsor_name: sponsorName,
            sponsor_logo: sponsorLogo,
            man_image: manImage,
            woman_image: womanImage,
            current_user_rating: currentUserRating,
            bookmarked,
            references,
          },
        } = drug.data.data;
        const {
          id: promotedId,
          attributes: {
            url: promotedIdUrl,
            title,
            slug,
            description,
            image: promotedIdImage,
          },
        } = drug.data.meta.ads.data[0];
        dispatch(
          loadDrugSuccess({
            id,
            name,
            url,
            mainTitle,
            body,
            categoryDrugId,
            image,
            rating,
            writtenBy,
            reviewedBy,
            reviewedDate,
            sponsorName,
            sponsorLogo,
            manImage,
            womanImage,
            currentUserRating,
            bookmarked,
            references,
            promoted: {
              promotedId,
              promotedIdUrl,
              slug,
              title,
              description,
              promotedIdImage,
            },
          }),
        );
      })
      .catch((error) => {
        dispatch(loadDrugFailure(error));
      });
  };
}

function loadDrugComments() {
  return {
    type: ACTION_TYPES.LOAD_DRUG_COMMENTS,
  };
}

function loadDrugCommentsSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_DRUG_COMMENTS_SUCCESS,
    payload: { drugComments: data },
  };
}

function loadDrugCommentsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_DRUG_COMMENTS_FAILURE,
    payload: { error },
  };
}

export function loadDrugCommentsFetch(commentableId) {
  return (dispatch) => {
    dispatch(loadDrugComments());
    return commentsAPI
      .getComments('drug', commentableId)
      .then((comments) => {
        const data = comments.data.data.map((item) => {
          const {
            id,
            attributes: {
              text,
              commentable_id: commentableId,
              created_at: createdAt,
              user_id: userId,
              user_name: userName,
              user_age: userAge,
              user_gender: userGender,
              comments_count: commentsCount,
              answered_by_doctor: answeredByDoctor,
              user_photo: userPhoto,
            },
          } = item;
          return {
            id,
            text,
            commentableId,
            createdAt,
            userId,
            userName,
            userAge,
            userGender,
            userPhoto,
            commentsCount,
            answeredByDoctor,
          };
        });
        dispatch(loadDrugCommentsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadDrugCommentsFailure(error));
      });
  };
}

function setRating(data) {
  return {
    type: ACTION_TYPES.SET_RATING,
    payload: { rating: data },
  };
}

export function setRatingFetch(rating, ratingable_type, ratingableId) {
  return (dispatch, getState) => {
    const {
      userToken,
      userInfo: { id },
    } = getState().authentication;
    return ratingAPI
      .setRating(id, userToken, rating, ratingable_type, ratingableId)
      .then(() => {
        dispatch(setRating(ratingableId));
      })
      .catch(() => {});
  };
}

function setBookmark() {
  return {
    type: ACTION_TYPES.SET_BOOKMARK,
  };
}

export function setBookmarkFetch(drugId) {
  return (dispatch, getState) => {
    const {
      userToken,
      userInfo: { id: userId },
    } = getState().authentication;
    return bookmarksAPI
      .createBookmark(userId, userToken, 'drug', drugId)
      .then(() => {
        dispatch(setBookmark());
      })
      .catch(() => {});
  };
}
