const STATE_KEY = 'drugs';

export const ACTION_TYPES = {
  CLEAR_DRUGS: `${STATE_KEY}/CLEAR_DRUGS`,

  LOAD_CATEGORIES: `${STATE_KEY}/LOAD_CATEGORIES`,
  LOAD_CATEGORIES_SUCCESS: `${STATE_KEY}/LOAD_CATEGORIES_SUCCESS`,
  LOAD_CATEGORIES_FAILURE: `${STATE_KEY}/LOAD_CATEGORIES_FAILURE`,

  LOAD_MOST_SEARCHED_DRUGS: `${STATE_KEY}/LOAD_MOST_SEARCHED_DRUGS`,
  LOAD_MOST_SEARCHED_DRUGS_SUCCESS: `${STATE_KEY}/LOAD_MOST_SEARCHED_DRUGS_SUCCESS`,
  LOAD_MOST_SEARCHED_DRUGS_FAILURE: `${STATE_KEY}/LOAD_MOST_SEARCHED_DRUGS_FAILURE`,

  LOAD_DRUGS_BY_CATEGORIES: `${STATE_KEY}/LOAD_DRUGS_BY_CATEGORIES`,
  LOAD_DRUGS_BY_CATEGORIES_SUCCESS: `${STATE_KEY}/LOAD_DRUGS_BY_CATEGORIES_SUCCESS`,
  LOAD_DRUGS_BY_CATEGORIES_FAILURE: `${STATE_KEY}/LOAD_DRUGS_BY_CATEGORIES_FAILURE`,

  LOAD_DRUG: `${STATE_KEY}/LOAD_DRUG`,
  LOAD_DRUG_SUCCESS: `${STATE_KEY}/LOAD_DRUG_SUCCESS`,
  LOAD_DRUG_FAILURE: `${STATE_KEY}/LOAD_DRUG_FAILURE`,

  LOAD_DRUG_COMMENTS: `${STATE_KEY}/LOAD_DRUG_COMMENTS`,
  LOAD_DRUG_COMMENTS_SUCCESS: `${STATE_KEY}/LOAD_DRUG_COMMENTS_SUCCESS`,
  LOAD_DRUG_COMMENTS_FAILURE: `${STATE_KEY}/LOAD_DRUG_COMMENTS_FAILURE`,

  SET_RATING: `${STATE_KEY}/SET_RATING`,

  SET_BOOKMARK: `${STATE_KEY}/SET_BOOKMARK`,
};

const initialState = {
  categories: [],
  mostSearchedDrugs: [],
  mostSearchedDrugsPage: 1,
  drugs: [],
  drug: {},
  drugComments: [],
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR_DRUGS:
      return {
        ...state,
        drugs: [],
      };
    case ACTION_TYPES.LOAD_CATEGORIES:
      return {
        ...state,
      };
    case ACTION_TYPES.LOAD_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.payload.categories,
      };
    case ACTION_TYPES.LOAD_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case ACTION_TYPES.LOAD_MOST_SEARCHED_DRUGS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_MOST_SEARCHED_DRUGS_SUCCESS:
      return {
        ...state,
        mostSearchedDrugs: [
          ...state.mostSearchedDrugs,
          ...action.payload.mostSearchedDrugs,
        ],
        mostSearchedDrugsPage: state.mostSearchedDrugsPage + 1,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_MOST_SEARCHED_DRUGS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_DRUGS_BY_CATEGORIES:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_DRUGS_BY_CATEGORIES_SUCCESS:
      return {
        ...state,
        drugs: action.payload.drugs,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_DRUGS_BY_CATEGORIES_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_DRUG:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_DRUG_SUCCESS:
      return {
        ...state,
        drug: action.payload.drug,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_DRUG_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_DRUG_COMMENTS:
      return {
        ...state,
      };
    case ACTION_TYPES.LOAD_DRUG_COMMENTS_SUCCESS:
      return {
        ...state,
        drugComments: action.payload.drugComments,
      };
    case ACTION_TYPES.LOAD_DRUG_COMMENTS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      };
    case ACTION_TYPES.SET_RATING:
      return {
        ...state,
        drug: { ...state.drug, currentUserRating: action.payload.rating },
      };
    case ACTION_TYPES.SET_BOOKMARK:
      return {
        ...state,
        drug: { ...state.drug, bookmarked: !state.drug.bookmarked },
      };
    default:
      return state;
  }
}
