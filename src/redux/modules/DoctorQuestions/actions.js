import { ACTION_TYPES } from './index';
import questionsAPI from '../../../api/questions-api';

export const clearQuestions = () => ({
  type: ACTION_TYPES.CLEAR_QUESTIONS,
});

function loadQuestions() {
  return {
    type: ACTION_TYPES.LOAD_QUESTIONS,
  };
}

function loadQuestionsSuccess(data) {
  return {
    type: ACTION_TYPES.LOAD_QUESTIONS_SUCCESS,
    payload: { ...data },
  };
}

function loadQuestionsFailure(error) {
  return {
    type: ACTION_TYPES.LOAD_QUESTIONS_FAILURE,
    payload: { error },
  };
}

export function loadQuestionsFetch() {
  return (dispatch, getState) => {
    dispatch(loadQuestions());
    const { page } = getState().doctorQuestions;
    const {
      userToken,
      userInfo: { id: userId },
    } = getState().authentication;
    return questionsAPI
      .getQuestions(userId, userToken, page)
      .then((questions) => {
        let data = {};
        data.questions = questions.data.data.map((item) => {
          const {
            id,
            attributes: {
              title,
              promo,
              answered_by_doctor: answeredByDoctor,
              answers_count: answersCount,
              created_at: createdAt,
              updated_at: updatedAt,
              liked,
            },
          } = item;
          return {
            id,
            title,
            promo,
            answeredByDoctor,
            answersCount,
            createdAt,
            updatedAt,
            liked,
          };
        });
        if (page === 1) {
          const {
            id,
            attributes: { title, description, url, slug, image },
          } = questions.data.meta.ads.data;
          data.promoted = { id, title, description, url, slug, image };
        }
        dispatch(loadQuestionsSuccess(data));
      })
      .catch((error) => {
        dispatch(loadQuestionsFailure(error));
      });
  };
}

function setOrUnsetLike(index) {
  return {
    type: ACTION_TYPES.SET_OR_UNSET_LIKE,
    payload: { index },
  };
}

export function setOrUnsetLikeFetch(index) {
  return (dispatch, getState) => {
    dispatch(setOrUnsetLike(index));
    const { liked, id } = getState().doctorQuestions.questions[index];
    const { userToken, userInfo } = getState().authentication;
    const unset = liked ? null : true;
    return questionsAPI
      .setOrUnsetQuestionLike(id, userInfo.id, userToken, unset)
      .catch((error) => {
        dispatch(setOrUnsetLike(index));
      });
  };
}

function setOrUnsetDislike(index) {
  return {
    type: ACTION_TYPES.SET_OR_UNSET_DISLIKE,
    payload: { index },
  };
}

export function setOrUnsetDislikeFetch(index) {
  return (dispatch, getState) => {
    dispatch(setOrUnsetDislike(index));
    const { liked, id } = getState().doctorQuestions.questions[index];
    const { userToken, userInfo } = getState().authentication;
    const unset = !liked && liked !== null ? null : true;
    return questionsAPI
      .setOrUnsetQuestionDislike(id, userInfo.id, userToken, unset)
      .catch((error) => {
        dispatch(setOrUnsetDislike(index));
      });
  };
}

function postQuestion() {
  return {
    type: ACTION_TYPES.POST_QUESTION,
  };
}

function postQuestionSuccess() {
  return {
    type: ACTION_TYPES.POST_QUESTION_SUCCESS,
  };
}

function postQuestionFailure(error) {
  return {
    type: ACTION_TYPES.POST_QUESTION_FAILURE,
    payload: { error },
  };
}

export function postQuestionFetch(title, images) {
  return (dispatch, getState) => {
    dispatch(postQuestion());
    const {
      userToken,
      userInfo: { id: userId },
    } = getState().authentication;
    return questionsAPI
      .createQuestion(userId, userToken, title, images, null, null)
      .then((question) => {
        dispatch(postQuestionSuccess());
      })
      .catch((error) => {
        dispatch(postQuestionFailure(error));
        throw error;
      });
  };
}
