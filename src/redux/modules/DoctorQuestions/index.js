const STATE_KEY = 'doctor_question';

export const ACTION_TYPES = {
  CLEAR_QUESTIONS: `${STATE_KEY}/CLEAR_BLOG`,

  LOAD_QUESTIONS: `${STATE_KEY}/LOAD_QUESTIONS`,
  LOAD_QUESTIONS_SUCCESS: `${STATE_KEY}/LOAD_QUESTIONS_SUCCESS`,
  LOAD_QUESTIONS_FAILURE: `${STATE_KEY}/LOAD_QUESTIONS_FAILURE`,

  SET_OR_UNSET_LIKE: `${STATE_KEY}/SET_OR_UNSET_LIKE`,
  SET_OR_UNSET_DISLIKE: `${STATE_KEY}/SET_OR_UNSET_DISLIKE`,

  POST_QUESTION: `${STATE_KEY}/POST_QUESTION`,
  POST_QUESTION_SUCCESS: `${STATE_KEY}/POST_QUESTION_SUCCESS`,
  POST_QUESTION_FAILURE: `${STATE_KEY}/POST_QUESTION_FAILURE`,
};

const initialState = {
  questions: [],
  page: 1,
  promoted: {},
  error: null,
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.CLEAR_QUESTIONS:
      return initialState;
    case ACTION_TYPES.LOAD_QUESTIONS:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.LOAD_QUESTIONS_SUCCESS:
      return {
        ...state,
        questions: [...state.questions, ...action.payload.questions],
        page: state.page + 1,
        allLoaded: action.payload.questions.length === 0,
        promoted: action.payload?.promoted,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.LOAD_QUESTIONS_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    case ACTION_TYPES.SET_OR_UNSET_LIKE:
      return {
        ...state,
        questions: [...state.questions].map((item, index) => {
          if (index === action.payload.index) {
            return { ...item, liked: item.liked ? null : true };
          }
          return item;
        }),
      };
    case ACTION_TYPES.SET_OR_UNSET_DISLIKE:
      return {
        ...state,
        questions: [...state.questions].map((item, index) => {
          if (index === action.payload.index) {
            return {
              ...item,
              liked: !item.liked && item.liked !== null ? null : false,
            };
          }
          return item;
        }),
      };
    case ACTION_TYPES.POST_QUESTION:
      return {
        ...state,
        loading: true,
        loaded: false,
      };
    case ACTION_TYPES.POST_QUESTION_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
      };
    case ACTION_TYPES.POST_QUESTION_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
        loaded: false,
      };
    default:
      return state;
  }
}
