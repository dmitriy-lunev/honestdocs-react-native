import Axios from "axios";
import { instance } from "./api";

const userAPI = {
  getUserInfo(userID, token, section) {
    return instance.get(
      `/api/app/v2/users/${userID}?token=${token}&section=${section}`
    );
  },
  putUserInfo(userID, token, object) {
    return instance.put(`/api/app/v2/users/${userID}?token=${token}`, object);
  },
  putUserAvatar(userID, token, object) {
    return Axios.put(
      `https://staging.honestdocs.co/api/app/v2/users/${userID}/avatar?token=${token}`,
      object,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
  },
};

export default userAPI;
