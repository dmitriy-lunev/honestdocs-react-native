import { instance } from "./api";

const packageAPI = {
  getPackage(uuid) {
    return instance.get(`/api/app/v2/packages/${uuid}`);
  },
  getPackagesByCategory({ category, params }) {
    return instance.get(`/api/app/v2/packages/shop_categories/${category}`, {
      params,
    });
  },
  getBrands(category) {
    return instance.get(
      `/api/app/v2/packages/shop_categories/${category}/brands`
    );
  },
  getPackagesByBrandAndPrise(category, brandId, priceSortType, page) {
    // prise: 0 - do not sort, 1 - sort from low to high , 2 - sort from high to low
    return instance.get(
      `/api/app/v2/packages/shop_categories/${category}?page=${page}&brand_id=${brandId}&sort_by=${priceSortType}`
    );
  },
};

export default packageAPI;
