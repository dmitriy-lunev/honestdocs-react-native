import { instance } from './api';

const bookmarksAPI = {
  createBookmark(user_id, token, type, id) {
    return instance.post('/api/app/v2/users/123/bookmarks?', {
      user_id,
      token,
      type,
      id,
    });
  },
};

export default bookmarksAPI;
