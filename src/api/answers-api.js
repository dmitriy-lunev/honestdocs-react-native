import { instance } from './api';

const answersAPI = {
  getAnswers(commentable_id, user_id, token, page) {
    return instance.get('/api/app/v2/answers', {
      params: {
        commentable_type: 'question',
        commentable_id,
        user_id,
        token,
        page,
      },
    });
  },
  createAnswer(commentable_id, user_id, token, text) {
    return instance.post('/api/app/v2/answers?', {
      commentable_type: 'question',
      commentable_id,
      user_id,
      token,
      text,
    });
  },
};

export default answersAPI;
