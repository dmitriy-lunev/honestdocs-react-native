import { instance } from './api';

const ratingAPI = {
  setRating(user_id, token, value, ratingable_type, ratingable_id) {
    return instance.post('/api/v2/ratings?', {
      user_id,
      token,
      value,
      ratingable_type,
      ratingable_id,
    });
  },
};

export default ratingAPI;
