import { instance } from './api';

const questionsAPI = {
  getHomepageQuestions() {
    return instance.get('/api/app/v2/questions/homepage');
  },
  getQuestions(user_id, token, page) {
    return instance.get('/api/app/v2/questions', {
      params: { user_id, token, page },
    });
  },
  getQuestion(id, user_id, token) {
    return instance.get(`/api/app/v2/questions/${id}`, {
      params: { user_id, token },
    });
  },
  setOrUnsetQuestionLike(id, user_id, token, unset) {
    return instance.post(`/api/app/v2/questions/${id}/like?`, {
      user_id,
      token,
      unset,
    });
  },
  setOrUnsetQuestionDislike(id, user_id, token, unset) {
    return instance.post(`/api/app/v2/questions/${id}/dislike?`, {
      user_id,
      token,
      unset,
    });
  },
  claimOrUnclaimQuestion(id, user_id, token, unset) {
    return instance.post(`/api/app/v2/questions/${id}/claim?`, {
      user_id,
      token,
      unset,
    });
  },
  followOrUnfollowQuestion(id, user_id, token, unset) {
    return instance.post(`/api/app/v2/questions/${id}/follow?`, {
      user_id,
      token,
      unset,
    });
  },
  createQuestion(user_id, token, title, images, hospital_id, promo) {
    return instance.post(`/api/app/v2/users/${user_id}/questions?`, {
      token,
      title,
      images,
      hospital_id,
      promo,
    });
  },
};

export default questionsAPI;
