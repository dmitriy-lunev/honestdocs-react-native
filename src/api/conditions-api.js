import { instance } from './api';

const conditionsAPI = {
  getCategoryConditions() {
    return instance.get('/api/app/v2/category_conditions/trending');
  },
  getMostSearchedConditions(page, search = null) {
    return instance.get('/api/app/v2/category_conditions', {
      params: {
        page,
        search,
      },
    });
  },
  getConditionsByCategory(category_id) {
    return instance.get('/api/app/v2/conditions', {
      params: {
        category_id,
      },
    });
  },
  getCondition(drug_id) {
    return instance.get(`/api/app/v2/conditions/${drug_id}`);
  },
};

export default conditionsAPI;
