import { instance } from './api';

const commentsAPI = {
  getComments(commentable_type, commentable_id, page = null) {
    return instance.get('/api/app/v2/comments', {
      params: {
        commentable_type,
        commentable_id,
        page,
      },
    });
  },
  createComment(commentable_type, commentable_id, user_id, token, text) {
    return instance.post('/api/app/v2/comments?', {
      commentable_type,
      commentable_id,
      user_id,
      token,
      text,
    });
  },
};

export default commentsAPI;
