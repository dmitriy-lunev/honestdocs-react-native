import axios from "axios";

export const instance = axios.create({
  baseURL: "https://staging.honestdocs.co",
});
