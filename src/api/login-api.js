import { instance } from "./api";

const loginAPI = {
  sendOtp(mobile) {
    return instance.get(`/api/app/v2/logins/${mobile}/send_otp`);
  },
  checkOtp(mobile, code) {
    return instance.get(`/api/app/v2/logins/${mobile}/check_otp?code=${code}`);
  },
  deleteToken(mobile, token) {
    return instance.delete(`/api/app/v2/logins/${mobile}?token=${token}`);
  },
};

export default loginAPI;
