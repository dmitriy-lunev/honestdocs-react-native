import { instance } from './api';

const drugsAPI = {
  getCategoryDrugs() {
    return instance.get('/api/app/v2/category_drugs/trending');
  },
  getMostSearchedDrugs(page, search = null) {
    return instance.get('/api/app/v2/category_drugs', {
      params: {
        page,
        search,
      },
    });
  },
  getDrugsByCategory(category_id) {
    return instance.get('/api/app/v2/drugs', {
      params: {
        category_id,
      },
    });
  },
  getDrug(drug_id) {
    return instance.get(`/api/app/v2/drugs/${drug_id}`);
  },
};

export default drugsAPI;
