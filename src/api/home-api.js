import axios from 'axios';
import { instance } from './api';

const CancelToken = axios.CancelToken;
export let cancelGetBlogArticles;
export let cancelGetBlogArticlesByTag;

const homeAPI = {
  getBanners() {
    return instance.get('/api/app/v2/banners/homepage');
  },
  getShopCategories() {
    return instance.get('/api/app/v2/shop_categories');
  },
  getHomepageHospitals() {
    return instance.get('/api/app/v2/hospitals/homepage');
  },
  getHospitals(page) {
    return instance.get(`/api/app/v2/hospitals?page=${page}`);
  },
  getHospital(uuid) {
    return instance.get(`/api/app/v2/hospitals/${uuid}`);
  },
  getHomepageDoctorQA() {
    return instance.get('/api/app/v2/questions/homepage');
  },
  getBlogArticles(page) {
    return instance.get(`/api/app/v2/newsfeed?page=${page}`, {
      cancelToken: new CancelToken(function executor(c) {
        // An executor function receives a cancel function as a parameter
        cancelGetBlogArticles = c;
      }),
    });
  },
  getBlogArticlesTags() {
    return instance.get(`/api/app/v2/topic_tags`);
  },
  getBlogArticlesByTag(page, tag) {
    return instance.get(`/api/app/v2/newsfeed/by_tag/${tag}?page=${page}`, {
      cancelToken: new CancelToken(function executor(c) {
        // An executor function receives a cancel function as a parameter
        cancelGetBlogArticlesByTag = c;
      }),
    });
  },
  getBlogArticle(uuid) {
    return instance.get(`/api/app/v2/blog_articles/${uuid}`);
  },
  getComments(commentable_type, commentable_id) {
    return instance.get(
      `/api/app/v2/comments?commentable_type=${commentable_type}&commentable_id=${commentable_id}`,
    );
  },
};

export default homeAPI;
