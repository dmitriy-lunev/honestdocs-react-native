import { func, node, object } from 'prop-types';
import React, { useState } from 'react';
import { Pressable, Text, View } from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

const DropDownViewV2 = ({ renderHeader, headerContainerStyle, children }) => {
  const [opened, setOpened] = useState(false);
  const [contentHeight, setContentHeight] = useState(false);
  const translateY = useSharedValue(undefined);

  const onLayoutContent = (e) => {
    const contentH = e.nativeEvent.layout.height;
    if (!contentHeight) {
      setContentHeight(contentH);
      translateY.value = withTiming(0);
    }
  };

  const handleOpening = () => {
    if (opened) {
      translateY.value = withTiming(0);
    } else {
      translateY.value = withTiming(contentHeight);
    }
    setOpened(!opened);
  };

  const style = useAnimatedStyle(() => {
    return {
      height: translateY.value,
    };
  });

  return (
    <View>
      <View style={headerContainerStyle}>
        <Pressable onPress={handleOpening}>{renderHeader()}</Pressable>
      </View>
      <View style={{ overflow: 'hidden' }}>
        <Animated.View style={style} onLayout={onLayoutContent}>
          {children}
        </Animated.View>
      </View>
    </View>
  );
};

export default DropDownViewV2;

DropDownViewV2.propTypes = {
  renderHeader: func,
  headerContainerStyle: object,
  children: node,
};

DropDownViewV2.defaultProps = {
  renderHeader: () => null,
  headerContainerStyle: {},
  children: node,
};
