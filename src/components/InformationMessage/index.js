import React, { useState } from 'react';
import { Dimensions, Text, TouchableWithoutFeedback } from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import styles from './styles';

export const informationModalRef = React.createRef();

const { width } = Dimensions.get('window');
const MODAL_WIDTH = 350;
const MODAL_HEIGHT = 80;

const InformationMessage = () => {
  const translateY = useSharedValue(0);

  const [text, setText] = useState(null);

  const animatedStyles = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      top: -MODAL_HEIGHT,
      left: (width - MODAL_WIDTH) / 2,
      width: MODAL_WIDTH,
      height: MODAL_HEIGHT,
      backgroundColor: 'red',
      transform: [{ translateY: withTiming(translateY.value) }],
    };
  });

  informationModalRef.current = { renderContent: null };

  informationModalRef.current = {
    ...informationModalRef.current,
    openInfoMessage: (text) => {
      setText(text);
      translateY.value = 250;
      setTimeout(() => (translateY.value = 0), 4000);
    },
  };

  return (
    <TouchableWithoutFeedback onPress={() => (translateY.value = 0)}>
      <Animated.View style={[animatedStyles, styles.container]}>
        <Text style={{ textAlign: 'center', color: 'white' }}>{text}</Text>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

export default InformationMessage;
