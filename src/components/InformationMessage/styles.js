import { Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('window');
const MODAL_WIDTH = 350;
const MODAL_HEIGHT = 80;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: -MODAL_HEIGHT,
    left: (width - MODAL_WIDTH) / 2,
    width: MODAL_WIDTH,
    height: MODAL_HEIGHT,
    backgroundColor: 'red',
  },
});

export default styles;
