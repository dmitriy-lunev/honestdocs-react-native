import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  centeredModalView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.basicBgOpacity,
  },
  modalView: {
    paddingTop: 15,
    width: width - 30,
    backgroundColor: colors.white,
  },
  modalText: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 40,
    fontSize: 25,
    fontWeight: 'bold',
  },
  addImageVariantButton: {
    fontSize: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: colors.white,
  },
});

export default styles;
