import React from 'react';
import { Modal, TouchableWithoutFeedback, View } from 'react-native';
import { bool, func, node } from 'prop-types';
import styles from './styles';

const FadeCenteredModal = ({ visible, setVisible, children }) => {
  return (
    <Modal animationType="none" transparent={true} visible={visible}>
      <TouchableWithoutFeedback onPress={() => setVisible(!visible)}>
        <View style={styles.centeredModalView}>
          <TouchableWithoutFeedback>
            <View style={styles.modalView}>{children}</View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

export default FadeCenteredModal;

FadeCenteredModal.propTypes = {
  visible: bool,
  setVisible: func,
  children: node,
};
