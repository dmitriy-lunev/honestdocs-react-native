import { number } from 'prop-types';
import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

const ReloadingIndicator = ({ height }) => (
  <View style={[styles.container, { height }]}>
    <ActivityIndicator size="large" color="rgb(8, 130, 81)" />
  </View>
);

ReloadingIndicator.propTypes = {
  height: number,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
  },
});

export default ReloadingIndicator;
