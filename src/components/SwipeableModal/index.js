import { bool, func, node } from 'prop-types';
import React, { useEffect } from 'react';
import { Dimensions, Modal, View } from 'react-native';
import { PanGestureHandler } from 'react-native-gesture-handler';
import Animated, {
  Extrapolate,
  interpolate,
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useState } from 'react/cjs/react.development';

import styles from './styles';

const { height } = Dimensions.get('window');

const SwipeableModal = ({ visible, setVisible, children }) => {
  const translateY = useSharedValue(height);
  const [isVisible, setIsVisible] = useState(visible);

  const insets = useSafeAreaInsets();

  useEffect(() => {
    if (visible) {
      setIsVisible(true);
      translateY.value = withTiming(0, { duration: 500 });
    } else {
      translateY.value = withTiming(height, { duration: 500 }, () => {
        runOnJS(setIsVisible)(false);
      });
    }
  }, [visible]);

  const onGestureEvent = useAnimatedGestureHandler({
    onStart: (_, ctx) => {
      ctx.offsetY = translateY.value;
    },
    onActive: (event, ctx) => {
      if (event.translationY > 0) {
        translateY.value = ctx.offsetY + event.translationY;
      }
    },
    onEnd: () => {
      if (translateY.value < 0.3 * height) {
        translateY.value = withTiming(0);
      } else {
        runOnJS(setVisible)(false);
        translateY.value = withTiming(height, { duration: 500 }, () => {
          runOnJS(setIsVisible)(false);
        });
      }
    },
  });

  const style = useAnimatedStyle(() => {
    return {
      flex: 1,
      transform: [{ translateY: translateY.value }],
    };
  });

  const bgStyle = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      backgroundColor: 'black',
      opacity: interpolate(
        translateY.value,
        [0, height],
        [0.6, 0],
        Extrapolate.CLAMP,
      ),
    };
  });

  return (
    <Modal animationType="none" transparent={true} visible={isVisible}>
      <View
        style={[
          styles.container,
          isVisible
            ? { marginBottom: insets.bottom, paddingTop: insets.top }
            : {
                display: 'none',
              },
        ]}
      >
        <Animated.View style={bgStyle} />
        <PanGestureHandler {...{ onGestureEvent }}>
          <Animated.View {...{ style }}>
            <View style={styles.object}>{children}</View>
          </Animated.View>
        </PanGestureHandler>
      </View>
    </Modal>
  );
};

export default SwipeableModal;

SwipeableModal.propTypes = {
  visible: bool,
  setVisible: func,
  children: node,
};
