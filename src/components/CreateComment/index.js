import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Pressable, Text, View } from 'react-native';
import { connect } from 'react-redux';
import CreateCommentModal from './CreateCommentModal';
import { bool, func, number, string } from 'prop-types';
import styles from './styles';

const CreateComment = ({
  buttonText,
  commentableType,
  commentableId,
  onSubmit,
  isSignedIn,
  token,
  userId,

  visible,
  setVisible,
}) => {
  const { navigate } = useNavigation();
  // const [visible, setVisible] = useState(false);

  return (
    <>
      <Pressable
        onPress={() => {
          {
            if (!isSignedIn) {
              navigate('SingInStack');
            } else {
              setVisible(!visible);
            }
          }
        }}
      >
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonText}>{buttonText}</Text>
        </View>
      </Pressable>
      <CreateCommentModal
        {...{
          visible,
          setVisible,
          commentableType,
          commentableId,
          onSubmit,
          token,
          userId,
        }}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    isSignedIn: state.authentication.isSignedIn,
    token: state.authentication.userToken,
    userId: state.authentication.userInfo.id,
  };
};

export default connect(mapStateToProps)(CreateComment);

CreateComment.propTypes = {
  buttonText: string,
  commentableType: string,
  commentableId: string,
  onSubmit: func,
  isSignedIn: bool,
  token: string,
  userId: number,
};
