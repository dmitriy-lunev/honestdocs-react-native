import React, { useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import { connect } from 'react-redux';
import { useMutation } from 'react-query';
import { Formik } from 'formik';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { bool, func, number, string } from 'prop-types';

import { RNButton, SwipeableModal } from '../..';
import commentsAPI from '../../../api/comments-api';
import colors from '../../../theme/colors';
import styles from './styles';

const validate = (values) => {
  const errors = {};
  if (!values.comment) errors.comment = 'Required';
  return errors;
};

const CreateCommentModal = ({
  visible,
  setVisible,
  commentableType,
  commentableId,
  onSubmit,
  userId,
  token,
}) => {
  const mutation = useMutation(
    (text) => {
      return commentsAPI.createComment(
        commentableType,
        commentableId,
        userId,
        token,
        text,
      );
    },
    {
      onSuccess: (data) => {
        onSubmit(data);
        setVisible(!visible);
      },
    },
  );

  const _onSubmit = ({ comment }) => {
    mutation.mutate(comment);
  };

  useEffect(() => {
    if (!visible) {
      Keyboard.dismiss();
    }
  }, [visible]);

  return (
    <SwipeableModal visible={visible} setVisible={setVisible}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <KeyboardAvoidingView
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'center',
            }}
          >
            <View style={styles.dropDownIcon} />
            <View style={styles.modalContainer}>
              <Formik
                initialValues={{ comment: '' }}
                onSubmit={_onSubmit}
                validate={validate}
              >
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                }) => {
                  return (
                    <View>
                      <TextInput
                        multiline={true}
                        onChangeText={handleChange('comment')}
                        onBlur={handleBlur('comment')}
                        value={values.comment}
                        placeholder="Write your comment"
                        placeholderTextColor={colors.greyText}
                        style={styles.input}
                      />

                      <RNButton
                        label="Post reply"
                        disabled={
                          Object.keys(errors).length !== 0 ||
                          values.comment === ''
                        }
                        backgroundColor={colors.blue}
                        onPress={handleSubmit}
                      />
                    </View>
                  );
                }}
              </Formik>
              <TouchableOpacity
                style={styles.closeIconButton}
                onPress={() => {
                  setVisible(!visible);
                  Keyboard.dismiss();
                }}
              >
                <FontAwesomeIcon
                  name="angle-down"
                  size={30}
                  color={colors.green}
                />
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </View>
      </TouchableWithoutFeedback>
    </SwipeableModal>
  );
};

const mapStateToProps = (state) => ({
  userId: state.authentication.userInfo.id,
  token: state.authentication.userToken,
});

export default connect(mapStateToProps)(CreateCommentModal);

CreateCommentModal.propTypes = {
  visible: bool,
  setVisible: func,
  commentableType: string,
  commentableId: string || number,
  onSubmit: func,
  userId: number,
  token: string,
};

CreateCommentModal.defaultProps = {
  onSubmit: (data) => null,
};
