import { StyleSheet } from 'react-native';
import colors from '../../theme/colors';

const styles = StyleSheet.create({
  buttonContainer: {
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 3,
    backgroundColor: colors.concrete,
  },
  buttonText: {
    fontSize: 16,
    color: colors.basicText,
  },
});

export default styles;
