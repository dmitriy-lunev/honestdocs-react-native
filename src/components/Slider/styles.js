import { StyleSheet } from 'react-native';
import colors from '../../theme/colors';

const BORDER_RADIUS = 8;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    paddingTop: 20,
    marginHorizontal: 20,
  },
  headerTitle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  headerLink: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.green,
  },
  slide: {
    marginLeft: 15,
    marginVertical: 20,
    borderRadius: BORDER_RADIUS,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.3,
    shadowRadius: 8,
    elevation: 13,
  },
  slideContainer: {
    borderRadius: BORDER_RADIUS,
    overflow: 'hidden',
  },
});

export default styles;
