/* eslint-disable react/prop-types */
import React from 'react';
import { Dimensions, Text, View } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { number, bool, array, func, string, object } from 'prop-types';

import styles from './styles';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { navigate } from '../../services/navigation';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');

const Slider = ({
  data,
  itemWidth,
  loop,
  loopClonesPerSide,
  renderItem,
  title,
  path,
  params,
  withLink,
  containerStyle,
  ...rest
}) => {
  const navigation = useNavigation();

  const _renderItem = ({ item }) => {
    return (
      <View style={[styles.slide, { width: itemWidth - 15 }]}>
        <View style={styles.slideContainer}>{renderItem(item)}</View>
      </View>
    );
  };

  return (
    <View style={[styles.container, containerStyle]}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerTitle}>{title}</Text>
        {withLink && (
          <TouchableOpacity onPress={() => navigation.navigate(path, params)}>
            <Text style={styles.headerLink}>view all</Text>
          </TouchableOpacity>
        )}
      </View>
      <Carousel
        data={data}
        sliderWidth={width}
        itemWidth={itemWidth}
        activeSlideAlignment={'start'}
        inactiveSlideOpacity={1}
        inactiveSlideScale={1}
        loop={loop}
        loopClonesPerSide={loop ? loopClonesPerSide : undefined}
        keyExtractor={(item, index) =>
          `slider_${item.id.toString()}_${index.toString()}`
        }
        {...rest}
        renderItem={_renderItem}
      />
    </View>
  );
};

Slider.propTypes = {
  data: array,
  itemWidth: number,
  loop: bool,
  loopClonesPerSide: number,
  renderItem: func,
  title: string,
  path: string,
  params: object,
  withLink: bool,
  containerStyle: object,
};

Slider.defaultProps = {
  loop: false,
  loopClonesPerSide: 3,
  params: {},
};

export default Slider;
