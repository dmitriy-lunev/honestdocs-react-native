import React from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { object, node, string, bool } from 'prop-types';

import styles from './styles';
import colors from '../../theme/colors';

const SEARCH_HEIGHT = 90;

const SearchButton = ({ active, title, params, children }) => {
  const { navigate } = useNavigation();
  return (
    <TouchableWithoutFeedback onPress={() => navigate('SearchStack', params)}>
      <View style={styles.container}>
        {children || active ? (
          <View
            style={[
              styles.activeSearchContainer,
              { height: SEARCH_HEIGHT - 40 },
            ]}
          >
            <FontAwesomeIcon name="search" size={20} color={colors.blue} />
            <Text style={styles.activeSearchText}>{title}</Text>
          </View>
        ) : (
          <View style={styles.search}>
            <FontAwesomeIcon
              name="search"
              size={20}
              color={colors.navbarBorder}
            />
            <Text style={styles.text}>{title}</Text>
          </View>
        )}
      </View>
    </TouchableWithoutFeedback>
  );
};

SearchButton.propTypes = {
  active: bool,
  title: string,
  params: object,
  children: node,
};

SearchButton.defaultProps = {
  active: false,
  title: 'Search',
  params: {},
  children: null,
};

export default SearchButton;
