import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../theme/colors';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    paddingVertical: 20,
    backgroundColor: colors.green,
    justifyContent: 'center',
    alignItems: 'center',
  },
  search: {
    paddingVertical: 12,
    width: width - 30,
    backgroundColor: colors.extraGreen,
    alignItems: 'center',
    paddingHorizontal: 20,
    borderRadius: 8,
    flexDirection: 'row',
  },
  text: {
    color: colors.navbarBorder,
    fontSize: 20,
    paddingLeft: 5,
  },
  refreshControl: {
    color: 'green',
    width: 100,
    height: 100,
    backgroundColor: 'red',
  },
  activeSearchContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    width: '90%',
  },
  activeSearchText: {
    paddingLeft: 15,
    color: 'grey',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
