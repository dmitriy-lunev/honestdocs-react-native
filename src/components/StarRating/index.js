import React, { useState } from 'react';
import { Pressable, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import { setRatingFetch } from '../../redux/modules/Drugs/actions';
import colors from '../../theme/colors';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

const STAR_COUNT = 5;

const iconColors = {
  'star-o': colors.emptyColor,
  star: colors.green,
};

const StarRating = ({
  title,
  rating,
  ratingableType,
  ratingableId,
  starSize,
  isSignedIn,
  setRating,
}) => {
  const { navigate } = useNavigation();
  const [currentRating, setCurrentRating] = useState(rating);

  const handlePress = (starRating) => {
    if (!isSignedIn) {
      navigate('SingInStack');
    } else {
      setCurrentRating(starRating);
      setRating(starRating, ratingableType, ratingableId);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.starContainer}>
        {Array(STAR_COUNT)
          .fill(null)
          .map((_, index) => {
            const name = index + 1 <= currentRating ? 'star' : 'star-o';
            return (
              <Pressable
                key={index.toString()}
                onPress={() => handlePress(index + 1)}
              >
                <Icon
                  name={name}
                  size={starSize}
                  color={iconColors[name]}
                  style={styles.star}
                />
              </Pressable>
            );
          })}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  isSignedIn: state.authentication.isSignedIn,
});

const mapDispatchToProps = {
  setRating: setRatingFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(StarRating);
