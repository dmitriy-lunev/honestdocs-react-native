import { StyleSheet } from 'react-native';
import colors from '../../theme/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    marginBottom: 10,
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.green,
  },
  starContainer: {
    flexDirection: 'row',
  },
  star: {
    marginHorizontal: 2,
  },
});

export default styles;
