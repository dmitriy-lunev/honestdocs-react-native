import React, { useEffect, useRef, useState } from 'react';
import { Text, View, Animated } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './styles';

const DropDownView = ({ title, renderHeader, dropHeaderHeight, children }) => {
  const [isMounted, setIsMounted] = useState(false);
  const [contentVisible, setContentVisible] = useState(false);
  const [headerHeight, setHeaderHeight] = useState(dropHeaderHeight + 30);
  const [contentHeight, setContentHeight] = useState(0);
  let animated = useRef(new Animated.Value(headerHeight)).current;

  const onLayoutHeader = (e) => {
    const headerH = e.nativeEvent.layout.height;
    if (!contentVisible) {
      setHeaderHeight(headerH);
    }
  };

  const onLayoutContent = (e) => {
    const contentH = e.nativeEvent.layout.height;

    if (contentHeight < contentH) {
      setContentHeight(contentH);
    }
  };

  const runAnimation = () => {
    const finalValue = contentVisible
      ? headerHeight
      : headerHeight + contentHeight;

    Animated.timing(animated, {
      toValue: finalValue,
      useNativeDriver: false,
    }).start();
  };

  const onPress = () => {
    setContentVisible(!contentVisible);
    runAnimation();
  };

  return (
    <Animated.View style={[styles.container, { height: animated }]}>
      <View
        style={[styles.headerContainer, { height: dropHeaderHeight + 30 }]}
        onLayout={onLayoutHeader}
      >
        {renderHeader(onPress, contentVisible)}
      </View>
      <View onLayout={onLayoutContent}>{children}</View>
    </Animated.View>
  );
};

export default DropDownView;
