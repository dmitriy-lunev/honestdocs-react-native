import { number, object, string } from 'prop-types';
import React, { useCallback } from 'react';
import { Text, TouchableWithoutFeedback, View } from 'react-native';
import { useState } from 'react/cjs/react.development';

const HideableText = ({
  numberOfLines,
  text,
  containerStyle,
  textStyle,
  buttonStyle,
}) => {
  const [showMore, setShowMore] = useState(false);
  const [textShown, setShowText] = useState(false);
  const toggleShowText = () => {
    setShowText(!textShown);
  };

  const onTextLayout = useCallback((e) => {
    if (e.nativeEvent.lines.length > 4) {
      setShowMore(!showMore);
    }
  }, []);

  return (
    <View style={containerStyle}>
      <Text
        style={textStyle}
        numberOfLines={textShown ? null : showMore ? numberOfLines : null}
        onTextLayout={onTextLayout}
      >
        {text}
      </Text>
      {showMore && (
        <TouchableWithoutFeedback onPress={toggleShowText}>
          <Text style={buttonStyle}>
            {textShown ? 'Show less' : 'Show more'}
          </Text>
        </TouchableWithoutFeedback>
      )}
    </View>
  );
};

export default HideableText;

HideableText.propTypes = {
  numberOfLines: number,
  text: string,
  containerStyle: object,
  textStyle: object,
  buttonStyle: object,
};

HideableText.defaultProps = {
  numberOfLines: 4,
  containerStyle: {},
};
