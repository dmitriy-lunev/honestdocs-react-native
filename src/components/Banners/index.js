import React from 'react';
import { Dimensions, Image, View } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { array, bool } from 'prop-types';

import ReloadingIndicator from '../ReloadingIndicator';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

const { width } = Dimensions.get('window');

const Banners = ({ loading, loaded, banners }) => {
  const attitude = 274 / 764;
  const navigation = useNavigation();

  if (loading || !loaded) {
    const height = banners.length * ((width - 30) * attitude + 15) + 15;
    return <ReloadingIndicator height={height} />;
  }

  return (
    <View style={styles.container}>
      {banners.map((item, index) => {
        return (
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('HDmall')}
            key={index}
            style={styles.bannerContainer}
          >
            <Image
              width={width - 30}
              height={(width - 30) * attitude}
              style={[
                styles.image,
                { width: width - 30, height: (width - 30) * attitude },
              ]}
              source={{
                uri: item
                  ? `https://staging.honestdocs.co${item}`
                  : 'https://naurok-test.nyc3.cdn.digitaloceanspaces.com/uploads/test/510013/91297/676499_1585497084.jpg',
              }}
            />
          </TouchableWithoutFeedback>
        );
      })}
    </View>
  );
};

Banners.propTypes = {
  loading: bool,
  loaded: bool,
  banners: array,
};

export default Banners;
