import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { string, number } from 'prop-types';

const RNIcon = ({ name, color, size }) => {
  return <Icon name={name} color={color} size={size} />;
};

RNIcon.propTypes = {
  name: string,
  color: string,
  size: number,
};

export default RNIcon;
