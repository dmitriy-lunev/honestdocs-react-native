import React from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { number } from 'prop-types';

import styles from './styles';
import { roundAvgReview } from '../../helpers/roundAvgReview';
import colors from '../../theme/colors';

const iconColors = {
  'star-half-empty': colors.green,
  'star-o': colors.emptyColor,
  star: colors.green,
};

const StarsReview = ({ avgReview, starsCount, starSize }) => {
  const stars = Array(starsCount).fill(null);
  const cur = roundAvgReview(avgReview);
  return (
    <View style={styles.container}>
      {stars.map((_, index) => {
        const name =
          index + 1 > cur
            ? index + 0.5 === cur
              ? 'star-half-empty'
              : 'star-o'
            : 'star';
        return (
          <Icon
            name={name}
            size={starSize}
            color={iconColors[name]}
            key={index.toString()}
          />
        );
      })}
    </View>
  );
};

StarsReview.propTypes = {
  avgReview: number,
  starsCount: number,
  starSize: number,
};

StarsReview.defaultProps = {
  starsCount: 5,
  starSize: 20,
};

export default StarsReview;
