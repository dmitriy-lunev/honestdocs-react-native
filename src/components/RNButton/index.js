import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { bool, func, string } from 'prop-types';

import colors from '../../theme/colors';
import styles from './styles';

const RNButton = ({ label, disabled, backgroundColor, onPress }) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress} activeOpacity={0.7}>
      <View
        style={[
          styles.container,
          disabled ? { backgroundColor: colors.grey } : { backgroundColor },
        ]}
      >
        <Text style={styles.labelText}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default RNButton;

RNButton.propTypes = {
  label: string,
  disabled: bool,
  backgroundColor: string,
  onPress: func,
};
