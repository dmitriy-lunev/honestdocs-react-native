import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 20,
  },
  titleText: {
    marginTop: 50,
    marginVertical: 3,
    fontSize: 35,
    fontWeight: 'bold',
  },
  inputContainer: {
    marginTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  input: {
    height: 50,
    width: (width - 5 * 20) / 4,
    justifyContent: 'center',
    textAlign: 'center',
    borderRadius: 5,
    fontSize: 30,
    backgroundColor: colors.grey,
  },
  inputFocused: {
    borderWidth: 2,
    borderColor: colors.green,
  },
  timerContainer: {
    marginTop: 30,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
  },
  resendButtonText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.blue,
  },
  timerText: {
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.greyText,
  },
  text: {
    marginTop: 15,
    width: '100%',
    marginBottom: 20,
    fontSize: 15,
    color: colors.greyText,
    textAlign: 'center',
  },
  errorText: {
    marginVertical: 8,
    height: 15,
    width: '100%',
    fontSize: 15,
    lineHeight: 15,
    fontWeight: 'bold',
    color: colors.errorColor,
    textAlign: 'center',
  },
});

export default styles;
