import { Formik } from 'formik';
import { func, string } from 'prop-types';
import React, { useEffect, useRef, useState } from 'react';
import { View, Text } from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';

import RNButton from '../../../components/RNButton';
import {
  checkOtpFetch,
  sendOtpFetch,
} from '../../../redux/modules/Authentication/actions';
import colors from '../../../theme/colors';
import styles from './styles';

const validate = (values) => {
  const errors = {};

  if (!values.otp1 || !values.otp2 || !values.otp3 || !values.otp4)
    errors.otp = 'Must be at least 4 characters';
  if (!values.otp1) errors.otp = 'Required';

  return errors;
};

const CheckOtp = ({ userMobile, sendOtp, checkOtp }) => {
  const ref1 = useRef();
  const ref2 = useRef();
  const ref3 = useRef();
  const ref4 = useRef();
  const [focused, setFocused] = useState('');
  const [timer, setTimer] = useState(100);
  let timerId;

  // TODO: rewrite timer

  useEffect(() => {
    if (timer !== 0) tick();
    return () => clearTimeout(timerId);
  }, [timer]);

  const tick = () => {
    timerId = setTimeout(() => setTimer((timer) => timer - 1), 1000);
  };

  const secondsToTime = (secs) => {
    let minutes = Math.floor(secs / 60);

    let divisor_for_seconds = secs % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      minutes: minutes < 10 ? `0${minutes}` : `${minutes}`,
      seconds: seconds < 10 ? `0${seconds}` : `${seconds}`,
    };
    return obj;
  };

  const onSubmit = ({ otp1, otp2, otp3, otp4 }, formik) => {
    checkOtp(`${otp1}${otp2}${otp3}${otp4}`).catch((error) =>
      formik.setErrors({ otp: error.response.data.description }),
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.titleText}>Enter OTP</Text>
      <Formik
        initialValues={{ otp1: null, otp2: null, otp3: null, otp4: null }}
        onSubmit={onSubmit}
        validate={validate}
      >
        {(formik) => {
          const {
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            touched,
            errors,
          } = formik;
          return (
            <View>
              <View style={styles.inputContainer}>
                <TextInput
                  caretHidden={true}
                  maxLength={1}
                  onChangeText={(text) => {
                    handleChange('otp1')(text);
                    if (text) ref2.current.focus();
                  }}
                  onFocus={() => {
                    setFocused('input1');
                    if (values.otp1) ref2.current.focus();
                  }}
                  onBlur={handleBlur('otp1')}
                  value={values.otp1}
                  keyboardType="phone-pad"
                  style={[
                    styles.input,
                    focused === 'input1' ? styles.inputFocused : null,
                  ]}
                  ref={ref1}
                />
                <TextInput
                  caretHidden={true}
                  maxLength={1}
                  onChangeText={(text) => {
                    handleChange('otp2')(text);
                    if (text) ref3.current.focus();
                  }}
                  onKeyPress={(e) => {
                    if (!values.otp4 && e.nativeEvent.key === 'Backspace') {
                      values.otp1 = '';
                      ref1.current.focus();
                    }
                  }}
                  onFocus={() => {
                    setFocused('input2');
                    if (values.otp2) ref3.current.focus();
                    if (!values.otp2 && !values.otp1) ref1.current.focus();
                  }}
                  onBlur={handleBlur('otp2')}
                  value={values.otp2}
                  keyboardType="phone-pad"
                  ref={ref2}
                  style={[
                    styles.input,
                    focused === 'input2' ? styles.inputFocused : null,
                  ]}
                />
                <TextInput
                  caretHidden={true}
                  maxLength={1}
                  onChangeText={(text) => {
                    handleChange('otp3')(text);
                    if (text) ref4.current.focus();
                  }}
                  onKeyPress={(e) => {
                    if (!values.otp4 && e.nativeEvent.key === 'Backspace') {
                      values.otp2 = '';
                      ref2.current.focus();
                    }
                  }}
                  onFocus={() => {
                    setFocused('input3');
                    if (values.otp3) ref4.current.focus();
                    if (!values.otp3 && !values.otp2) ref2.current.focus();
                  }}
                  onBlur={handleBlur('otp3')}
                  value={values.otp3}
                  keyboardType="phone-pad"
                  ref={ref3}
                  style={[
                    styles.input,
                    focused === 'input3' ? styles.inputFocused : null,
                  ]}
                />
                <TextInput
                  caretHidden={true}
                  maxLength={1}
                  onChangeText={(text) => {
                    handleChange('otp4')(text);
                    if (text) {
                      onSubmit({ ...values, otp4: text }, formik);
                    }
                  }}
                  onKeyPress={(e) => {
                    if (!values.otp4 && e.nativeEvent.key === 'Backspace') {
                      values.otp3 = '';
                      ref3.current.focus();
                    }
                  }}
                  onFocus={() => {
                    setFocused('input4');
                    if (!values.otp4 && !values.otp3) ref3.current.focus();
                  }}
                  onBlur={handleBlur('otp4')}
                  value={values.otp4}
                  keyboardType="phone-pad"
                  ref={ref4}
                  style={[
                    styles.input,
                    focused === 'input4' ? styles.inputFocused : null,
                  ]}
                />
              </View>
              <Text style={styles.errorText}>
                {errors.otp && touched.otp4 && errors.otp}
              </Text>
              <Text style={styles.text}>
                We've sent a one-time password to {userMobile}
              </Text>
              <RNButton
                label="Log In Now"
                disabled={Object.keys(errors).length !== 0 || !values.otp1}
                backgroundColor={colors.green}
                onPress={handleSubmit}
              />
            </View>
          );
        }}
      </Formik>
      <View style={styles.timerContainer}>
        <Text style={styles.timerText}>Didn't get the code?</Text>
        {timer ? (
          <>
            <Text style={styles.timerText}>
              {' Re-send it '}
              {`${secondsToTime(timer).minutes} : ${
                secondsToTime(timer).seconds
              }`}
            </Text>
          </>
        ) : (
          <TouchableOpacity
            onPress={() => {
              sendOtp(userMobile).then(() => setTimer(120));
            }}
          >
            <Text style={styles.resendButtonText}>{` Re-send`}</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    userMobile: state.authentication.userMobile,
  };
};

const mapDispatchToProps = {
  sendOtp: sendOtpFetch,
  checkOtp: checkOtpFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckOtp);

CheckOtp.propTypes = {
  userMobile: string,
  sendOtp: func,
  checkOtp: func,
};
