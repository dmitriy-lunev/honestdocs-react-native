import React from 'react';
import { connect } from 'react-redux';
import {
  View,
  Text,
  ImageBackground,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Formik } from 'formik';
import { TextInput } from 'react-native-gesture-handler';
import * as Yup from 'yup';
import { func } from 'prop-types';

import RNButton from '../../../components/RNButton';
import { sendOtpFetch } from '../../../redux/modules/Authentication/actions';
import colors from '../../../theme/colors';
import styles from './styles';

const SendOtpSchema = Yup.object().shape({
  mobile: Yup.string()
    .min(10, 'Must be at least 10 characters')
    .max(10, 'Must be no more than 10 characters')
    .required('Required'),
});

const SendOtp = ({ sendOtp }) => {
  const navigation = useNavigation();

  const onSubmit = (values, actions) => {
    sendOtp(values.mobile)
      .then(() => navigation.push('SingInStack', { screen: 'CheckOtp' }))
      .catch((error) =>
        actions.setErrors({ mobile: error.response.data.description }),
      );
  };

  return (
    <ImageBackground
      source={require('../../../assets/images/auth_bg.png')}
      style={styles.image}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.container}
      >
        <Text style={styles.titleText}>Welcome</Text>
        <Text style={styles.titleText}>to honestdocs</Text>
        <Formik
          initialValues={{ mobile: '' }}
          onSubmit={onSubmit}
          validationSchema={SendOtpSchema}
        >
          {({ handleChange, handleBlur, handleSubmit, values, errors }) => {
            return (
              <View>
                <TextInput
                  onChangeText={handleChange('mobile')}
                  onBlur={handleBlur('mobile')}
                  value={values.mobile}
                  placeholder="Enter your mobile number"
                  placeholderTextColor={colors.greyText}
                  keyboardType="phone-pad"
                  style={styles.input}
                />
                <Text style={styles.errorText}>
                  {errors.mobile && errors.mobile}
                </Text>
                <RNButton
                  label="Log In Now"
                  disabled={Object.keys(errors).length !== 0 || !values.mobile}
                  backgroundColor={colors.green}
                  onPress={handleSubmit}
                />
              </View>
            );
          }}
        </Formik>
      </KeyboardAvoidingView>
    </ImageBackground>
  );
};

const mapDispatchToProps = {
  sendOtp: sendOtpFetch,
};

export default connect(null, mapDispatchToProps)(SendOtp);

SendOtp.propTypes = {
  sendOtp: func,
};
