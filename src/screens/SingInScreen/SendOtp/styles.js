import { StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: colors.darkOpacity,
    paddingHorizontal: 15,
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
  titleText: {
    marginVertical: 3,
    color: colors.white,
    fontSize: 35,
    fontWeight: 'bold',
  },
  input: {
    height: 50,
    marginTop: 30,
    paddingHorizontal: 15,
    borderRadius: 5,
    fontSize: 20,
    backgroundColor: 'white',
    fontWeight: 'bold',
  },
  errorText: {
    marginVertical: 5,
    color: colors.errorColor,
    fontWeight: 'bold',
    lineHeight: 14,
    height: 14,
    textAlign: 'center',
  },
});

export default styles;
