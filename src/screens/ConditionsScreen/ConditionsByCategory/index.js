import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { array, func } from 'prop-types';

import { SearchButton } from '../../../components';
import styles from './styles';
import {
  loadConditionsByCategoryFetch,
  clearConditions,
} from '../../../redux/modules/Conditions/actions';

const ConditionsByCategory = ({
  conditions,
  loadConditionsByCategory,
  clearConditions,
}) => {
  const { navigate } = useNavigation();
  const route = useRoute();
  const { categoryId, categoryName } = route?.params;

  useEffect(() => {
    loadConditionsByCategory(categoryId);
    return clearConditions;
  }, []);

  const handlePress = (conditionId) => {
    navigate('ConditionsStackScreen', {
      screen: 'Condition',
      params: { conditionId },
    });
  };

  return (
    <View style={styles.container}>
      <SearchButton />
      <ScrollView style={styles.scrollContainer}>
        <Text style={styles.title}>{categoryName}</Text>
        {conditions.map((item) => {
          return (
            <TouchableOpacity
              key={item.id.toString()}
              onPress={() => handlePress(item.id)}
            >
              <Text style={styles.itemText}>{item.mainTitle}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  conditions: state.conditions.conditions,
});

const mapDispatchToProps = {
  loadConditionsByCategory: loadConditionsByCategoryFetch,
  clearConditions,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConditionsByCategory);

ConditionsByCategory.propTypes = {
  conditions: array,
  loadConditionsByCategory: func,
  clearConditions: func,
};
