import { useNavigation } from '@react-navigation/native';
import { array, bool, func, string } from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Pressable, Share, Text, View } from 'react-native';
import { connect } from 'react-redux';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { CreateComment } from '../../../components';
import {
  loadConditionCommentsFetch,
  setConditionBookmarkFetch,
} from '../../../redux/modules/Conditions/actions';
import colors from '../../../theme/colors';
import styles from './styles';

const ConditionBottom = ({
  conditionId,
  isSignedIn,
  bookmarked,
  mainTitle,
  comments,
  loadConditionComments,
  setConditionBookmark,
}) => {
  const { navigate } = useNavigation();
  const [createCommentModalVisible, setCreateCommentModalVisible] = useState(
    false,
  );

  useEffect(() => {
    loadConditionComments(conditionId);
    return () => {};
  }, []);

  const onShare = async (message) => {
    try {
      const result = await Share.share({
        message,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View>
      <View style={[styles.flexRow, styles.bottomButtonsContainer]}>
        <View style={styles.flexRow}>
          <Text style={styles.commentsCountText}>{comments.length}</Text>
          <OcticonsIcon name="comment" size={20} color={colors.green} />
        </View>
        <View style={styles.flexRow}>
          <Pressable onPress={() => onShare(mainTitle)}>
            <EntypoIcon
              name="share-alternative"
              size={20}
              color={colors.green}
              style={styles.icon}
            />
          </Pressable>
          <Pressable
            onPress={() => {
              if (!isSignedIn) {
                navigate('SingInStack');
              } else {
                setConditionBookmark(conditionId);
              }
            }}
          >
            <FontAwesomeIcon
              name={bookmarked ? 'bookmark' : 'bookmark-o'}
              size={20}
              color={colors.green}
              style={styles.icon}
            />
          </Pressable>
        </View>
      </View>
      <CreateComment
        buttonText="Post a comment"
        commentableType="condition"
        commentableId={conditionId}
        visible={createCommentModalVisible}
        setVisible={setCreateCommentModalVisible}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  comments: state.conditions.conditionComments,
  isSignedIn: state.authentication.isSignedIn,
  bookmarked: state.conditions.condition.bookmarked,
  mainTitle: state.conditions.condition.mainTitle,
});

const mapDispatchToProps = {
  loadConditionComments: loadConditionCommentsFetch,
  setConditionBookmark: setConditionBookmarkFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConditionBottom);

ConditionBottom.propTypes = {
  conditionId: string,
  isSignedIn: bool,
  bookmarked: bool,
  mainTitle: string,
  comments: array,
  loadConditionComments: func,
  setConditionBookmark: func,
};
