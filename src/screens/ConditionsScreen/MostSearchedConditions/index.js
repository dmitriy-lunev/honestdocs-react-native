import React, { useEffect, useState } from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { array, func } from 'prop-types';

import { SearchButton } from '../../../components';
import DrugsCategoriesFilter from './ConditionsCategoriesFilter';
import { loadMostSearchedConditionsFetch } from '../../../redux/modules/Conditions/actions';
import MostSearchedItem from './MostSearchedIItem';
import styles from './styles';

const MostSearchedDrugs = ({
  mostSearchedConditions,
  loadMostSearchedConditions,
}) => {
  const { navigate } = useNavigation();
  const [categoryModalVisible, setCategoryModalVisible] = useState(false);

  useEffect(() => {
    loadMostSearchedConditions();
  }, []);

  const handlePress = (conditionId) => {
    navigate('ConditionsStackScreen', {
      screen: 'Condition',
      params: { conditionId },
    });
  };

  const renderCarouselItem = ({ item }) => {
    return (
      <View style={styles.carouselItemContainer}>
        {item.map((drug) => (
          <TouchableOpacity
            key={drug.id}
            activeOpacity={0.7}
            onPress={() => handlePress(drug.id)}
          >
            <Text style={styles.drugText}>{drug.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const renderItem = (item) => {
    const { name, conditions } = item.item;

    if (conditions.length === 0) {
      return null;
    }

    let data = [];
    let dataItem = [];
    conditions.forEach((item, index) => {
      if ((index + 1) % 5 !== 0) {
        dataItem.push(item);
        if (index + 1 === conditions.length) {
          data.push(dataItem);
        }
      } else {
        dataItem.push(item);
        data.push(dataItem);
        dataItem = [];
      }
    });

    return <MostSearchedItem {...{ name, data, renderCarouselItem }} />;
  };

  return (
    <View style={styles.container}>
      <SearchButton
        active={true}
        title="Search for a drug name"
        params={{ searchCategory: 'drugs' }}
      />
      <DrugsCategoriesFilter
        visible={categoryModalVisible}
        setVisible={setCategoryModalVisible}
      />
      <FlatList
        data={mostSearchedConditions}
        renderItem={renderItem}
        onEndReached={() => loadMostSearchedConditions()}
        keyExtractor={(item) => `flatlist_${item.id}`}
        ListHeaderComponent={() => (
          <View style={styles.listHeaderComponentContainer}>
            <Text style={styles.drugsTitle}>Conditions by category</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  loading: state.conditions.loading,
  mostSearchedConditions: state.conditions.mostSearchedConditions,
});

const mapDispatchToProps = {
  loadMostSearchedConditions: loadMostSearchedConditionsFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(MostSearchedDrugs);

MostSearchedDrugs.propTypes = {
  mostSearchedConditions: array,
  loadMostSearchedConditions: func,
};
