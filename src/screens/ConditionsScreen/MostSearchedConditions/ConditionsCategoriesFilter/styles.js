import { StyleSheet } from 'react-native';
import colors from '../../../../theme/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: colors.green,
    paddingRight: 10,
  },
  item: {
    alignItems: 'center',
    paddingHorizontal: 4,
    paddingTop: 5,
    paddingBottom: 20,
  },
  currentItem: {
    borderBottomColor: colors.superGrey,
    borderBottomWidth: 5,
  },
  itemText: {
    color: colors.superGrey,
    lineHeight: 15,
    fontSize: 15,
    fontWeight: 'bold',
  },
  menuButton: {
    paddingHorizontal: 8,
  },
});

export default styles;
