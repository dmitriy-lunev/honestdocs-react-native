import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { array, bool, func } from 'prop-types';

import { loadConditionsCategoriesFetch } from '../../../../redux/modules/Conditions/actions';
import CategoriesModal from '../CategoriesModal';
import colors from '../../../../theme/colors';
import styles from './styles';

const ConditionsCategoriesFilter = ({
  visible,
  setVisible,
  categories,
  loadConditionsCategories,
}) => {
  const { navigate } = useNavigation();

  useEffect(() => {
    loadConditionsCategories();
    return () => {};
  }, []);

  const onPressHandler = (categoryId, categoryName) => {
    navigate('ConditionsStackScreen', {
      screen: 'ConditionsByCategory',
      params: { categoryId, categoryName },
    });
  };

  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={true}
        bounces={false}
        showsHorizontalScrollIndicator={false}
      >
        <TouchableOpacity
          style={[styles.item, styles.currentItem]}
          onPress={() => true}
        >
          <Text style={styles.itemText}>Most searched</Text>
        </TouchableOpacity>
        {categories.map((item) => {
          return (
            <TouchableOpacity
              key={`filter_${item.id}`}
              style={[styles.item]}
              onPress={() => onPressHandler(item.id, item.name)}
            >
              <Text style={styles.itemText}>{item.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
      <TouchableOpacity
        style={styles.menuButton}
        onPress={() => setVisible(true)}
      >
        <FeatherIcon name="menu" size={26} color={colors.superGrey} />
      </TouchableOpacity>
      <CategoriesModal {...{ visible, setVisible }} />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    loaded: state.conditions.loaded,
    loading: state.conditions.loading,
    categories: state.conditions.categories,
  };
};

const mapDispatchToProps = {
  loadConditionsCategories: loadConditionsCategoriesFetch,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConditionsCategoriesFilter);

ConditionsCategoriesFilter.propTypes = {
  visible: bool,
  setVisible: func,
  categories: array,
  loadConditionsCategories: func,
};
