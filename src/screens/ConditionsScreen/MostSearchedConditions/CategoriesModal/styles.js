import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  dropDownIcon: {
    marginVertical: 10,
    height: 5,
    width: 50,
    borderRadius: 5,
    backgroundColor: colors.white,
  },
  modalContainer: {
    flex: 1,
    width,
    paddingHorizontal: 20,
    paddingTop: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
  },
  title: {
    marginTop: 15,
    fontSize: 20,
    fontWeight: 'bold',
  },
  closeIconButton: {
    position: 'absolute',
    top: 25,
    right: 20,
  },
  itemsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 30,
  },
  itemContainer: {
    width: (width - 40) / 3 - 15,
    marginRight: 15,
    marginBottom: 20,
    paddingVertical: 10,
    backgroundColor: colors.extraLightBlue,
  },
  itemText: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.blue,
  },
});

export default styles;
