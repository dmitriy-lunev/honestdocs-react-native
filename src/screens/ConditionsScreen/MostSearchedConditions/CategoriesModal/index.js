import React from 'react';
import { connect } from 'react-redux';
import { Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { array, bool, func } from 'prop-types';

import { SwipeableModal } from '../../../../components';
import colors from '../../../../theme/colors';
import styles from './styles';

const CategoriesModal = ({ visible, setVisible, categories }) => {
  const { navigate } = useNavigation();

  const handlePress = (categoryId, categoryName) => {
    setVisible(!visible);
    navigate('ConditionsStackScreen', {
      screen: 'ConditionsByCategory',
      params: { categoryId, categoryName },
    });
  };

  const renderItem = (item) => {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={() => handlePress(item.id, item.name)}
        key={item.id}
      >
        <View style={styles.itemContainer}>
          <Text style={styles.itemText}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SwipeableModal visible={visible} setVisible={setVisible}>
      <View style={styles.container}>
        <View style={styles.dropDownIcon} />
        <View style={styles.modalContainer}>
          <Text style={styles.title}>Choose category</Text>
          <View style={styles.itemsContainer}>
            {categories.map(renderItem)}
          </View>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.closeIconButton}
            onPress={() => setVisible(!visible)}
          >
            <FontAwesomeIcon name="angle-down" size={30} color={colors.green} />
          </TouchableOpacity>
        </View>
      </View>
    </SwipeableModal>
  );
};

const mapStateToProps = (state) => {
  return {
    categories: state.conditions.categories,
  };
};

export default connect(mapStateToProps)(CategoriesModal);

CategoriesModal.propTypes = {
  visible: bool,
  setVisible: func,
  categories: array,
};
