import { StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  searchContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    width: '90%',
  },
  searchText: {
    paddingLeft: 15,
    color: 'grey',
    fontSize: 20,
    fontWeight: 'bold',
  },
  drugsTitle: {
    marginTop: 30,
    marginLeft: 20,
    fontSize: 18,
    fontWeight: 'bold',
  },
  categoryTitle: {
    marginLeft: 20,
    marginBottom: 20,
    fontSize: 18,
    fontWeight: 'bold',
  },
  carouselItemContainer: {
    paddingBottom: 20,
    paddingLeft: 20,
  },
  drugText: {
    marginBottom: 10,
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.darkBlue,
  },
  itemContainer: {
    paddingTop: 15,
    backgroundColor: colors.white,
  },
  listHeaderComponentContainer: {
    backgroundColor: colors.white,
  },
  loadingContainer: {
    flex: 1,
    backgroundColor: colors.white,
  },
  separator: {
    height: 10,
    backgroundColor: colors.concrete,
  },
});

export default styles;
