import React, { useEffect, useState } from 'react';
import { Image, Linking, Modal, ScrollView, Text, View } from 'react-native';
import { connect } from 'react-redux';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { loadUserInfoFetch } from '../../../redux/modules/User/actions';

import styles from './styles';
import colors from '../../../theme/colors';
import { logOutFetch } from '../../../redux/modules/Authentication/actions';
import { func, object } from 'prop-types';
import { gestureHandlerRootHOC, TouchableOpacity } from 'react-native-gesture-handler';

const sections = [
  { title: 'My Profile', iconName: '', path: 'Profile' },
  { title: 'My Medical Records', iconName: '', path: 'MedicalRecord' },
  { title: 'My HealthStats', iconName: '', path: 'HealthStats' },
  { title: 'My HealthCoins', iconName: '', path: 'HealthCoins' },
  { title: 'My Questions', iconName: '', path: 'Questions' },
  { title: 'My Answers', iconName: '', path: 'Answers' },
  { title: 'My Bookmarks', iconName: '', path: 'Bookmarks' },
  { title: 'Settings', iconName: '', path: 'ProfileSettings' },
  { title: 'Contact Support', iconName: '', path: 'https://lodash.com/' },
  { title: 'Our policies', iconName: '', path: 'OurPolicies' },
  { title: 'Logout', iconName: '', path: 'Logout' },
];

const ProfileMenu = ({ navigation, basicInfo, loadUserInfo, logOut }) => {
  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    if (Object.keys(basicInfo).length === 0) loadUserInfo('basic_info');
  }, []);

  const { first_name: firstName, last_name: lastName, avatar } = basicInfo;

  const onPressHandler = (path) => {
    if (path === 'Logout') {
      setModalVisible(true);
      return;
    } else if (path === 'https://lodash.com/') {
      Linking.openURL(path);
      return;
    }
    navigation.push('ProfileStack', { screen: path });
  };

  const handleLogOut = () => {
    logOut().then(() => {
      setModalVisible(false);
      navigation.popToTop();
    });
  };

  const renderModal = gestureHandlerRootHOC(() => (
    <View style={styles.logoutModalContainer}>
      <View style={styles.logoutModal}>
        <View style={styles.modalTextContainer}>
          <Text style={styles.modalText}>Are you sure</Text>
          <Text style={styles.modalText}>you would like to log out?</Text>
        </View>
        <View style={styles.modalButtonsHorizontalBorder} />
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => setModalVisible(false)}
            style={styles.modalButton}
          >
            <Text style={styles.buttonText}>Cancel</Text>
          </TouchableOpacity>
          <View style={styles.modalButtonsVerticalBorder} />
          <TouchableOpacity onPress={handleLogOut} style={styles.modalButton}>
            <Text style={[styles.buttonText, styles.logoutButton]}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  ));

  return (
    <>
      <View style={styles.container}>
        <ScrollView>
          <View style={styles.headerContainer}>
            <Image
              width={undefined}
              height={undefined}
              source={{ uri: `https://staging.honestdocs.co${avatar}` }}
              style={styles.avatar}
            />
            <Text
              style={styles.helloText}
            >{`Hello ${firstName} ${lastName}`}</Text>
          </View>
          <View>
            {sections.map((section, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => onPressHandler(section.path)}
                activeOpacity={0.7}
              >
                <View style={styles.sectionContainer}>
                  <AntDesignIcon
                    name="github"
                    size={30}
                    color={colors.darkBlue}
                  />
                  <Text style={styles.sectionTitle}>{section.title}</Text>
                  <AntDesignIcon
                    name="right"
                    size={15}
                    color={colors.navbarBorder}
                    style={styles.arrowIcon}
                  />
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </ScrollView>
      </View>

      <Modal animationType="fade" transparent={true} visible={modalVisible}>
        {renderModal()}
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    basicInfo: state.user.basicInfo,
  };
};

const mapDispatchToProps = {
  loadUserInfo: loadUserInfoFetch,
  logOut: logOutFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileMenu);

ProfileMenu.propTypes = {
  navigation: object,
  basicInfo: object,
  loadUserInfo: func,
  logOut: func,
};
