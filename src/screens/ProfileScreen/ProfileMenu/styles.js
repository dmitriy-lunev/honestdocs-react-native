import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  logoutModalContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
  },
  logoutModal: {
    width: 270,
    height: 125,
    borderRadius: 12,
    overflow: 'hidden',
    backgroundColor: colors.white,
  },
  buttonContainer: {
    width: '100%',
    height: 42,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    borderTopColor: colors.navbarBorder,
  },
  modalButton: {
    width: 270 / 2 - 1,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonsVerticalBorder: {
    flex: 1,
    width: 1,
    backgroundColor: colors.navbarBorder,
  },
  modalButtonsHorizontalBorder: {
    height: 1,
    width: '100%',
    backgroundColor: colors.navbarBorder,
  },
  modalTextContainer: {
    height: 81,
    paddingHorizontal: 35,
    justifyContent: 'center',
  },
  modalText: {
    textAlign: 'center',
    fontSize: 17,
  },
  buttonText: {
    fontSize: 17,
    color: '#007AFF',
  },
  logoutButton: {
    fontWeight: 'bold',
  },

  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingLeft: 15,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 47,
    marginBottom: 20,
    marginRight: 30,
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: colors.green,
  },
  helloText: {
    flex: 1,
    marginLeft: 10,
    fontSize: 24,
    lineHeight: 28,
    letterSpacing: -0.576,
    fontWeight: 'bold',
    color: colors.mainText,
  },
  sectionContainer: {
    paddingLeft: 5,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: colors.navbarBorder,
    borderBottomWidth: 1,
  },
  sectionTitle: {
    paddingVertical: 12,
    paddingLeft: 13,
    fontSize: 17,
    lineHeight: 20,
    color: colors.mainText,
  },
  arrowIcon: {
    position: 'absolute',
    right: 0,
    marginRight: 15,
  },
});

export default styles;
