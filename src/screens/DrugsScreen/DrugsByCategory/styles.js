import { StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  scrollContainer: {
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  title: {
    marginBottom: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
  itemText: {
    marginVertical: 8,
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.darkBlue,
  },
});

export default styles;
