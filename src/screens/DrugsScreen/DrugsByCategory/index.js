import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { array, func } from 'prop-types';

import { SearchButton } from '../../../components';
import {
  clearDrugs,
  loadDrugsByCategoriesFetch,
} from '../../../redux/modules/Drugs/actions';
import styles from './styles';

const DrugsByCategory = ({ drugs, loadDrugsByCategories, clearDrugs }) => {
  const { navigate } = useNavigation();
  const route = useRoute();
  const { categoryId, categoryName } = route?.params;

  useEffect(() => {
    loadDrugsByCategories(categoryId);
    return clearDrugs;
  }, []);

  const handlePress = (drugId) => {
    navigate('DrugsStackScreen', {
      screen: 'Drug',
      params: { drugId },
    });
  };

  return (
    <View style={styles.container}>
      <SearchButton />
      <ScrollView style={styles.scrollContainer}>
        <Text style={styles.title}>{categoryName}</Text>
        {drugs.map((item) => {
          return (
            <TouchableOpacity
              key={item.id.toString()}
              onPress={() => handlePress(item.id)}
            >
              <Text style={styles.itemText}>{item.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  drugs: state.drugs.drugs,
});

export default connect(mapStateToProps, {
  loadDrugsByCategories: loadDrugsByCategoriesFetch,
  clearDrugs,
})(DrugsByCategory);

DrugsByCategory.propTypes = {
  drugs: array,
  loadDrugsByCategories: func,
  clearDrugs: func,
};
