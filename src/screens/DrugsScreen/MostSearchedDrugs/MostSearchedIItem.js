import { array, func, string } from 'prop-types';
import React, { useState } from 'react';
import { Dimensions, Text, View } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import styles from './styles';

const { width } = Dimensions.get('window');

const MostSearchedItem = ({ name, data, renderCarouselItem }) => {
  const [activeSlide, setActiveSlide] = useState(0);

  return (
    <View style={styles.itemContainer}>
      <Text style={styles.categoryTitle}>{name}</Text>
      <Carousel
        data={data}
        renderItem={renderCarouselItem}
        sliderWidth={width}
        itemWidth={(width - 30) / 2}
        containerCustomStyle={{}}
        activeSlideAlignment="start"
        inactiveSlideOpacity={1}
        inactiveSlideScale={1}
        onSnapToItem={(index) => setActiveSlide(index)}
        loop={true}
      />
      <Pagination activeDotIndex={activeSlide} dotsLength={data.length} />
    </View>
  );
};

export default MostSearchedItem;

MostSearchedItem.propTypes = {
  name: string,
  data: array,
  renderCarouselItem: func,
};
