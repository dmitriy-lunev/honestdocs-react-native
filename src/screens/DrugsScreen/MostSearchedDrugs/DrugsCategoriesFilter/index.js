import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Text, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { array, bool, func } from 'prop-types';

import { loadDrugsCategoriesFetch } from '../../../../redux/modules/Drugs/actions';
import colors from '../../../../theme/colors';
import styles from './styles';
import CategoriesModal from '../CategoriesModal';

const DrugsCategoriesFilter = ({
  visible,
  setVisible,
  categories,
  loadDrugsCategories,
}) => {
  const { navigate } = useNavigation();

  useEffect(() => {
    loadDrugsCategories();
    return () => {};
  }, []);

  const onPressHandler = (categoryId, categoryName) => {
    navigate('DrugsStackScreen', {
      screen: 'DrugsByCategory',
      params: { categoryId, categoryName },
    });
  };

  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={true}
        bounces={false}
        showsHorizontalScrollIndicator={false}
      >
        <TouchableOpacity
          style={[styles.item, styles.currentItem]}
          onPress={() => true}
        >
          <Text style={styles.itemText}>Most searched</Text>
        </TouchableOpacity>
        {categories.map((item) => {
          return (
            <TouchableOpacity
              key={`filter_${item.id}`}
              style={[styles.item]}
              onPress={() => onPressHandler(item.id, item.name)}
            >
              <Text style={styles.itemText}>{item.name}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
      <TouchableOpacity
        style={styles.menuButton}
        onPress={() => setVisible(true)}
      >
        <FeatherIcon name="menu" size={26} color={colors.superGrey} />
      </TouchableOpacity>
      <CategoriesModal {...{ visible, setVisible }} />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    loaded: state.drugs.loaded,
    loading: state.drugs.loading,
    categories: state.drugs.categories,
  };
};

const mapDispatchToProps = {
  loadDrugsCategories: loadDrugsCategoriesFetch,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrugsCategoriesFilter);

DrugsCategoriesFilter.propTypes = {
  visible: bool,
  setVisible: func,
  categories: array,
  loadDrugsCategories: func,
};
