import React, { useEffect, useState } from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { array, bool, func } from 'prop-types';

import { SearchButton } from '../../../components';
import { loadMostSearchedDrugsFetch } from '../../../redux/modules/Drugs/actions';
import DrugsCategoriesFilter from './DrugsCategoriesFilter';
import CategoriesModal from './CategoriesModal';
import MostSearchedItem from './MostSearchedIItem';
import colors from '../../../theme/colors';
import styles from './styles';

const SEARCH_HEIGHT = 90;

const MostSearchedDrugs = ({ mostSearchedDrugs, loadMostSearchedDrugs }) => {
  const { navigate } = useNavigation();
  const [categoryModalVisible, setCategoryModalVisible] = useState(false);

  useEffect(() => {
    loadMostSearchedDrugs();
  }, []);

  const handlePress = (drugId) => {
    navigate('DrugsStackScreen', {
      screen: 'Drug',
      params: { drugId },
    });
  };

  const renderCarouselItem = ({ item }) => {
    return (
      <View style={styles.carouselItemContainer}>
        {item.map((drug) => (
          <TouchableOpacity
            key={drug.id}
            activeOpacity={0.7}
            onPress={() => handlePress(drug.id)}
          >
            <Text style={styles.drugText}>{drug.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const renderItem = (item) => {
    const { name, drugs } = item.item;

    if (drugs.length === 0) {
      return null;
    }

    let data = [];
    let dataItem = [];
    drugs.forEach((item, index) => {
      if ((index + 1) % 5 !== 0) {
        dataItem.push(item);
        if (index + 1 === drugs.length) {
          data.push(dataItem);
        }
      } else {
        dataItem.push(item);
        data.push(dataItem);
        dataItem = [];
      }
    });

    return <MostSearchedItem {...{ name, data, renderCarouselItem }} />;
  };

  return (
    <View style={styles.container}>
      <SearchButton
        active={true}
        title="Search for a drug name"
        params={{ searchCategory: 'drugs' }}
      />
      <DrugsCategoriesFilter
        visible={categoryModalVisible}
        setVisible={setCategoryModalVisible}
      />
      <FlatList
        data={mostSearchedDrugs}
        renderItem={renderItem}
        onEndReached={() => loadMostSearchedDrugs()}
        keyExtractor={(item) => `flatlist_${item.id}`}
        ListHeaderComponent={() => (
          <View style={styles.listHeaderComponentContainer}>
            <Text style={styles.drugsTitle}>Drugs by category</Text>
          </View>
        )}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  loading: state.drugs.loading,
  mostSearchedDrugs: state.drugs.mostSearchedDrugs,
});

const mapDispatchToProps = {
  loadMostSearchedDrugs: loadMostSearchedDrugsFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(MostSearchedDrugs);

MostSearchedDrugs.propTypes = {
  mostSearchedDrugs: array,
  loadMostSearchedDrugs: func,
};
