import { useNavigation } from '@react-navigation/native';
import { array, func, string } from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Pressable, Share, Text, View } from 'react-native';
import { connect } from 'react-redux';
import {
  loadDrugCommentsFetch,
  setBookmarkFetch,
} from '../../../redux/modules/Drugs/actions';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import styles from './styles';
import colors from '../../../theme/colors';
import { CreateComment } from '../../../components';

const DrugBottom = ({
  isSignedIn,
  bookmarked,
  mainTitle,
  drugId,
  comments,
  loadDrugComments,
  setBookmark,
}) => {
  const { navigate } = useNavigation();
  const [createCommentModalVisible, setCreateCommentModalVisible] = useState(
    false,
  );

  useEffect(() => {
    loadDrugComments(drugId);
    return () => {};
  }, []);

  const onShare = async (message) => {
    try {
      const result = await Share.share({
        message,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <View>
      <View style={[styles.flexRow, styles.bottomButtonsContainer]}>
        <View style={styles.flexRow}>
          <Text style={styles.commentsCountText}>{comments.length}</Text>
          <OcticonsIcon name="comment" size={20} color={colors.green} />
        </View>
        <View style={styles.flexRow}>
          <Pressable onPress={() => onShare(mainTitle)}>
            <EntypoIcon
              name="share-alternative"
              size={20}
              color={colors.green}
              style={styles.icon}
            />
          </Pressable>
          <Pressable
            onPress={() => {
              if (!isSignedIn) {
                navigate('SingInStack');
              } else {
                setBookmark(drugId);
              }
            }}
          >
            <FontAwesomeIcon
              name={bookmarked ? 'bookmark' : 'bookmark-o'}
              size={20}
              color={colors.green}
              style={styles.icon}
            />
          </Pressable>
        </View>
      </View>
      <CreateComment
        buttonText="Post a comment"
        commentableType="drug"
        commentableId={drugId}
        visible={createCommentModalVisible}
        setVisible={setCreateCommentModalVisible}
      />
    </View>
  );
};

const mapStateToProps = (state) => ({
  comments: state.drugs.drugComments,
  isSignedIn: state.authentication.isSignedIn,
  bookmarked: state.drugs.drug.bookmarked,
  mainTitle: state.drugs.drug.mainTitle,
});

const mapDispatchToProps = {
  loadDrugComments: loadDrugCommentsFetch,
  setBookmark: setBookmarkFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(DrugBottom);

DrugBottom.propTypes = {
  drugId: string,
  comments: array,
  loadDrugComments: func,
};
