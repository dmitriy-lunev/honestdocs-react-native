import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const { width } = Dimensions.get('window');
const ratio = 544 / 968;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  contentContainer: {
    paddingHorizontal: 15,
  },
  image: {
    width,
    height: width * ratio,
  },
  title: {
    marginTop: 30,
    color: colors.green,
  },
  mainTitle: {
    marginVertical: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
  promotedContainer: {
    marginTop: 30,
    marginBottom: 10,
    paddingVertical: 20,
    borderColor: colors.greyText,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  promotedHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  promotedHeaderText: {
    flex: 1,
    paddingRight: 15,
  },
  promotedText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'grey',
  },
  promotedTitle: {
    marginTop: 10,
    fontSize: 17,
  },
  promotedDescription: {
    marginTop: 15,
  },
  promotedImage: {
    width: 105,
    height: 105,
    borderRadius: 5,
  },
  promotedLinkContainer: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  promotedLinkText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.green,
    marginRight: 5,
  },
  staffContainer: {
    marginVertical: 15,
    flexDirection: 'row',
  },
  stuffImage: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  staffTextContainer: {
    flex: 1,
    justifyContent: 'space-around',
  },
  staffLabel: {
    fontSize: 14,
    color: colors.textGreyColor,
  },
  staffName: {
    fontSize: 17,
  },
  ratingContainer: {
    paddingVertical: 40,
    borderColor: colors.greyText,
    borderBottomWidth: 1,
  },
  postAnswerContainer: {
    width: '100%',
    justifyContent: 'center',
    marginBottom: 15,
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 3,
    backgroundColor: colors.concrete,
  },
  postAnswerText: {
    fontSize: 16,
    color: colors.basicText,
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomButtonsContainer: {
    marginTop: 10,
    marginBottom: 15,
    justifyContent: 'space-between',
  },
  commentsCountText: {
    marginRight: 5,
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.green,
  },
  icon: {
    marginHorizontal: 6,
  },
});

export default styles;
