import React, { useEffect } from 'react';
import {
  Image,
  ScrollView,
  Share,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { useNavigation, useRoute } from '@react-navigation/native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { loadDrugFetch } from '../../../redux/modules/Drugs/actions';
import { SearchButton } from '../../../components';
import styles from './styles';
import HTML from 'react-native-render-html';
import colors from '../../../theme/colors';
import StarRating from '../../../components/StarRating';
import DrugBottom from './DrugBottom';
import { bool, func, object } from 'prop-types';

const tagStyles = {
  p: { marginBottom: 15, fontSize: 16 },
  a: { fontSize: 16, textDecorationLine: 'none' },
  li: { fontSize: 16 },
  h1: { fontSize: 18, marginBottom: 15 },
  h2: { fontSize: 18, marginBottom: 15 },
  h3: { fontSize: 18, marginBottom: 15 },
};

const Drug = ({ loading, drug, loadDrug }) => {
  const { navigate } = useNavigation();
  const route = useRoute();
  const { drugId } = route?.params;

  useEffect(() => {
    loadDrug(drugId);
  }, []);

  if (loading) {
    return null;
  }

  const {
    id,
    mainTitle,
    body,
    writtenBy,
    manImage,
    currentUserRating,
    promoted,
  } = drug;

  return (
    <View style={styles.container}>
      <SearchButton />
      <ScrollView>
        <Image
          source={{
            uri:
              'https://im.kommersant.ru/Issues.photo/OGONIOK/2019/019/KMO_148127_01452_1_t218_221732.jpg',
          }}
          width={undefined}
          height={undefined}
          style={styles.image}
        />
        <View style={styles.contentContainer}>
          <Text style={styles.title}>Drugs</Text>
          <Text style={styles.mainTitle}>{mainTitle}</Text>
          <View style={styles.bodyContainer}>
            <HTML source={{ html: body || '' }} tagsStyles={tagStyles} />
          </View>
          <View>
            <TouchableWithoutFeedback onPress={() => navigate('HDmall', {})}>
              <View style={styles.promotedContainer}>
                <View style={styles.promotedHeaderContainer}>
                  <View style={styles.promotedHeaderText}>
                    <Text style={styles.promotedText}>
                      Promoted by HonestDocs
                    </Text>
                    <Text style={styles.promotedTitle}>{promoted?.title}</Text>
                    <Text style={styles.promotedDescription}>
                      {promoted?.description}
                    </Text>
                  </View>
                  <Image
                    width={undefined}
                    height={undefined}
                    source={{
                      uri:
                        'https://www.gettyimages.com/gi-resources/images/frontdoor/editorial/Velo/GettyImages-Velo-1088643550.jpg',
                    }}
                    style={styles.promotedImage}
                  />
                </View>
                <View style={styles.promotedLinkContainer}>
                  <Text style={styles.promotedLinkText}>See details</Text>
                  <SimpleLineIcons
                    name="share-alt"
                    size={12}
                    color={colors.green}
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.staffContainer}>
            <Image
              source={{
                uri: 'https://staging.honestdocs.co' + manImage,
              }}
              width={undefined}
              height={undefined}
              style={styles.stuffImage}
            />
            <View style={styles.staffTextContainer}>
              <Text style={styles.staffLabel}>Written by</Text>
              <Text style={styles.staffName}>{writtenBy}</Text>
            </View>
          </View>
          <View style={styles.ratingContainer}>
            <StarRating
              title={
                !currentUserRating
                  ? 'How helpful was this article?'
                  : 'Thanks for rating'
              }
              rating={currentUserRating}
              ratingableType="drug"
              ratingableId={id}
              starSize={40}
            />
          </View>
          <DrugBottom drugId={drugId} />
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state) => ({
  drug: state.drugs.drug,
  loading: state.drugs.loading,
});

export default connect(mapStateToProps, { loadDrug: loadDrugFetch })(Drug);

Drug.propTypes = {
  loading: bool,
  drug: object,
  loadDrug: func,
};
