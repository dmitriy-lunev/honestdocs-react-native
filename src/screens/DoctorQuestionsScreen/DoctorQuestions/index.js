import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Text, TouchableOpacity, View, FlatList } from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/FontAwesome';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import { array, bool, func, object } from 'prop-types';

import {
  loadQuestionsFetch,
  setOrUnsetDislikeFetch,
  setOrUnsetLikeFetch,
} from '../../../redux/modules/DoctorQuestions/actions';
import { SearchButton } from '../../../components/index';
import AddQuestionModal from './AddQuestionModal';
import { calculateTimeAgo } from '../../../helpers/calculateTimeAgo';
import colors from '../../../theme/colors';
import styles from './styles';

const HEADER_HEIGHT = 160;
const SEARCH_HEIGHT = 90;

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const DoctorQuestions = ({
  navigation,
  isSignedIn,
  questions,
  loadQuestions,
  setOrUnsetLike,
  setOrUnsetDislike,
}) => {
  useEffect(() => {
    if (questions.length === 0) {
      loadQuestions();
    }
    return () => {};
  }, []);

  const [createQuestionVisible, setCreateQuestionVisible] = useState(false);

  const translationY = useSharedValue(0);

  const onScroll = useAnimatedScrollHandler((event) => {
    if (event.contentOffset.y <= 0) {
      translationY.value = 0;
    } else if (event.contentOffset.y < HEADER_HEIGHT - SEARCH_HEIGHT) {
      translationY.value = event.contentOffset.y;
    } else {
      translationY.value = HEADER_HEIGHT - SEARCH_HEIGHT;
    }
  });

  const styleHeader = useAnimatedStyle(() => {
    return {
      position: 'absolute',
      transform: [
        {
          translateY: -translationY.value,
        },
      ],
    };
  });

  const styleContent = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: HEADER_HEIGHT - translationY.value,
        },
      ],
    };
  });

  const styleAddButton = useAnimatedStyle(() => {
    return {
      right: interpolate(
        translationY.value,
        [0, SEARCH_HEIGHT],
        [-60, 35],
        Extrapolate.CLAMP,
      ),
    };
  });

  const renderItem = ({ item, index }) => {
    const {
      id,
      title,
      promo,
      answeredByDoctor,
      answersCount,
      createdAt,
      updatedAt,
      liked,
    } = item;

    return (
      <View style={styles.questionItemContainer}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={() =>
            navigation.navigate('DoctorQAStackScreen', {
              screen: 'DoctorQuestion',
              params: { id },
            })
          }
        >
          <View style={styles.questionItemPadding}>
            {promo && (
              <View style={styles.promotedContainer}>
                <OcticonsIcon name="arrow-up" size={20} color={colors.blue} />
                <Text style={styles.promotedText}>Promoted Question</Text>
              </View>
            )}
            <Text numberOfLines={4} style={styles.titleText}>
              {title}
            </Text>
            <Text style={styles.timeAgoText}>
              {calculateTimeAgo(createdAt)}
            </Text>
            <View style={styles.verticalLine} />
          </View>
          <View style={[styles.bottomContainer, styles.questionItemPadding]}>
            <View style={styles.doctorAnswerContainer}>
              <Text style={styles.answersCountText}>{answersCount}</Text>
              <OcticonsIcon name="comment" size={20} color={colors.green} />
              <Text style={styles.answeredByDoctorText}>
                {answeredByDoctor && 'Answered by a verified doctor'}
              </Text>
            </View>
            <View style={styles.likeDislikeContainer}>
              <TouchableOpacity
                onPress={() => {
                  if (isSignedIn) {
                    setOrUnsetLike(index);
                  } else {
                    navigation.navigate('SingInStack');
                  }
                }}
              >
                <AntDesignIcon
                  name="like2"
                  size={22}
                  color={liked === true ? colors.blue : colors.basicText}
                  style={styles.likeIcon}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  if (isSignedIn) {
                    setOrUnsetDislike(index);
                  } else {
                    navigation.navigate('SingInStack');
                  }
                }}
              >
                <AntDesignIcon
                  name="dislike2"
                  size={22}
                  color={
                    liked !== null && !liked ? colors.red : colors.basicText
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const handleOpenModal = () => {
    if (!isSignedIn) {
      navigation.navigate('SingInStack');
    } else {
      setCreateQuestionVisible(!createQuestionVisible);
    }
  };

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.box, styleHeader]}>
        <View
          style={[
            styles.hideableHeaderContainer,
            { height: HEADER_HEIGHT - SEARCH_HEIGHT },
          ]}
        >
          <View style={styles.hideableHeader}>
            <Text style={styles.hideableHeaderTitle}>Doctor Q&A</Text>
            <TouchableOpacity activeOpacity={0.7} onPress={handleOpenModal}>
              <Text style={styles.hideableHeaderText}>+ Ask a question</Text>
            </TouchableOpacity>
          </View>
        </View>
        <SearchButton params={{ searchCategory: 'hospitals' }}>
          <View
            style={[styles.searchContainer, { height: SEARCH_HEIGHT - 40 }]}
          >
            <Icon name="search" size={20} color={colors.blue} />
            <Text style={styles.searchText}>Search questions</Text>
          </View>
        </SearchButton>
      </Animated.View>

      <Animated.View style={[styles.scroll, styleContent]}>
        <AnimatedFlatList
          scrollEventThrottle={16}
          data={questions}
          renderItem={renderItem}
          onEndReached={() => loadQuestions()}
          {...{ onScroll }}
        />
      </Animated.View>

      <Animated.View
        style={[styles.addButton, styleAddButton, { top: SEARCH_HEIGHT + 40 }]}
      >
        <TouchableOpacity activeOpacity={0.7} onPress={handleOpenModal}>
          <AntDesignIcon name="pluscircle" size={60} color={colors.blue} />
        </TouchableOpacity>
      </Animated.View>

      <AddQuestionModal
        visible={createQuestionVisible}
        setVisible={setCreateQuestionVisible}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    isSignedIn: state.authentication.isSignedIn,
    questions: state.doctorQuestions.questions,
  };
};

const mapDispatchToProps = {
  loadQuestions: loadQuestionsFetch,
  setOrUnsetLike: setOrUnsetLikeFetch,
  setOrUnsetDislike: setOrUnsetDislikeFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(DoctorQuestions);

DoctorQuestions.propTypes = {
  navigation: object,
  isSignedIn: bool,
  questions: array,
  loadQuestions: func,
  setOrUnsetLike: func,
  setOrUnsetDislike: func,
};
