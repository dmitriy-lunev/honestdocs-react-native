import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Text,
  Image,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { bool, func } from 'prop-types';

import { SwipeableModal } from '../../../../components/index';
import AttachImageModal from './AttachImageModal';
import { postQuestionFetch } from '../../../../redux/modules/DoctorQuestions/actions';
import RNButton from '../../../../components/RNButton';
import colors from '../../../../theme/colors';
import styles from './styles';
import { informationModalRef } from '../../../../components/InformationMessage';

const validate = (values) => {
  const errors = {};
  if (!values.title) errors.title = 'Required';
  return errors;
};

const AddQuestionModal = ({ visible, setVisible, postQuestion }) => {
  const [attachImageVisible, setAttachImageVisible] = useState(false);
  const [images, setImages] = useState([]);

  const onSubmit = ({ title }, actions) => {
    const formData = new FormData();
    images.map((item) => formData.append(item));
    postQuestion(title, formData)
      .then(() => {
        actions.resetForm();
      })
      .catch((error) => {
        console.log(error.response);
        if (
          error.response.data.description ===
          'Not enough coins Not enough coins.'
        ) {
          setVisible(!visible);
          informationModalRef.current.openInfoMessage(
            error.response.data.description,
          );
        }
      });
  };

  return (
    <SwipeableModal visible={visible} setVisible={setVisible}>
      <>
        <View style={styles.container}>
          <View style={styles.dropDownIcon} />
          <View style={styles.modalContainer}>
            <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
              <Formik
                initialValues={{ title: '' }}
                onSubmit={onSubmit}
                validate={validate}
              >
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                }) => {
                  return (
                    <View>
                      <TextInput
                        multiline={true}
                        onChangeText={handleChange('title')}
                        onBlur={handleBlur('title')}
                        value={values.title}
                        placeholder="Type your question"
                        placeholderTextColor={colors.greyText}
                        style={styles.input}
                      />
                      <TouchableOpacity
                        onPress={() =>
                          setAttachImageVisible(!attachImageVisible)
                        }
                      >
                        <Text style={styles.attachText}>Attach a file</Text>
                      </TouchableOpacity>
                      <View style={styles.imagesContainer}>
                        {images.length !== 0 &&
                          images.map((item, index) => {
                            return (
                              <View
                                key={item.fileName}
                                style={styles.attachedImageContainer}
                              >
                                <Image
                                  source={{ uri: item.uri }}
                                  style={styles.attachedImage}
                                />
                                <TouchableOpacity
                                  onPress={() => {
                                    setImages(
                                      images.filter(
                                        (_, imageIndex) => index !== imageIndex,
                                      ),
                                    );
                                  }}
                                >
                                  <View style={styles.deleteImageButton}>
                                    <AntDesignIcon
                                      name="closecircleo"
                                      size={30}
                                      color={colors.green}
                                    />
                                  </View>
                                </TouchableOpacity>
                              </View>
                            );
                          })}
                      </View>
                      <RNButton
                        label="Ask a new question"
                        disabled={
                          Object.keys(errors).length !== 0 ||
                          values.title === ''
                        }
                        backgroundColor={colors.green}
                        onPress={handleSubmit}
                      />
                    </View>
                  );
                }}
              </Formik>
              <TouchableOpacity
                style={styles.closeIconButton}
                onPress={() => setVisible(!visible)}
              >
                <FontAwesomeIcon
                  name="angle-down"
                  size={30}
                  color={colors.green}
                />
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
        <AttachImageModal
          visible={attachImageVisible}
          setVisible={setAttachImageVisible}
          images={images}
          setImages={setImages}
        />
      </>
    </SwipeableModal>
  );
};

const mapDispatchToProps = {
  postQuestion: postQuestionFetch,
};

export default connect(null, mapDispatchToProps)(AddQuestionModal);

AddQuestionModal.propTypes = {
  visible: bool,
  setVisible: func,
  postQuestion: func,
};
