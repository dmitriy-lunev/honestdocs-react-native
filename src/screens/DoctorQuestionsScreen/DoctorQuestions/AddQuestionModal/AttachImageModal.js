import React from 'react';
import { Modal, Text, TouchableHighlight, View } from 'react-native';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { array, bool, func } from 'prop-types';

import styles from './styles';

const AttachImageModal = ({ visible, setVisible, images, setImages }) => {
  const handleChoosePhoto = () => {
    const options = {
      mediaType: 'photo',
    };
    launchImageLibrary(options, (response) => {
      if (response.uri) {
        setVisible(false);
        setImages([...images, response]);
      }
    });
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={() => {
        setVisible(!visible);
      }}
    >
      <View style={styles.centeredModalView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Add image</Text>
          <TouchableHighlight onPress={handleChoosePhoto}>
            <Text style={styles.addImageVariantButton}>
              Choose from library
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => {
              launchCamera({}, (response) => {
                if (response.uri) {
                  setImages([...images, response]);
                  setVisible(!visible);
                }
              });
            }}
          >
            <Text style={styles.addImageVariantButton}>Take photo</Text>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => setVisible(!visible)}>
            <Text style={styles.addImageVariantButton}>Cancel</Text>
          </TouchableHighlight>
        </View>
      </View>
    </Modal>
  );
};

export default AttachImageModal;

AttachImageModal.propTypes = {
  visible: bool,
  setVisible: func,
  images: array,
  setImages: func,
};
