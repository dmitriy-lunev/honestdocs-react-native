import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  addButton: {
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 30,
  },
  box: {
    width: '100%',
  },
  scroll: {
    flex: 1,
  },
  text: {
    fontSize: 30,
  },
  searchContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    width: '90%',
  },
  searchText: {
    paddingLeft: 15,
    color: 'grey',
    fontSize: 20,
    fontWeight: 'bold',
  },
  hideableHeaderContainer: {
    backgroundColor: colors.green,
    paddingHorizontal: 15,
    justifyContent: 'flex-end',
  },
  hideableHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  hideableHeaderTitle: {
    color: colors.white,
    fontSize: 26,
    fontWeight: 'bold',
  },
  hideableHeaderText: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  separator: {
    width: '100%',
    height: 10,
  },
  questionItemContainer: {
    marginBottom: 10,
    backgroundColor: colors.white,
  },
  questionItemPadding: {
    paddingLeft: 16,
    paddingRight: 14,
  },
  promotedContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: 20,
  },
  promotedText: {
    marginLeft: 5,
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.blue,
  },
  titleText: {
    marginTop: 20,
    fontSize: 17,
  },
  timeAgoText: {
    marginTop: 7,
    marginBottom: 17,
    fontSize: 13,
    color: colors.basicText,
  },
  verticalLine: {
    width: '100%',
    height: 1,
    backgroundColor: colors.emptyColor,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  doctorAnswerContainer: {
    paddingTop: 13,
    paddingBottom: 13,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  answersCountText: {
    marginRight: 5,
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.green,
  },
  answeredByDoctorText: {
    marginLeft: 11,
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.basicText,
  },
  likeDislikeContainer: {
    flexDirection: 'row',
  },
  likeIcon: {
    marginRight: 5,
  },
});

export default styles;
