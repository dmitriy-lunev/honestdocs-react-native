import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  questionContainer: {
    paddingHorizontal: 15,
    backgroundColor: colors.white,
  },
  promotedContainer: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: 20,
  },
  promotedText: {
    marginLeft: 5,
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.blue,
  },
  title: {
    marginTop: 15,
    marginRight: 60,
    fontSize: 20,
    fontWeight: 'bold',
  },
  addButton: {
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 30,
  },
  askedByAndTimeAgoText: {
    marginTop: 15,
    color: colors.basicText,
  },
  followContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.concrete,
  },
  followText: {
    marginLeft: 5,
    fontSize: 15,
    fontWeight: 'bold',
    color: colors.basicText,
  },
  bottomContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 5,
  },
  doctorAnswerContainer: {
    paddingVertical: 13,
    width: width - 45 - 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  answersCountText: {
    marginRight: 5,
    color: colors.green,
    fontSize: 13,
    fontWeight: 'bold',
  },
  answeredByDoctorText: {
    marginLeft: 11,
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.basicText,
  },
  likeDislikeContainer: {
    width: 45,
    flexDirection: 'row',
  },
  likeIcon: {
    marginRight: 5,
  },
  postAnswerContainer: {
    width: '100%',
    justifyContent: 'center',
    marginBottom: 15,
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 3,
    backgroundColor: colors.concrete,
  },
  postAnswerText: {
    fontSize: 16,
    color: colors.basicText,
  },
  answerModalButton: {
    fontSize: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: colors.white,
  },
});

export default styles;
