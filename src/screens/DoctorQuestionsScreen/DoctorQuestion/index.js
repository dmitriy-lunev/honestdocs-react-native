import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Pressable, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

import Answers from './Answers';
import CreateAnswerModal from './CreateAnswerModal';
import { SearchButton } from '../../../components';
import { calculateTimeAgo } from '../../../helpers/calculateTimeAgo';
import questionsAPI from '../../../api/questions-api';
import colors from '../../../theme/colors';
import styles from './styles';

const getQuestionById = async (questionId, userId, token) => {
  const {
    data: {
      data: {
        id,
        attributes: {
          title,
          answered_by_doctor: answeredByDoctor,
          answered_by_nurse: answeredByNurse,
          answers_count: answersCount,
          created_at: createdAt,
          edited,
          promo,
          user_id: questionUserId,
          user_name: userName,
          user_age: userAge,
          user_gender: userGender,
          liked,
          followed,
          followings_count: followingsCount,
          images,
        },
      },
    },
  } = await questionsAPI.getQuestion(questionId, userId, token);
  return {
    id,
    title,
    answeredByDoctor,
    answeredByNurse,
    answersCount,
    createdAt,
    edited,
    promo,
    questionUserId,
    userName,
    userAge,
    userGender,
    liked,
    followed,
    followingsCount,
    images,
  };
};

function useQuestion(questionId, userId, token) {
  return useQuery(
    ['question', questionId],
    () => getQuestionById(questionId, userId, token),
    {
      enabled: !!questionId,
    },
  );
}

const DoctorQuestion = ({ userId, userToken, isSignedIn }) => {
  const navigation = useNavigation();
  const route = useRoute();
  const { id: questionId } = route.params;

  const [createAnswerModalVisible, setCreateAnswerModalVisible] = useState(
    false,
  );
  const [createCommentModalVisible, setCreateCommentModalVisible] = useState(
    false,
  );

  const showCreateCommentModal = () => {
    setCreateCommentModalVisible(!createCommentModalVisible);
  };

  const { status, data, error, isFetching } = useQuestion(
    questionId,
    userId,
    userToken,
  );

  const queryClient = useQueryClient();

  const followOrUnfollowQuestion = () => {
    return questionsAPI.followOrUnfollowQuestion(
      questionId,
      userId,
      userToken,
      !data.followed,
    );
  };

  const { mutate: mutateFollowed } = useMutation(followOrUnfollowQuestion);

  const toggleFollowed = () => {
    mutateFollowed(null, {
      onSuccess: () => {
        queryClient.setQueryData(['question', questionId], () => {
          return {
            ...data,
            followed: !data.followed,
            followingsCount: !data.followed
              ? data.followingsCount + 1
              : data.followingsCount - 1,
          };
        });
      },
      onError: (error) => {
        console.log(error);
      },
    });
  };

  const likeQuestion = () => {
    const unset = data.liked ? true : null;
    return questionsAPI.setOrUnsetQuestionLike(
      questionId,
      userId,
      userToken,
      unset,
    );
  };

  const { mutate: mutateLiked } = useMutation(likeQuestion);

  const toggleLiked = () => {
    mutateLiked(null, {
      onSuccess: () => {
        queryClient.setQueryData(['question', questionId], () => {
          return {
            ...data,
            liked: data.liked ? null : true,
          };
        });
      },
      onError: (error) => {
        console.log(error.response);
      },
    });
  };

  const dislikeQuestion = () => {
    const unset = !data.liked && data.liked !== null ? true : null;
    return questionsAPI.setOrUnsetQuestionDislike(
      questionId,
      userId,
      userToken,
      unset,
    );
  };

  const { mutate: mutateDisliked } = useMutation(dislikeQuestion);

  const toggleDisliked = () => {
    mutateDisliked(null, {
      onSuccess: () => {
        queryClient.setQueryData(['question', questionId], () => {
          return {
            ...data,
            liked: !data.liked && data.liked !== null ? null : false,
          };
        });
      },
      onError: (error) => {
        console.log(error.response);
      },
    });
  };

  if (status === 'loading') {
    return (
      // <View style={styles.loaderContainer}>
      <View>
        <Text>loading...</Text>
      </View>
    );
  }

  const renderHeader = () => {
    return (
      <View style={styles.questionContainer}>
        {data.promo && (
          <View style={styles.promotedContainer}>
            <OcticonsIcon name="arrow-up" size={20} color={colors.blue} />
            <Text style={styles.promotedText}>Promoted Question</Text>
          </View>
        )}
        <Text style={styles.title}>{data.title}</Text>
        <Text style={styles.askedByAndTimeAgoText}>
          {`Asked by ${data.userName || 'Anonymous'} · ${calculateTimeAgo(
            data.createdAt,
          )}`}
        </Text>
        <TouchableOpacity
          onPress={() => {
            if (!isSignedIn) {
              navigation.navigate('SingInStack');
            } else {
              toggleFollowed();
            }
          }}
        >
          <View style={styles.followContainer}>
            <OcticonsIcon
              name={data.followed ? 'check' : 'plus'}
              size={15}
              color={data.followed ? colors.green : colors.basicText}
            />
            <Text
              style={[
                styles.followText,
                data.followed && { color: colors.green },
              ]}
            >{`Follow this question · ${data.followingsCount}`}</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.bottomContainer}>
          <View style={styles.doctorAnswerContainer}>
            <Text style={styles.answersCountText}>{data.answersCount}</Text>
            <OcticonsIcon name="comment" size={20} color={colors.green} />
            <Text style={styles.answeredByDoctorText}>
              {data.answeredByDoctor && 'Answered by a verified doctor'}
            </Text>
          </View>

          <View style={styles.likeDislikeContainer}>
            <TouchableOpacity
              onPress={() => {
                if (!isSignedIn) {
                  navigation.navigate('SingInStack');
                } else {
                  toggleLiked();
                }
              }}
            >
              <AntDesignIcon
                name="like2"
                size={20}
                color={data.liked ? colors.blue : colors.basicText}
                style={styles.likeIcon}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                if (!isSignedIn) {
                  navigation.navigate('SingInStack');
                } else {
                  toggleDisliked();
                }
              }}
            >
              <AntDesignIcon
                name="dislike2"
                size={20}
                color={
                  !data.liked && data.liked !== null
                    ? colors.red
                    : colors.basicText
                }
              />
            </TouchableOpacity>
          </View>
        </View>
        <Pressable
          onPress={() => {
            {
              if (!isSignedIn) {
                navigation.navigate('SingInStack');
              } else {
                setCreateAnswerModalVisible(!createAnswerModalVisible);
              }
            }
          }}
        >
          <View style={styles.postAnswerContainer}>
            <Text style={styles.postAnswerText}>Post an answer</Text>
          </View>
        </Pressable>
      </View>
    );
  };

  return (
    <View>
      <SearchButton title="Search questions" />
      <Answers
        commentableId={questionId}
        renderHeader={renderHeader}
        showModal={showCreateCommentModal}
      />
      <CreateAnswerModal
        visible={createAnswerModalVisible}
        setVisible={setCreateAnswerModalVisible}
        commentableId={questionId}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    userToken: state.authentication.userToken,
    userId: state.authentication.userInfo.id,
    isSignedIn: state.authentication.isSignedIn,
  };
};

export default connect(mapStateToProps)(DoctorQuestion);
