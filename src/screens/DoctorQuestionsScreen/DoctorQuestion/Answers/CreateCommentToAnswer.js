import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import AnswerModal from './AnswerModal';
import colors from '../../../../theme/colors';
import { bool, number, string } from 'prop-types';
import { CreateComment } from '../../../../components';

const CreateCommentToAnswer = ({
  commentsCount,
  isSignedIn,
  answerId,
  modalTitle,
}) => {
  const [createCommentModalVisible, setCreateCommentModalVisible] = useState(
    false,
  );

  const showCreateCommentModal = () => {
    setCreateCommentModalVisible(!createCommentModalVisible);
  };

  return (
    <>
      <View>
        <View style={styles.bottomContainer}>
          <Text style={styles.commentsCount}>{commentsCount}</Text>
          <OcticonsIcon name="comment" size={20} color={colors.green} />
          <View style={styles.createCommentContainer}>
            <CreateComment
              buttonText="Post a comment"
              commentableType="answer"
              commentableId={answerId}
              onSubmit={(data) => true}
              visible={createCommentModalVisible}
              setVisible={setCreateCommentModalVisible}
            />
          </View>
        </View>
      </View>
      <AnswerModal
        title={modalTitle}
        {...{ isSignedIn }}
        openCommentModal={showCreateCommentModal}
      />
    </>
  );
};

export default CreateCommentToAnswer;

const styles = StyleSheet.create({
  bottomContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
    marginBottom: 10,
  },
  commentsCount: {
    marginRight: 5,
    color: colors.green,
    fontSize: 13,
    fontWeight: 'bold',
  },
  createCommentContainer: {
    flex: 1,
    marginLeft: 15,
  },
});

CreateCommentToAnswer.propTypes = {
  commentsCount: number,
  isSignedIn: bool,
  answerId: string,
  modalTitle: string,
};
