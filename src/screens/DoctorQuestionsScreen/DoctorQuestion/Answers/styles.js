import { StyleSheet } from 'react-native';
import { color } from 'react-native-reanimated';
import colors from '../../../../theme/colors';

const styles = StyleSheet.create({
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    marginTop: 10,
    paddingHorizontal: 15,
    paddingVertical: 20,
    backgroundColor: colors.white,
  },
  verifiedDoctorText: {
    paddingLeft: 5,
    paddingRight: 10,
    fontWeight: 'bold',
    color: colors.blue,
  },
  timeAgoText: {
    color: colors.basicText,
  },
  answeredUserLabel: {
    marginBottom: 5,
    color: colors.basicText,
  },
  answeredUserText: {
    fontWeight: 'bold',
  },
  avatar: {
    marginRight: 10,
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  answeredUserContainer: {
    paddingVertical: 10,
  },
  answerText: {
    marginTop: 10,
    fontSize: 16,
  },
  answerDotsModal: {
    position: 'absolute',
    right: 0,
    top: 8,
  },
  commentsCount: {
    marginRight: 5,
    color: colors.green,
    fontSize: 13,
    fontWeight: 'bold',
  },
  bottomContainer: {
    alignItems: 'center',
    marginTop: 12,
    marginBottom: 10,
  },
  postCommentButton: {
    flex: 1,
    marginLeft: 10,
    paddingHorizontal: 10,
    paddingVertical: 3,
    fontSize: 16,
    backgroundColor: colors.concrete,
    color: colors.basicText,
  },
  answerModalContainer: {
    width: '100%',
    justifyContent: 'center',
    marginBottom: 15,
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 3,
    backgroundColor: colors.concrete,
  },
  answerModalText: {
    fontSize: 16,
    color: colors.basicText,
  },
  answerModalButton: {
    fontSize: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: colors.white,
  },
  answerModalTitle: {
    marginHorizontal: 40,
    marginVertical: 15,
    fontSize: 16,
    fontWeight: 'bold',
  },
  addButton: {
    position: 'absolute',
    backgroundColor: 'white',
    borderRadius: 30,
  },
});

export default styles;
