import React, { useState } from 'react';
import { Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import { bool, string } from 'prop-types';
import { useQuery } from 'react-query';
import commentsAPI from '../../../../../api/comments-api';
import styles from './styles';
import HideableText from '../../../../../components/HideableText';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import colors from '../../../../../theme/colors';
import { calculateTimeAgo } from '../../../../../helpers/calculateTimeAgo';
import { FadeCenteredModal } from '../../../../../components';

const fetchComments = async (commentableId) => {
  const data = await commentsAPI.getComments('answer', commentableId);
  return data.data.data.map((item) => {
    const {
      id,
      attributes: {
        text,
        created_at: createdAt,
        user_name: userName,
        user_age: userAge,
        user_gender: userGender,
      },
    } = item;
    return {
      id,
      text,
      createdAt,
      userName,
      userAge,
      userGender,
    };
  });
};

const AnswerComments = ({ commentableId, commentsCount, isSignedIn }) => {
  const navigation = useNavigation();
  const [showComments, setShowComments] = useState(false);
  const [visibleModal, setVisibleModal] = useState(false);

  const { data, status, error } = useQuery(
    ['answer_comment', commentableId],
    () => fetchComments(commentableId),
  );

  if (status === 'loading') {
    return null;
  }

  if (!showComments) {
    return (
      <TouchableOpacity onPress={() => setShowComments(!showComments)}>
        <Text style={styles.showCommentText}>
          Show comments {commentsCount}
        </Text>
      </TouchableOpacity>
    );
  }

  return (
    <View>
      {data.map((comment) => {
        const { id, text, createdAt, userName, userAge, userGender } = comment;
        return (
          <View key={id} style={styles.commentContainer}>
            <Text style={styles.basicText}>{`Commented by ${
              userName || 'Anonymous'
            } ${!!userAge ? userAge + 'y/o' : ''} ${
              !!userGender ? userGender : ''
            }`}</Text>
            <Text style={styles.basicText}>{calculateTimeAgo(createdAt)}</Text>
            <HideableText
              numberOfLines={3}
              text={text}
              containerStyle={styles.textContainer}
              textStyle={styles.textStyle}
              buttonStyle={styles.buttonStyle}
            />
            <View style={styles.commentDotsModal}>
              <TouchableOpacity
                onPress={() => {
                  if (!isSignedIn) {
                    navigation.navigate('SingInStack');
                  } else {
                    setVisibleModal(!visibleModal);
                  }
                }}
              >
                <EntypoIcon
                  name="dots-three-horizontal"
                  size={30}
                  color={colors.basicText}
                />
              </TouchableOpacity>
            </View>
            <FadeCenteredModal
              visible={visibleModal}
              setVisible={setVisibleModal}
            >
              <Text style={styles.commentModalTitle}>{id}</Text>
              <TouchableHighlight onPress={() => {}}>
                <Text style={styles.commentModalButton}>Report</Text>
              </TouchableHighlight>
              <TouchableHighlight
                onPress={() => setVisibleModal(!visibleModal)}
              >
                <Text style={styles.commentModalButton}>Cancel</Text>
              </TouchableHighlight>
            </FadeCenteredModal>
          </View>
        );
      })}
    </View>
  );
};

const mapStateToProps = (state) => ({
  isSignedIn: state.authentication.isSignedIn,
});

export default connect(mapStateToProps)(AnswerComments);

AnswerComments.propTypes = {
  commentableId: string,
  isSignedIn: bool,
};
