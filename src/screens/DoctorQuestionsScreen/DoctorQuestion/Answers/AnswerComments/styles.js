import { StyleSheet } from 'react-native';
import colors from '../../../../../theme/colors';

const styles = StyleSheet.create({
  showCommentText: {
    paddingTop: 10,
    textAlign: 'center',
    color: colors.basicText,
  },
  commentContainer: {
    marginTop: 10,
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: colors.concrete,
  },
  basicText: {
    color: colors.basicText,
    fontSize: 13,
  },
  textContainer: {
    marginTop: 10,
  },
  textStyle: {
    fontSize: 15,
    color: colors.basicText,
  },
  buttonStyle: {
    color: colors.green,
  },
  commentDotsModal: {
    position: 'absolute',
    right: 20,
    top: 15,
  },
  commentModalContainer: {
    width: '100%',
    justifyContent: 'center',
    marginBottom: 15,
    paddingHorizontal: 10,
    paddingVertical: 6,
    borderRadius: 3,
    backgroundColor: colors.concrete,
  },
  commentModalText: {
    fontSize: 16,
    color: colors.basicText,
  },
  commentModalButton: {
    fontSize: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: colors.white,
  },
  commentModalTitle: {
    marginHorizontal: 40,
    marginVertical: 15,
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default styles;
