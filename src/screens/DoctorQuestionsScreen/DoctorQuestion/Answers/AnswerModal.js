import { useNavigation } from '@react-navigation/native';
import { bool, func, string } from 'prop-types';
import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { FadeCenteredModal } from '../../../../components';
import colors from '../../../../theme/colors';

const AnswerModal = ({ title, isSignedIn, openCommentModal }) => {
  const [visible, setVisible] = useState(false);
  const { navigate } = useNavigation();

  return (
    <View style={styles.answerDotsModal}>
      <TouchableOpacity
        onPress={() => {
          if (!isSignedIn) {
            navigate('SingInStack');
          } else {
            setVisible(!visible);
          }
        }}
      >
        <EntypoIcon
          name="dots-three-horizontal"
          size={30}
          color={colors.basicText}
        />
      </TouchableOpacity>
      <FadeCenteredModal
        visible={visible}
        setVisible={setVisible}
        title={title}
      >
        <Text style={styles.answerModalTitle}>{title}</Text>
        <TouchableHighlight
          onPress={() => {
            setVisible(!visible);
            openCommentModal();
          }}
        >
          <Text style={styles.answerModalButton}>Post a reply</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => {}}>
          <Text style={styles.answerModalButton}>Report</Text>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => setVisible(!visible)}>
          <Text style={styles.answerModalButton}>Cancel</Text>
        </TouchableHighlight>
      </FadeCenteredModal>
    </View>
  );
};

export default AnswerModal;

const styles = StyleSheet.create({
  answerModalButton: {
    fontSize: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: colors.white,
  },
  answerModalTitle: {
    marginHorizontal: 40,
    marginVertical: 15,
    fontSize: 16,
    fontWeight: 'bold',
  },
  answerDotsModal: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
});

AnswerModal.propTypes = {
  title: string,
  isSignedIn: bool,
  openCommentModal: func,
};
