import React, { useState } from 'react';
import { connect } from 'react-redux';
import { FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import { useInfiniteQuery } from 'react-query';
import OcticonsIcon from 'react-native-vector-icons/Octicons';
import { bool, func, number, string } from 'prop-types';

import CreateCommentToAnswer from './CreateCommentToAnswer';
import { calculateTimeAgo } from '../../../../helpers/calculateTimeAgo';
import HideableText from '../../../../components/HideableText';
import AnswerComments from './AnswerComments';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import AddQuestionModal from '../../DoctorQuestions/AddQuestionModal';
import answersAPI from '../../../../api/answers-api';
import colors from '../../../../theme/colors';
import styles from './styles';

const Answers = ({
  isSignedIn,
  commentableId,
  userId,
  token,
  renderHeader,
}) => {
  const [addQuestionVisible, setAddQuestionVisible] = useState(false);

  const {
    data,
    status,
    fetchNextPage,
    fetchPreviousPage,
    hasNextPage,
    hasPreviousPage,
    isFetchingNextPage,
    isFetchingPreviousPage,
    ...rest
  } = useInfiniteQuery(
    ['answers', commentableId],
    ({ pageParam = 1 }) =>
      answersAPI.getAnswers(commentableId, userId, token, pageParam),
    {
      getNextPageParam: (lastPage, allPages) => lastPage.nextCursor,
    },
  );

  if (status === 'loading') {
    return (
      <View>
        {renderHeader()}
        <Text>Loading...</Text>
      </View>
    );
  }

  const renderItem = ({ item, index }) => {
    return (
      <View>
        {item.data.data.map((answer) => {
          const {
            id,
            attributes: {
              text,
              created_at: createdAt,
              answered_by_doctor: answeredByDoctor,
              answered_by_nurse: answeredByNurse,
              user_age: userAge,
              user_gender: userGender,
              user_name: userName,
              comments_count: commentsCount,
            },
          } = answer;

          const answerBy = `${userName || 'Anonymous'}${
            !!userGender
              ? ', ' + userGender.charAt(0).toUpperCase() + userGender.slice(1)
              : ''
          }${!!userAge ? ', ' + userAge + ' y/o' : ''}`;

          return (
            <View key={id.toString()} style={styles.container}>
              <View style={styles.flexRow}>
                {answeredByDoctor ||
                  (true && (
                    <>
                      <OcticonsIcon
                        name="check"
                        size={20}
                        color={colors.blue}
                      />
                      <Text style={styles.verifiedDoctorText}>
                        Verified Doctor
                      </Text>
                    </>
                  ))}
                <Text style={styles.timeAgoText}>
                  {calculateTimeAgo(createdAt)}
                </Text>
              </View>
              <View style={[styles.flexRow, styles.answeredUserContainer]}>
                <Image
                  source={{
                    uri:
                      'https://pbs.twimg.com/profile_images/740272510420258817/sd2e6kJy_400x400.jpg',
                  }}
                  width={undefined}
                  height={undefined}
                  style={styles.avatar}
                />
                <View>
                  <Text style={styles.answeredUserLabel}>Answered by</Text>
                  <Text style={styles.answeredUserText}>{answerBy}</Text>
                </View>
              </View>
              <HideableText
                text={text}
                textStyle={styles.answerText}
                buttonStyle={{ color: colors.green }}
              />
              <CreateCommentToAnswer
                commentsCount={commentsCount}
                isSignedIn={isSignedIn}
                answerId={id}
                modalTitle={`Answer posted by ${answerBy}`}
              />
              {commentsCount ? (
                <AnswerComments
                  commentableId={id.toString()}
                  commentsCount={commentsCount}
                />
              ) : null}
            </View>
          );
        })}
      </View>
    );
  };

  return (
    <View>
      <FlatList
        data={data.pages}
        renderItem={renderItem}
        onEndReached={() => {
          if (hasNextPage) {
            fetchNextPage();
          }
        }}
        keyExtractor={(_, index) => index.toString()}
        ListHeaderComponent={renderHeader()}
      />
      <View style={[styles.addButton, { top: 40, right: 15 }]}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => setAddQuestionVisible(!addQuestionVisible)}
        >
          <AntDesignIcon name="pluscircle" size={60} color={colors.blue} />
        </TouchableOpacity>
      </View>
      <AddQuestionModal
        visible={addQuestionVisible}
        setVisible={setAddQuestionVisible}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    isSignedIn: state.authentication.isSignedIn,
    userId: state.authentication.userInfo.id,
    token: state.authentication.userToken,
  };
};

export default connect(mapStateToProps)(Answers);

Answers.propTypes = {
  isSignedIn: bool,
  commentableId: string,
  userId: number,
  token: string,
  renderHeader: func,
  showModal: func,
};
