import React, { useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from 'react-native';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { bool, func, number, string } from 'prop-types';

import { SwipeableModal } from '../../../../components/index';
import { postQuestionFetch } from '../../../../redux/modules/DoctorQuestions/actions';
import RNButton from '../../../../components/RNButton';
import colors from '../../../../theme/colors';
import styles from './styles';
import { useMutation } from 'react-query';
import answersAPI from '../../../../api/answers-api';
import { SafeAreaView } from 'react-native-safe-area-context';

const validate = (values) => {
  const errors = {};
  if (!values.text) errors.text = 'Required';
  return errors;
};

const CreateAnswerModal = ({
  commentableId,
  userId,
  token,
  visible,
  setVisible,
}) => {
  const mutation = useMutation((text) => {
    return answersAPI.createAnswer(commentableId, userId, token, text);
  });

  const onSubmit = ({ text }) => {
    mutation.mutate(text);
  };

  useEffect(() => {
    if (!visible) {
      Keyboard.dismiss();
    }
  }, [visible]);

  return (
    <SwipeableModal visible={visible} setVisible={setVisible}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
          style={styles.container}
        >
          <View style={styles.dropDownIcon} />
          <View style={styles.modalContainer}>
            <Formik
              initialValues={{ text: '' }}
              onSubmit={onSubmit}
              validate={validate}
            >
              {({ handleChange, handleBlur, handleSubmit, values, errors }) => {
                return (
                  <View>
                    <TextInput
                      multiline={true}
                      onChangeText={handleChange('text')}
                      onBlur={handleBlur('text')}
                      value={values.text}
                      placeholder="Write your answer"
                      placeholderTextColor={colors.greyText}
                      style={styles.input}
                    />

                    <RNButton
                      label="Post reply"
                      disabled={
                        Object.keys(errors).length !== 0 || values.text === ''
                      }
                      backgroundColor={colors.blue}
                      onPress={handleSubmit}
                    />
                  </View>
                );
              }}
            </Formik>
            <TouchableOpacity
              style={styles.closeIconButton}
              onPress={() => {
                setVisible(!visible);
                Keyboard.dismiss();
              }}
            >
              <FontAwesomeIcon
                name="angle-down"
                size={30}
                color={colors.green}
              />
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </SwipeableModal>
  );
};

const mapStateToProps = (state) => {
  return {
    userId: state.authentication.userInfo.id,
    token: state.authentication.userToken,
  };
};

const mapDispatchToProps = {
  postQuestion: postQuestionFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateAnswerModal);

CreateAnswerModal.propTypes = {
  commentableId: string,
  userId: number,
  token: string,
  visible: bool,
  setVisible: func,
  postQuestion: func,
};
