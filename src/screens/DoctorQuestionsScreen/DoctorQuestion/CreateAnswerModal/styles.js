import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  dropDownIcon: {
    height: 5,
    width: 50,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  modalContainer: {
    position: 'relative',
    width,
    marginTop: 10,
    paddingBottom: 10,
    paddingHorizontal: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: colors.white,
  },
  closeIconButton: {
    position: 'absolute',
    top: 16,
    right: 11,
  },
  input: {
    height: 150,
    marginTop: 40,
    marginBottom: 13,
    borderBottomWidth: 1,
    marginHorizontal: 5,
    borderBottomColor: colors.concrete,
    fontSize: 17,
    fontWeight: 'bold',
    letterSpacing: 0.5,
  },
  attachText: {
    marginBottom: 30,
    fontSize: 14,
    fontWeight: 'bold',
    color: colors.green,
  },
  imagesContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  attachedImageContainer: {
    marginRight: 20,
    marginBottom: 25,
    position: 'relative',
  },
  attachedImage: {
    width: (width - 15 * 2 - 3 * 20) / 4,
    height: (width - 15 * 2 - 3 * 20) / 4,
  },
  deleteImageButton: {
    position: 'absolute',
    top: -((width - 15 * 2 - 3 * 20) / 4) - 15,
    right: -15,
    backgroundColor: colors.white,
    borderRadius: 15,
  },
  centeredModalView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.basicBgOpacity,
  },
  modalView: {
    paddingTop: 15,
    width: width - 30,
    backgroundColor: colors.white,
  },
  modalText: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 40,
    fontSize: 25,
    fontWeight: 'bold',
  },
  addImageVariantButton: {
    fontSize: 20,
    paddingHorizontal: 15,
    paddingVertical: 15,
    backgroundColor: colors.white,
  },
});

export default styles;
