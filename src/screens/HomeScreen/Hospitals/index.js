import React, { useEffect } from 'react';
import { Image, Text, View } from 'react-native';
import {
  FlatList,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import { array, bool, func, number } from 'prop-types';

import actions from '../../../redux/modules/Home/actions';
import { SearchButton, StarsReview } from '../../../components';
import ReloadingIndicator from '../../../components/ReloadingIndicator';
import { Header } from './Header';
import styles from './styles';

const { loadHospitalsFetch } = actions;

const Hospitals = ({
  hospitals,
  page,
  allLoaded,
  loadHospitals,
  loading,
  loaded,
}) => {
  useEffect(() => {
    loadHospitals();
  }, []);

  const navigation = useNavigation();

  if ((loading || !loaded) && page === 1) {
    return <ReloadingIndicator height={100} />;
  }

  const handleLoadMore = () => {
    if ((!loading || loaded) && !allLoaded) {
      loadHospitals();
    }
  };

  const renderFooter = () => {
    if (!loading) return null;
    return (
      <View style={styles.loaderContainer}>
        <Text style={styles.loaderText}>Loading...</Text>
      </View>
    );
  };

  const renderItem = ({ item }) => {
    return (
      <TouchableWithoutFeedback
        onPress={() =>
          navigation.navigate('HospitalsStackScreen', {
            screen: 'Hospital',
            params: { uuid: item.id },
          })
        }
        style={styles.itemContainer}
      >
        <View style={styles.itemImageContainer}>
          <Image
            width={undefined}
            height={undefined}
            source={{
              uri:
                'https://media-cdn.tripadvisor.com/media/photo-s/16/d0/ed/49/the-img-hospital-is-a.jpg',
            }}
            style={styles.image}
          />
        </View>
        <View style={styles.itemTextContainer}>
          <Text style={styles.textText}>{item.name}</Text>
          <StarsReview avgReview={item.avgReview} />
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <>
      <SearchButton params={{ searchCategory: 'hospitals' }}>
        <View style={styles.searchContainer}>
          <Icon name="map-marker" size={25} color="blue" />
          <Text style={styles.searchText}>Search for a hospital</Text>
        </View>
      </SearchButton>
      <FlatList
        renderItem={renderItem}
        data={hospitals}
        onEndReached={handleLoadMore}
        keyExtractor={(item) => item.id.toString()}
        onEndReachedThreshold={1}
        ListHeaderComponent={Header}
        ListFooterComponent={renderFooter}
      />
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    hospitals: state.home.hospitals.hospitals,
    page: state.home.hospitals.page,
    allLoaded: state.home.hospitals.hospitalsAllLoaded,
    loading: state.home.hospitals.loading,
    loaded: state.home.hospitals.loaded,
  };
};

const mapDispatchToProps = { loadHospitals: loadHospitalsFetch };

export default connect(mapStateToProps, mapDispatchToProps)(Hospitals);

Hospitals.propTypes = {
  hospitals: array,
  page: number,
  allLoaded: bool,
  loadHospitals: func,
  loading: bool,
  loaded: bool,
};
