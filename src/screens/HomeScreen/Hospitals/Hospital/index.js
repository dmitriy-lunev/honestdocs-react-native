import { useNavigation, useRoute } from '@react-navigation/native';
import React from 'react';
import { Image, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useQuery } from 'react-query';
import { SearchButton, StarsReview } from '../../../../components';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import HTML from 'react-native-render-html';

import homeAPI from '../../../../api/home-api';
import styles from './styles';
import colors from '../../../../theme/colors';

const tagStyles = {
  p: { marginBottom: 15, fontSize: 16 },
  a: { fontSize: 16, textDecorationLine: 'none' },
  li: { fontSize: 16 },
  h1: { fontSize: 18, marginBottom: 15 },
  h2: { fontSize: 18, marginBottom: 15 },
  h3: { fontSize: 18, marginBottom: 15 },
};

const getArticleById = async (id) => {
  const { data } = await homeAPI.getHospital(id);
  const {
    name,
    avg_review: avgReview,
    description,
    phone,
    image,
    address,
    working_hours: workingHours,
  } = data.data.attributes;
  return { name, avgReview, description, phone, image, address, workingHours };
};

function useHospital(hospitalId) {
  return useQuery(['hospital', hospitalId], () => getArticleById(hospitalId), {
    enabled: !!hospitalId,
  });
}

const Hospital = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const { uuid: hospitalId } = route.params;

  const { status, data, error, isFetching } = useHospital(hospitalId);

  const handlePush = (uuid) => {
    navigation.push('Article', { uuid });
  };

  if (status === 'loading') {
    return (
      <View style={styles.loaderContainer}>
        <Text>loading...</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <SearchButton params={{ searchCategory: 'hospitals' }}>
        <View style={styles.searchContainer}>
          <FontAwesomeIcon name="map-marker" size={25} color="blue" />
          <Text style={styles.searchText}>Search for a hospital</Text>
        </View>
      </SearchButton>

      <ScrollView style={styles.contentContainer}>
        <Text style={styles.name}>{data.name}</Text>
        <Image
          source={{
            uri:
              'https://im.kommersant.ru/Issues.photo/OGONIOK/2019/019/KMO_148127_01452_1_t218_221732.jpg',
          }}
          width={undefined}
          height={undefined}
          style={styles.image}
        />
        <StarsReview avgReview={data.avgReview} starSize={25} />
        <Text style={styles.address}>
          {data.address ? data.address : 'No information'}
        </Text>
        <View style={styles.rowView}>
          <FontAwesomeIcon name="phone-square" size={20} color={colors.green} />
          <Text style={styles.phone}>{data.phone}</Text>
        </View>
        <View style={styles.rowView}>
          <FontAwesomeIcon name="clock-o" size={20} color={colors.green} />
          <Text style={styles.workingHourTitle}>Operating hours</Text>
        </View>
        <View style={styles.workingHoursContainer}>
          {data.workingHours.map((item, index) => {
            return (
              <View key={item.day + index} style={styles.workingHoursItem}>
                <Text style={styles.workingHoursDay}>{`${item.day}:  `}</Text>
                <Text
                  style={styles.workingHoursText}
                >{`From: ${item.from} To ${item.to}`}</Text>
              </View>
            );
          })}
        </View>
        <HTML source={{ html: data.description }} tagsStyles={tagStyles} />
      </ScrollView>
    </View>
  );
};

export default Hospital;
