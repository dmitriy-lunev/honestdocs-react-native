import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../../theme/colors';

const { width } = Dimensions.get('window');
const ratio = 544 / 968;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  searchContainer: {
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    width: '90%',
  },
  searchText: {
    paddingLeft: 15,
    color: 'grey',
    fontSize: 20,
    fontWeight: 'bold',
  },
  contentContainer: {
    paddingHorizontal: 15,
    paddingTop: 30,
  },
  image: {
    width: '100%',
    height: width * ratio,
    marginVertical: 20,
    borderRadius: 10,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  address: {
    fontSize: 16,
    marginVertical: 20,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
  phone: {
    paddingLeft: 10,
    fontSize: 16,
    color: colors.textGreyColor,
  },
  workingHourTitle: {
    paddingLeft: 10,
    fontSize: 16,
    fontWeight: 'bold',
  },
  workingHoursContainer: {
    alignItems: 'center',
    marginBottom: 10,
  },
  workingHoursItem: {
    marginVertical: 4,
    width: 140,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  workingHoursDay: {
    color: colors.green,
    fontSize: 16,
    fontWeight: 'bold',
  },
  workingHoursText: {
    fontSize: 16,
    color: colors.textGreyColor,
  },
});

export default styles;
