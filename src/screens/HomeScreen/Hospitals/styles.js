import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 30,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 8,
    width: '90%',
  },
  searchText: {
    paddingLeft: 15,
    color: 'grey',
    fontSize: 20,
    fontWeight: 'bold',
  },
  itemContainer: {
    backgroundColor: 'white',
    paddingTop: 25,
    paddingHorizontal: 15,
    flexDirection: 'row',
  },
  itemImageContainer: {
    // marginRight: 20,
  },
  itemTextContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },
  textText: {
    fontSize: 20,
    marginBottom: 10,
  },
  sliderContainer: {
    paddingBottom: 15,
    marginTop: 0,
  },
  flatListTitle: {
    paddingTop: 15,
    paddingHorizontal: 15,
    marginTop: 10,
    backgroundColor: 'white',
    fontSize: 14,
    fontWeight: 'bold',
  },
  loaderContainer: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgb(8, 130, 81)',
  },
  loaderText: {
    color: 'white',
    fontSize: 16,
  },
  image: {
    height: 100,
    width: 120,
    paddingRight: 10,
  },
});

export default styles
