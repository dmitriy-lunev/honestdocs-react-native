import React from 'react'
import { Text } from 'react-native';

import HospitalsSlider from './HospitalsSlider';
import styles from './styles'

export const Header = () => (
  <>
    <HospitalsSlider
      title={'Hospitals Nearby'}
      path={'Hospital'}
      linkText={null}
      style={styles.sliderContainer}
    />
    <Text style={styles.flatListTitle}>Top Rated Hospitals</Text>
  </>
);
