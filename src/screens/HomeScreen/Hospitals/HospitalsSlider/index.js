import React, { memo, useEffect } from 'react';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { string, object, bool, array, func } from 'prop-types';

import { loadHomepageHospitalsFetch } from '../../../../redux/modules/Home/Hospitals/actions';
import { Slider } from '../../../../components';
import { renderItem } from './renderItem';
import ReloadingIndicator from '../../../../components/ReloadingIndicator';

const { width } = Dimensions.get('window');

const HospitalsSlider = ({
  title,
  path,
  linkText,
  withLink,
  loading,
  loaded,
  homepageHospitals,
  loadHomepageHospitals,
  containerStyle,
  ...rest
}) => {
  useEffect(() => {
    if (homepageHospitals.length === 0) {
      loadHomepageHospitals();
    }
  }, []);

  // if (loading || !loaded) {
  //   return <ReloadingIndicator height={336.5} />;
  // }

  return (
    <Slider
      data={homepageHospitals}
      itemWidth={width - 50}
      renderItem={renderItem}
      loop={true}
      title={title}
      path={path}
      linkText={linkText}
      withLink={withLink}
      containerStyle={containerStyle}
      {...rest}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.home.hospitals.loading,
    loaded: state.home.hospitals.loaded,
    homepageHospitals: state.home.hospitals.homepageHospitals,
  };
};

const mapDispatchToProps = {
  loadHomepageHospitals: loadHomepageHospitalsFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(HospitalsSlider);

HospitalsSlider.propTypes = {
  title: string,
  linkText: string,
  style: object,
  path: string,
  withLink: bool,
  loading: bool,
  loaded: bool,
  homepageHospitals: array,
  loadHomepageHospitals: func,
  containerStyle: object,
};

HospitalsSlider.defaultProps = {
  linkText: 'view all',
  withLink: false,
  containerStyle: {},
  homepageHospitals: null,
};
