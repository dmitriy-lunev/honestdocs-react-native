import React, { useState } from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { navigate } from '../../../../services/navigation';

export const renderItem = (item) => {
  return (
    <TouchableWithoutFeedback
      style={styles.container}
      onPress={() =>
        navigate('HospitalsStackScreen', {
          screen: 'Hospital',
          params: { uuid: item.id },
        })
      }
    >
      <ImageBackground
        source={{
          uri:
            'https://naurok-test.nyc3.cdn.digitaloceanspaces.com/uploads/test/510013/91297/676499_1585497084.jpg',
        }}
        style={styles.backgroundImage}
      >
        <ImageBackground
          source={{
            uri: `https://staging.honestdocs.co/${item.image}`,
          }}
          style={styles.backgroundImage}
        >
          <View style={styles.content}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.text}>{item.address}</Text>
            <Text style={styles.text}>{item.phone}</Text>
          </View>
        </ImageBackground>
      </ImageBackground>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
  },
  content: {
    marginTop: 100,
    paddingBottom: 30,
    backgroundColor: 'white',
    paddingLeft: 20,
    paddingRight: 50,
  },
  name: {
    paddingTop: 15,
    fontSize: 17,
    fontWeight: 'bold',
  },
  text: {
    paddingTop: 15,
    fontSize: 14,
    color: 'grey',
  },
});
