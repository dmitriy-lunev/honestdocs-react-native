import { StyleSheet } from 'react-native';
import colors from '../../../../theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  headerText: {
    fontSize: 15,
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 10,
    paddingHorizontal: 15,
    backgroundColor: 'white',
  },
  itemContainer: {
    flexDirection: 'row',
    marginVertical: 10,
    marginHorizontal: 15,
  },
  itemImage: {
    width: 110,
    height: 75,
    marginRight: 15,
    borderRadius: 5,
  },
  itemInfo: {
    flex: 1,
    justifyContent: 'center',
  },
  itemText: {
    fontSize: 17,
  },
  promotedContainer: {
    marginTop: 30,
    marginBottom: 10,
    paddingVertical: 20,
    marginHorizontal: 15,
    borderColor: 'grey',
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  promotedHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  promotedHeaderText: {
    flex: 1,
    paddingRight: 15,
  },
  promotedText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'grey',
  },
  promotedTitle: {
    marginTop: 10,
    fontSize: 17,
  },
  promotedDescription: {
    marginTop: 15,
  },
  promotedImage: {
    width: 105,
    height: 105,
    borderRadius: 5,
  },
  promotedLinkContainer: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  promotedLinkText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.green,
    marginRight: 5,
  },
  loaderContainer: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.green,
  },
  loaderText: {
    color: 'white',
    fontSize: 16,
  },
});

export default styles;
