import { array, bool, func, node, object } from 'prop-types';
import React, { PureComponent } from 'react';
import { FlatList, Image, Text, View } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { navigate } from '../../../../services/navigation';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import styles from './styles';
import colors from '../../../../theme/colors';

class ArticleList extends PureComponent {
  constructor(props) {
    super(props);
    this.renderPromoted = this.renderPromoted.bind(this);
    this.renderItem = this.renderItem.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
  }

  handleLoadMore = () => {
    if ((!this.props.loading || this.props.loaded) && !this.props.allLoaded) {
      this.props.loadMore();
    }
  };

  renderPromoted = (index) => {
    if ((index + 1) % 5 === 0) {
      return (
        <TouchableWithoutFeedback onPress={() => navigate('HDmall', {})}>
          <View style={styles.promotedContainer}>
            <View style={styles.promotedHeaderContainer}>
              <View style={styles.promotedHeaderText}>
                <Text style={styles.promotedText}>Promoted by HonestDocs</Text>
                <Text style={styles.promotedTitle}>
                  {this.props.promoted.title}
                </Text>
                <Text style={styles.promotedDescription}>
                  {this.props.promoted.description}
                </Text>
              </View>
              <Image
                width={undefined}
                height={undefined}
                source={{
                  uri:
                    'https://www.gettyimages.com/gi-resources/images/frontdoor/editorial/Velo/GettyImages-Velo-1088643550.jpg',
                }}
                style={styles.promotedImage}
              />
            </View>
            <View style={styles.promotedLinkContainer}>
              <Text style={styles.promotedLinkText}>See details</Text>
              <Icon name="share-alt" size={12} color={colors.green} />
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      return null;
    }
  };

  // eslint-disable-next-line
  renderItem = ({ item, index }) => {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback
          onPress={() =>
            this.props.navigation.navigate('BlogStackScreen', {
              screen: 'Article',
              params: { uuid: item.id },
            })
          }
        >
          <View style={styles.itemContainer}>
            <Image
              width={undefined}
              height={undefined}
              source={{
                uri:
                  'https://images.unsplash.com/photo-1471864190281-a93a3070b6de?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MzB8fG1lZGljaW5lfGVufDB8fDB8&w=1000&q=80',
              }}
              style={styles.itemImage}
            />
            <View style={styles.itemInfo}>
              <Text numberOfLines={3} style={styles.itemText}>
                {item.title}
              </Text>
            </View>
          </View>
        </TouchableWithoutFeedback>
        {this.renderPromoted(index)}
      </View>
    );
  };

  renderHeader = () => {
    return (
      <>
        {this.props.header}
        <Text style={styles.headerText}>Recommended for you</Text>
      </>
    );
  };

  renderFooter = () => {
    if (!this.props.loading) return null;
    return (
      <View style={styles.loaderContainer}>
        <Text style={styles.loaderText}>Loading...</Text>
      </View>
    );
  };
  render() {
    return (
      <FlatList
        renderItem={this.renderItem}
        data={this.props.blogArticles}
        onEndReached={this.handleLoadMore}
        keyExtractor={(item) => item.id.toString()}
        onEndReachedThreshold={0.7}
        ListHeaderComponent={this.renderHeader}
        ListFooterComponent={this.renderFooter}
        refreshControl={this.props.refreshControl}
      />
    );
  }
}

ArticleList.propTypes = {
  loading: bool,
  loaded: bool,
  allLoaded: bool,
  blogArticles: array,
  promoted: object,
  loadMore: func,
  refreshControl: node,
  children: node,
  header: object,
};

ArticleList.defaultProps = {};

export default ArticleList;
