import React, { useState } from 'react';
import { View } from 'react-native';

import { SearchButton } from '../../../components';
import { HomepageNewsFeed } from './components';
import Header from './Header';
import styles from './styles';

const Homepage = () => {
  const [visible, setVisible] = useState(true);

  return (
    <View style={styles.container}>
      <SearchButton />
      <HomepageNewsFeed header={<Header />} />
    </View>
  );
};

export default Homepage;
