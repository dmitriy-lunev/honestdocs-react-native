import React, { memo } from 'react';
import { StyleSheet } from 'react-native';

import {
  DoctorQASlider,
  HDmallHomepage,
  HomepageBanners,
  MiniAppsSlider,
  Services,
} from './components';
import HospitalsSlider from '../Hospitals/HospitalsSlider';
import NewsFeedSlider from '../Blog/NewsFeedSlider';

const Header = () => {
  return (
    <>
      <Services />
      <HDmallHomepage />
      <HomepageBanners />
      <MiniAppsSlider containerStyle={styles.marginBottom} />
      <HospitalsSlider
        title="Hospitals"
        path="HospitalsStackScreen"
        withLink
        containerStyle={styles.marginBottom}
      />
      <DoctorQASlider containerStyle={styles.marginBottom} />
      <NewsFeedSlider withLink />
    </>
  );
};

const styles = StyleSheet.create({
  marginBottom: {
    marginBottom: 10,
  },
});

export default memo(Header);
