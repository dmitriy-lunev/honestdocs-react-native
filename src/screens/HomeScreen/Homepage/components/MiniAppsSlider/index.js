import React from 'react';
import { Dimensions } from 'react-native';
import { object } from 'prop-types';

import { Slider } from '../../../../../components';
import { renderItem } from './renderItem';

const { width } = Dimensions.get('window');

const data = [
  {
    id: 'CT',
    title: 'Calculators and Trackers',
    apps: [
      { id: 'BMI', title: 'Body Mass Index (BMI) Calculator' },
      { id: 'WT', title: 'Water Intake Tracker' },
    ],
  },
  {
    title: 'Reference Tables',
    id: 'RT',
    apps: [{ id: 'PT', title: 'Period Tracker' }],
  },
];

const MiniAppsSlider = ({ containerStyle }) => {
  return (
    <Slider
      title="Mini Apps"
      path="MiniAppsStack"
      withLink
      data={data}
      itemWidth={width - 50}
      renderItem={renderItem}
      loop={true}
      {...{ containerStyle }}
    />
  );
};

export default MiniAppsSlider;

MiniAppsSlider.propTypes = {
  containerStyle: object,
};
