import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

import { navigate } from '../../../../../services/navigation';

export const renderItem = (item) => {
  const handleNavigation = (path) => navigate(path);

  return (
    <>
      <Text style={styles.title}>{item.title}</Text>
      <View>
        {item.apps.map((app, index) => (
          <View key={app.id.toString()}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => handleNavigation('MiniAppsStack')}
            >
              <Text style={styles.text}>{app.title}</Text>
              <Icon name="angle-right" color={'grey'} size={20} />
            </TouchableOpacity>
            {index + 1 !== item.apps.length ? (
              <View style={styles.border} />
            ) : null}
          </View>
        ))}
        {item.apps.length === 1 ? (
          <>
            <View style={styles.border} />
            <View style={[styles.button]} />
          </>
        ) : null}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 15,
    paddingLeft: 20,
    paddingTop: 20,
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    borderBottomColor: 'grey',
  },
  text: {
    fontSize: 14,
  },
  border: {
    height: 1,
    width: '100%',
    backgroundColor: 'grey',
  },
});
