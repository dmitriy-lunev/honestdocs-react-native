import { array, bool, func, object } from 'prop-types';
import React, { useCallback, useEffect, useState } from 'react';
import { RefreshControl } from 'react-native';
import { connect } from 'react-redux';
import { loadBlogArticlesFetch } from '../../../../../redux/modules/Home/Blog/actions';
import { useNavigation } from '@react-navigation/native';

import ArticleList from '../../../components/ArticlesList';
import { wait } from '../../../../../helpers/wait';

const HomepageNewsFeed = ({
  loading,
  loaded,
  allLoaded,
  blogArticles,
  promoted,
  loadBlogArticles,
  header,
}) => {
  const navigation = useNavigation();

  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    // dispatch(loadBannerFetch());
    // dispatch(loadShopCategoriesFetch());
    // dispatch(loadHomepageHospitalsFetch());
    // dispatch(loadHomepageDoctorQAFetch());

    wait(2000).then(() => setRefreshing(false));
  }, []);

  useEffect(() => {
    loadBlogArticles();
    return () => {};
  }, []);

  const loadMore = () => loadBlogArticles();

  return (
    <ArticleList
      {...{
        loading,
        loaded,
        allLoaded,
        blogArticles,
        promoted,
        loadMore,
        header,
      }}
      navigation={navigation}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
          color="rgb(8, 130, 81)"
        />
      }
    />
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.home.blog.loading,
    loaded: state.home.blog.loaded,
    allLoaded: state.home.blog.allLoaded,
    blogArticles: state.home.blog.blogArticles,
    promoted: state.home.blog.promoted,
  };
};

const mapDispatchToProps = {
  loadBlogArticles: loadBlogArticlesFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomepageNewsFeed);

HomepageNewsFeed.propTypes = {
  loading: bool,
  loaded: bool,
  allLoaded: bool,
  blogArticles: array,
  promoted: object,
  loadBlogArticles: func,
  header: object,
};
