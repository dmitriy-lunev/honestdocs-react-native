import { Dimensions, StyleSheet } from 'react-native';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  servicesContainer: {
    padding: 20,
    backgroundColor: '#fff',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  tabsContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tab: {
    paddingTop: 20,
    paddingBottom: 20,
    height: 120,
    width: (width - 40) / 4 - 8,
    paddingHorizontal: 5,
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.3,
    shadowRadius: 8,
    elevation: 13,
  },
  textServices: {
    fontSize: 12,
    textAlign: 'center',
    paddingHorizontal: 7,
  },
  icon: {
    marginBottom: 10,
    width: 50,
    height: 50,
  },
});

export default styles;
