import React from 'react';
import { Image, Text, View } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import images from '../../../../../assets';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

const services = [
  { title: 'Doctor Q&A', path: 'DoctorQA', image: 'doctor' },
  { title: 'Book Hospital', path: 'HospitalsStackScreen', image: 'hospital' },
  { title: 'Mini Apps', path: 'MiniAppsStack', image: 'health_tools' },
  { title: 'News Feed', path: 'BlogStackScreen', image: 'news_feed' },
];

const Services = () => {
  const navigation = useNavigation();

  const handleNavigate = (path) => {
    navigation.navigate(path, { screen: 'TabsStack' });
  };

  return (
    <View style={styles.servicesContainer}>
      <Text style={styles.text}>Services</Text>
      <View style={styles.tabsContainer}>
        {services.map((item) => (
          <TouchableWithoutFeedback
            onPress={() => handleNavigate(item.path)}
            style={styles.tab}
            key={item.path}
          >
            <Image
              source={images[item.image]}
              width={20}
              height={20}
              style={styles.icon}
            />
            <Text style={styles.textServices}>{item.title}</Text>
          </TouchableWithoutFeedback>
        ))}
      </View>
    </View>
  );
};

export default Services;
