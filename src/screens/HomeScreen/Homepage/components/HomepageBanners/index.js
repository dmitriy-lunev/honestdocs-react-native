import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { array, bool, func } from 'prop-types';

import { Banners } from '../../../../../components';
import { loadBannersFetch } from '../../../../../redux/modules/Home/Homepage/actions';

const HomepageBanners = ({ loading, loaded, banners, loadBanners }) => {
  useEffect(() => {
    loadBanners();
    return () => {};
  }, []);

  return <Banners {...{ loading, loaded, banners }} />;
};

const mapStateToProps = (state) => {
  return {
    loading: state.home.homepage.loading,
    loaded: state.home.homepage.loaded,
    banners: state.home.homepage.banners,
  };
};

const mapDispatchToProps = {
  loadBanners: loadBannersFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomepageBanners);

HomepageBanners.propTypes = {
  loading: bool,
  loaded: bool,
  banners: array,
  loadBanners: func,
};
