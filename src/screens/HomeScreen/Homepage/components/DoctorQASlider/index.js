import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Dimensions } from 'react-native';
import { array, bool, func, object } from 'prop-types';

import { loadHomepageDoctorQAFetch } from '../../../../../redux/modules/Home/DoctorQA/actions';
import { renderItem } from './renderItem';
import { Slider } from '../../../../../components';
import ReloadingIndicator from '../../../../../components/ReloadingIndicator';

const { width } = Dimensions.get('window');

const DoctorQASlider = ({
  loading,
  loaded,
  homepageDoctorQA,
  loadHomepageDoctorQA,
  containerStyle,
  ...rest
}) => {
  useEffect(() => {
    loadHomepageDoctorQA();
    return () => {};
  }, []);

  if (loading || !loaded) {
    return <ReloadingIndicator height={349.5} />;
  }

  return (
    <Slider
      title="Doctor Q&A"
      path="DoctorQA"
      withLink
      data={homepageDoctorQA}
      itemWidth={width - 50}
      renderItem={renderItem}
      loop={true}
      {...{ containerStyle }}
      {...rest}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.home.doctorQA.loading,
    loaded: state.home.doctorQA.loaded,
    homepageDoctorQA: state.home.doctorQA.homepageDoctorQA,
  };
};

const mapDispatchToProps = {
  loadHomepageDoctorQA: loadHomepageDoctorQAFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(DoctorQASlider);

DoctorQASlider.propTypes = {
  loading: bool,
  loaded: bool,
  homepageDoctorQA: array,
  loadHomepageDoctorQA: func,
  containerStyle: object,
};
DoctorQASlider.defaultProps = {};
