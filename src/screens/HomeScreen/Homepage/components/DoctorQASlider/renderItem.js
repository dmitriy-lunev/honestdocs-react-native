import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { navigate } from '../../../../../services/navigation';

const formatDate = (data) => data;

export const renderItem = (item) => {
  return (
    <>
      <View style={styles.content}>
        <Text numberOfLines={3} style={styles.name}>
          {item.title}
        </Text>
        <View style={styles.doctor}>
          <Image />
          <View>
            <Text style={styles.text}>{item.answer.doctor}</Text>
            <Text style={styles.text}>
              {`Answered ${formatDate(item.answer.created_at)}`}
            </Text>
          </View>
        </View>
        <Text numberOfLines={2} style={[styles.text, styles.answer]}>
          {item.answer.text}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={() =>
          navigate('DoctorQA', {
            screen: 'DoctorQA',
            uuid: item.id.toString(),
          })
        }
      >
        <Text style={styles.buttonLabel}>View more</Text>
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  content: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
  },
  name: {
    paddingTop: 15,
    fontSize: 17,
    fontWeight: 'bold',
    height: 100,
  },
  text: {
    fontSize: 14,
    color: 'grey',
  },
  doctor: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  answer: {
    marginBottom: 35,
    height: 40,
  },
  button: {
    borderTopColor: 'grey',
    borderTopWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 12,
  },
  buttonLabel: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});
