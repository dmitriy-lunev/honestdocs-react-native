import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../../../theme/colors';

const { width } = Dimensions.get('window');
const tabWidth = width / 3 - 20;
const iconSize = tabWidth - 20;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    padding: 20,
    height: 230,
    backgroundColor: '#fff',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  linkText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: colors.green,
  },
  tabContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tab: {
    paddingTop: 20,
    width: tabWidth,
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  tabIcon: {
    height: iconSize,
    width: iconSize,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: iconSize / 2,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.3,
    shadowRadius: 8,
    elevation: 15,
  },
  tabText: {
    paddingTop: 10,
    fontSize: 16,
    textAlign: 'center',
  },
  image: {
    height: 0.6 * iconSize,
    width: 0.6 * iconSize,
  },
});

export default styles;
