import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Image, Text, View } from 'react-native';
import {
  BorderlessButton,
  TouchableOpacity,
} from 'react-native-gesture-handler';
import { array, bool, func } from 'prop-types';

import { loadShopCategoriesFetch } from '../../../../../redux/modules/Home/Homepage/actions';
import ReloadingIndicator from '../../../../../components/ReloadingIndicator';
import styles from './styles';
import { useNavigation } from '@react-navigation/native';

const HDmallHomepage = ({
  loading,
  loaded,
  shopCategories,
  loadShopCategories,
}) => {
  useEffect(() => {
    loadShopCategories();
  }, []);

  const navigation = useNavigation();

  const handleNavigation = (path) => {
    navigation.navigate(path, {});
  };

  if (loading || !loaded) {
    return <ReloadingIndicator height={230} />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.text}>HDmall</Text>
        <TouchableOpacity onPress={() => handleNavigation('HDmall')}>
          <Text style={styles.linkText}>view all</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.tabContainer}>
        {shopCategories.map(({ title, image }, index) => (
          <View style={styles.tab} key={index}>
            <BorderlessButton
              activeOpacity={1}
              borderless={false}
              onPress={() => handleNavigation('HDmall')}
            >
              <View style={styles.tabIcon}>
                <Image
                  height={undefined}
                  width={undefined}
                  style={styles.image}
                  source={{ uri: image }}
                />
              </View>
              <Text style={styles.tabText}>{title}</Text>
            </BorderlessButton>
          </View>
        ))}
      </View>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.home.homepage.loading,
    loaded: state.home.homepage.loaded,
    shopCategories: state.home.homepage.shopCategories,
  };
};

const mapDispatchToProps = {
  loadShopCategories: loadShopCategoriesFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(HDmallHomepage);

HDmallHomepage.propTypes = {
  loading: bool,
  loaded: bool,
  shopCategories: array,
  loadShopCategories: func,
};
