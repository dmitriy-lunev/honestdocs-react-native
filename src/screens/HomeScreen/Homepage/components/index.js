export { default as HDmallHomepage } from './HDmallHomepage';
export { default as Services } from './Services';
export { default as MiniAppsSlider } from './MiniAppsSlider';
export { default as DoctorQASlider } from './DoctorQASlider';
export { default as HomepageNewsFeed } from './HomepageNewsFeed';
export { default as HomepageBanners } from './HomepageBanners';
