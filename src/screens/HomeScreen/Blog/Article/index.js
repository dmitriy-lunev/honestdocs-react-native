import { useNavigation, useRoute } from '@react-navigation/native';
import axios from 'axios';
import React, { useState } from 'react';
import {
  Button,
  Image,
  Modal,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  ScrollView,
  TouchableOpacity as GestureHandlerTouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import { useQuery } from 'react-query';
import { DropDownView, SearchButton } from '../../../../components';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Entypo from 'react-native-vector-icons/Entypo';

import styles from './styles';
import HTML from 'react-native-render-html';
import colors from '../../../../theme/colors';

const tagStyles = {
  p: { marginBottom: 15, fontSize: 16 },
  a: { fontSize: 16, textDecorationLine: 'none' },
  li: { fontSize: 16 },
  h1: { fontSize: 18, marginBottom: 15 },
  h2: { fontSize: 18, marginBottom: 15 },
  h3: { fontSize: 18, marginBottom: 15 },
};

const getArticleById = async (id) => {
  const { data } = await axios.get(
    `https://staging.honestdocs.co/api/app/v2/blog_articles/${id}`,
  );
  const {
    main_title: mainTitle,
    url,
    body,
    title,
    image,
    written_by: writtenBy,
    reviewed_by: reviewedBy,
    man_image: manImage,
    summary,
    references,
  } = data.data.attributes;
  const additionalArticles = data.meta.ads.data.map((item) => {
    const {
      id,
      attributes: { title, description, image },
    } = item;
    return { id, title, description, image };
  });
  const article = {
    id,
    mainTitle,
    url,
    body,
    title,
    image,
    writtenBy,
    reviewedBy,
    manImage,
    summary,
    references,
  };
  return { article, additionalArticles };
};

function useArticle(articleId) {
  return useQuery(['article', articleId], () => getArticleById(articleId), {
    enabled: !!articleId,
  });
}

const Article = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const { uuid: articleId } = route.params;

  const { status, data, error, isFetching } = useArticle(articleId);

  const [modalVisible, setModalVisible] = useState(false);

  const renderDropHeader = (onPress, contentVisible) => {
    return (
      <>
        <Text style={[styles.dropHeaderText]}>Things you should know</Text>
        <TouchableOpacity onPress={onPress}>
          <Text style={[styles.dropHeaderButton]}>
            {contentVisible ? 'Close' : 'Expand'}
          </Text>
        </TouchableOpacity>
      </>
    );
  };

  const renderReferencesDropHeader = (onPress, contentVisible) => {
    return (
      <>
        <TouchableWithoutFeedback onPress={onPress}>
          <View style={styles.dropReferencesHeaderContainer}>
            <Text style={[styles.dropReferencesHeaderText]}>References</Text>
            <Entypo
              name={contentVisible ? 'triangle-up' : 'triangle-down'}
              size={14}
              color={colors.greyText}
            />
          </View>
        </TouchableWithoutFeedback>
      </>
    );
  };

  const handlePush = (uuid) => {
    navigation.push('Article', { uuid });
  };

  let article;
  let adArticle;
  if (status !== 'loading') {
    article = data.article;
    adArticle = data.additionalArticles;
  } else {
    return (
      <View style={styles.loaderContainer}>
        <Text>loading...</Text>
      </View>
    );
  }

  const handlePressButton = () => {
    navigation.push('BlogStackScreen', {
      screen: 'Article',
      params: {
        uuid: '3459da3c-dab6-4ffb-a245-73856a09d93c',
        count: route.params.count ? route.params.count + 1 : 1,
      },
    });
  };
  console.log('screen', route.params.uuid, (route.params.count || 0) + 1);

  return (
    <View style={styles.container}>
      <Button title="test" onPress={handlePressButton} />

      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text>some text</Text>
            <View style={styles.modalCloseContainer}>
              <GestureHandlerTouchableOpacity
                onPress={() => setModalVisible(false)}
              >
                <EvilIcon name="close" size={25} />
              </GestureHandlerTouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <SearchButton />
      <ScrollView>
        <Image
          source={{
            uri:
              'https://im.kommersant.ru/Issues.photo/OGONIOK/2019/019/KMO_148127_01452_1_t218_221732.jpg',
          }}
          width={undefined}
          height={undefined}
          style={styles.image}
        />
        <View style={styles.contentContainer}>
          <Text style={styles.title}>{article.title}</Text>
          <Text style={styles.mainTitle}>{article.mainTitle}</Text>
          <TouchableOpacity
            onPress={() => setModalVisible(true)}
            style={styles.modalButtonContainer}
          >
            <FeatherIcon name="check" size={22} color="#0398fc" />
            <Text style={styles.modalButtonText}>some text</Text>
          </TouchableOpacity>
          <DropDownView renderHeader={renderDropHeader} dropHeaderHeight={18}>
            <View style={styles.dropDownContent}>
              <Text>some text</Text>
              <Text>some text</Text>
              <Text>some text</Text>
              <Text>some text</Text>
            </View>
          </DropDownView>
          <View style={styles.bodyContainer}>
            <HTML source={{ html: article.body }} tagsStyles={tagStyles} />
          </View>
          {false &&
            adArticle.map((item) => {
              return (
                <View key={item.id}>
                  <TouchableWithoutFeedback
                    onPress={() => navigation.navigate('HDmall', {})}
                  >
                    <View style={styles.promotedContainer}>
                      <View style={styles.promotedHeaderContainer}>
                        <View style={styles.promotedHeaderText}>
                          <Text style={styles.promotedText}>
                            Promoted by HonestDocs
                          </Text>
                          <Text style={styles.promotedTitle}>{item.title}</Text>
                          <Text style={styles.promotedDescription}>
                            {item.description}
                          </Text>
                        </View>
                        <Image
                          width={undefined}
                          height={undefined}
                          source={{
                            uri:
                              'https://www.gettyimages.com/gi-resources/images/frontdoor/editorial/Velo/GettyImages-Velo-1088643550.jpg',
                          }}
                          style={styles.promotedImage}
                        />
                      </View>
                      <View style={styles.promotedLinkContainer}>
                        <Text style={styles.promotedLinkText}>See details</Text>
                        <SimpleLineIcons
                          name="share-alt"
                          size={12}
                          color={colors.green}
                        />
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              );
            })}
          <View style={styles.staffContainer}>
            <Image
              source={{
                uri: 'https://staging.honestdocs.co' + article.manImage,
              }}
              width={undefined}
              height={undefined}
              style={styles.stuffImage}
            />
            <View style={styles.staffTextContainer}>
              <Text style={styles.staffLabel}>Written by</Text>
              <Text style={styles.staffName}>{article.writtenBy}</Text>
            </View>
          </View>
          <View style={styles.staffContainer}>
            <Image
              source={{
                uri: 'https://staging.honestdocs.co' + article.manImage,
              }}
              width={undefined}
              height={undefined}
              style={styles.stuffImage}
            />
            <View style={styles.staffTextContainer}>
              <Text style={styles.staffLabel}>Reviewed by</Text>
              <Text style={styles.staffName}>{article.reviewedBy}</Text>
            </View>
          </View>
          <View style={styles.referencesContainer}>
            <DropDownView
              renderHeader={renderReferencesDropHeader}
              dropHeaderHeight={14}
            >
              <View style={styles.dropDownContent}>
                {article.references.map((item, index) => (
                  <Text key={index} style={styles.referencesText}>
                    {item}
                  </Text>
                ))}
              </View>
            </DropDownView>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Article;
