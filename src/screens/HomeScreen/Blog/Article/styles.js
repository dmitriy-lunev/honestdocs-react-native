import { StyleSheet, Dimensions } from 'react-native';
import colors from '../../../../theme/colors';

const { width } = Dimensions.get('window');
const ratio = 544 / 968;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  image: {
    width,
    height: width * ratio,
  },
  modalBackGround: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(1, 1, 1, 0.3)',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    padding: 15,
    width: width - 30,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 8,

    height: 500,
  },
  modalCloseContainer: {
    position: 'absolute',
    right: 15,
    top: 15,
  },
  modalButtonContainer: {
    marginVertical: 15,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    width: width / 2,
    height: 35,
    borderWidth: 1,
    borderColor: colors.bluewater,
    borderRadius: 15,
  },
  modalButtonText: {
    marginLeft: 5,
    color: colors.bluewater,
    fontSize: 14,
    fontWeight: 'bold',
  },
  contentContainer: {
    marginHorizontal: 15,
    marginTop: 40,
  },
  title: {
    color: colors.green,
  },
  mainTitle: {
    marginTop: 20,
    fontSize: 20,
    lineHeight: 35,
    fontWeight: 'bold',
  },
  dropDownContent: {
    paddingHorizontal: 15,
    paddingBottom: 15,
  },
  bodyContainer: {
    marginTop: 30,
  },
  promotedContainer: {
    marginTop: 30,
    marginBottom: 10,
    paddingVertical: 20,
    borderColor: 'grey',
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  promotedHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  promotedHeaderText: {
    flex: 1,
    paddingRight: 15,
  },
  promotedText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'grey',
  },
  promotedTitle: {
    marginTop: 10,
    fontSize: 17,
  },
  promotedDescription: {
    marginTop: 15,
  },
  promotedImage: {
    width: 105,
    height: 105,
    borderRadius: 5,
  },
  promotedLinkContainer: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  promotedLinkText: {
    fontSize: 13,
    fontWeight: 'bold',
    color: colors.green,
    marginRight: 5,
  },
  staffContainer: {
    marginTop: 10,
    flexDirection: 'row',
  },
  stuffImage: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  staffTextContainer: {
    flex: 1,
    justifyContent: 'space-around',
  },
  staffLabel: {
    fontSize: 14,
    color: colors.textGreyColor,
  },
  staffName: {
    fontSize: 17,
  },
  dropHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
  },
  dropHeaderText: {
    fontSize: 18,
    lineHeight: 18,
    fontWeight: 'bold',
  },
  dropHeaderButton: {
    color: colors.green,
    fontSize: 18,
    lineHeight: 18,
  },
  dropReferencesHeaderContainer: {
    flexDirection: 'row',
  },
  dropReferencesHeaderText: {
    color: colors.greyText,
  },
  referencesContainer: {
    marginVertical: 20,
  },
  referencesText: {
    color: colors.greyText,
    marginBottom: 10,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
  },
});

export default styles;
