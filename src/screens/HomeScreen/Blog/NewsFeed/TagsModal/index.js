import { array, func, object } from 'prop-types';
import React, { useState } from 'react';
import { Text, View } from 'react-native';
import {
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { setFollowingTagsId } from '../../../../../redux/modules/Home/Blog/actions';
import styles from './styles';
import colors from '../../../../../theme/colors';
import SwipeableModal from '../../../../../components/SwipeableModal';

const TagsModal = ({
  visible,
  setVisible,
  followingTagsId,
  articlesTags,
  setFollowingTagsId,
}) => {
  const [editMode, setEditMode] = useState(false);

  const otherTags = Object.keys(articlesTags).filter((item) => {
    return !followingTagsId.includes(item);
  });

  const [_followingTagsId, _setFollowingTagsId] = useState(followingTagsId);
  const [_otherTags, _setOtherTags] = useState(otherTags);

  const handleEditButton = () => {
    if (editMode) {
      setFollowingTagsId(_followingTagsId);
    }
    setEditMode(!editMode);
  };

  const handleDeleteTag = (item) => {
    if (editMode && _followingTagsId.length > 3) {
      _setFollowingTagsId(
        _followingTagsId.filter((x) => {
          return !(x === item);
        }),
      );
      _setOtherTags([..._otherTags, item]);
    }
  };

  const handleAddTag = (item) => {
    if (editMode) {
      _setOtherTags(
        _otherTags.filter((x) => {
          return !(x === item);
        }),
      );
      _setFollowingTagsId([..._followingTagsId, item]);
    }
  };

  return (
    <SwipeableModal visible={visible} setVisible={setVisible}>
      <View style={styles.container}>
        <View style={styles.dropDownIcon} />
        <View style={styles.modalContainer}>
          <View style={styles.modalHeader}>
            <Text style={styles.headerText}>Topics I'm following</Text>
            <TouchableOpacity onPress={handleEditButton}>
              {editMode ? (
                <Text style={styles.editButtonText}>Save</Text>
              ) : (
                <View style={styles.editButtonContainer}>
                  <MaterialIcons name="edit" color={colors.green} size={18} />
                  <Text style={styles.editButtonText}>Edit</Text>
                </View>
              )}
            </TouchableOpacity>
          </View>
          <ScrollView>
            <View style={styles.tagsContainer}>
              {_followingTagsId.map((item) => {
                return (
                  <TouchableWithoutFeedback
                    onPress={() => handleDeleteTag(item)}
                    style={[styles.tagItem, styles.followingTag]}
                    key={item}
                  >
                    <Text style={styles.followingTagText}>
                      {articlesTags[item]}
                    </Text>
                    {editMode && (
                      <View style={styles.closeButton}>
                        <Icon
                          color="rgba(0,0,0,0.3)"
                          name="close-circle"
                          size={25}
                        />
                      </View>
                    )}
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
            <View style={styles.otherTagTextContainer}>
              <Text style={styles.headerText}>Recommended topics</Text>
              <Text style={styles.modalText}>Tap to follow topic</Text>
            </View>
            <View style={styles.tagsContainer}>
              {_otherTags.map((item) => {
                return (
                  <TouchableWithoutFeedback
                    onPress={() => handleAddTag(item)}
                    style={[styles.tagItem, styles.otherTagContainer]}
                    key={item}
                  >
                    <Text>{articlesTags[item]}</Text>
                  </TouchableWithoutFeedback>
                );
              })}
            </View>
          </ScrollView>
        </View>
      </View>
    </SwipeableModal>
  );
};

const mapStateToProps = (state) => {
  return {
    followingTagsId: state.home.blog.followingTagsId,
    articlesTags: state.home.blog.articlesTags,
  };
};

const mapDispatchToProps = {
  setFollowingTagsId,
};

export default connect(mapStateToProps, mapDispatchToProps)(TagsModal);

TagsModal.propTypes = {
  followingTagsId: array,
  articlesTags: object,
  setFollowingTagsId: func,
};
