import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../../../../theme/colors';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  dropDownIcon: {
    marginTop: 50,
    height: 5,
    width: 50,
    borderRadius: 5,
    backgroundColor: 'white',
  },
  modalContainer: {
    flex: 1,
    width,
    marginTop: 10,
    paddingHorizontal: 10,
    paddingTop: 15,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
  },
  modalHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 5,
    paddingBottom: 20,
    paddingHorizontal: 10,
  },
  headerText: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  tagsContainer: {
    marginVertical: 8,
    flexDirection: 'row',
    width: '100%',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  tagItem: {
    marginBottom: 10,
    height: 100,
    width: (width - 40) / 2 - 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  followingTag: {
    backgroundColor: 'rgba(66, 135, 245, 0.3)',
  },
  followingTagText: {
    fontWeight: 'bold',
    color: 'rgba(66, 135, 245, 1)',
  },
  otherTagContainer: {
    borderWidth: 2,
    borderColor: 'rgba(0,0,0,0.3)',
  },
  otherTagText: {
    fontWeight: 'bold',
  },
  otherTagTextContainer: {
    paddingHorizontal: 10,
    marginTop: 40,
    marginBottom: 25,
  },
  modalText: {
    color: 'rgba(0,0,0,0.6)',
  },
  closeButton: {
    position: 'absolute',
    right: -8,
    top: -8,
  },
  editButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  editButtonText: {
    paddingLeft: 2,
    color: colors.green,
    fontWeight: 'bold',
    fontSize: 16,
  },
});

export default styles;
