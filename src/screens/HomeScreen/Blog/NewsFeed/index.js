import React, { useCallback, useEffect, useRef, useState } from 'react';
import { RefreshControl, View } from 'react-native';
import { array, bool, func, number, object } from 'prop-types';

import ArticleList from '../../components/ArticlesList';
import { connect } from 'react-redux';
import {
  clearBlogArticles,
  loadBlogArticlesByTagFetch,
  loadBlogArticlesFetch,
} from '../../../../redux/modules/Home/Blog/actions';
import { wait } from '../../../../helpers/wait';
import NewsFeedSlider from '../NewsFeedSlider';
import { SearchButton } from '../../../../components';
import TagsFilter from './TagsFilter';
import { useRoute } from '@react-navigation/native';
import {
  cancelGetBlogArticles,
  cancelGetBlogArticlesByTag,
} from '../../../../api/home-api';
import TagsModal from './TagsModal';

const NewsFeed = ({
  loading,
  loaded,
  allLoaded,
  blogArticles,
  page,
  promoted,
  clearBlogArticles,
  loadBlogArticles,
  loadBlogArticlesByTag,
  navigation,
}) => {
  const [visible, setVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = useCallback(() => {
    setRefreshing(true);

    // dispatch(loadBannerFetch());
    // dispatch(loadShopCategoriesFetch());
    // dispatch(loadHomepageHospitalsFetch());
    // dispatch(loadHomepageDoctorQAFetch());

    wait(2000).then(() => setRefreshing(false));
  }, []);

  const route = useRoute();
  const currentTag = route?.params?.currentTag || undefined;

  useEffect(() => {
    if (currentTag && cancelGetBlogArticlesByTag) {
      cancelGetBlogArticlesByTag();
    } else if (cancelGetBlogArticles && cancelGetBlogArticlesByTag) {
      cancelGetBlogArticles();
      cancelGetBlogArticlesByTag();
    }
    clearBlogArticles();
    if (currentTag) {
      loadBlogArticlesByTag(currentTag);
    } else {
      loadBlogArticles();
    }
  }, [currentTag]);

  const loadMore = () => {
    if (currentTag) {
      loadBlogArticlesByTag(currentTag);
    } else {
      loadBlogArticles();
    }
  };

  return (
    <>
      <SearchButton />
      <TagsFilter visible={visible} setVisible={setVisible} />
      <ArticleList
        navigation={navigation}
        header={currentTag ? null : <NewsFeedSlider />}
        {...{ loading, loaded, allLoaded, blogArticles, promoted, loadMore }}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            color="rgb(8, 130, 81)"
          />
        }
      />
      {visible && <TagsModal visible={visible} setVisible={setVisible} />}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    loading: state.home.blog.loading,
    loaded: state.home.blog.loaded,
    allLoaded: state.home.blog.allLoaded,
    blogArticles: state.home.blog.blogArticles,
    page: state.home.blog.page,
    promoted: state.home.blog.promoted,
  };
};

const mapDispatchToProps = {
  clearBlogArticles: clearBlogArticles,
  loadBlogArticles: loadBlogArticlesFetch,
  loadBlogArticlesByTag: loadBlogArticlesByTagFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeed);

NewsFeed.propTypes = {
  loading: bool,
  loaded: bool,
  allLoaded: bool,
  blogArticles: array,
  page: number,
  promoted: object,
  clearBlogArticles: func,
  loadBlogArticles: func,
  loadBlogArticlesByTag: func,
  navigation: object,
};
