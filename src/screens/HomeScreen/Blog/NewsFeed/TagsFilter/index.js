import { array, bool, func, object } from 'prop-types';
import React, { useEffect } from 'react';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { loadBlogArticlesTagsFetch } from '../../../../../redux/modules/Home/Blog/actions';

import styles from './styles';
import colors from '../../../../../theme/colors';

const TagsFilter = ({
  visible,
  setVisible,
  articlesTags,
  followingTagsId,
  loadBlogArticlesTags,
}) => {
  const navigation = useNavigation();
  const route = useRoute();
  const currentTag = route?.params?.currentTag || null;

  useEffect(() => {
    loadBlogArticlesTags();
    return () => {};
  }, []);

  const onPressHandler = (currentTag) => {
    navigation.navigate('BlogStackScreen', {
      screen: 'NewsFeed',
      params: { currentTag },
    });
  };

  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={true}
        bounces={false}
        showsHorizontalScrollIndicator={false}
      >
        <TouchableOpacity
          style={[styles.item, currentTag ? '' : styles.currentItem]}
          onPress={() => onPressHandler(null)}
        >
          <Text style={styles.itemText}>Popular</Text>
        </TouchableOpacity>
        {followingTagsId.map((item) => {
          return (
            <TouchableOpacity
              key={item}
              style={[
                styles.item,
                currentTag === item ? styles.currentItem : '',
              ]}
              onPress={() => onPressHandler(item)}
            >
              <Text style={styles.itemText}>{articlesTags[item]}</Text>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
      <TouchableOpacity
        style={styles.menuButton}
        onPress={() => setVisible(!visible)}
      >
        <Icon name="menu" size={26} color={colors.superGrey} />
      </TouchableOpacity>
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    loaded: state.home.blog.loaded,
    loading: state.home.blog.loading,
    articlesTags: state.home.blog.articlesTags,
    followingTagsId: state.home.blog.followingTagsId,
  };
};

const mapDispatchToProps = {
  loadBlogArticlesTags: loadBlogArticlesTagsFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(TagsFilter);

TagsFilter.propTypes = {
  loaded: bool,
  loading: bool,
  articlesTags: object,
  followingTagsId: array,
  loadBlogArticlesTags: func,
};
