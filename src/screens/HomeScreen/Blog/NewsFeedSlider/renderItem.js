import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

import { navigate } from '../../../../services/navigation';

export const renderItem = (item) => {
  return (
    <TouchableWithoutFeedback
      style={styles.container}
      onPress={() =>
        navigate('BlogStackScreen', {
          screen: 'Article',
          params: { uuid: item.id.toString() },
        })
      }
    >
      <ImageBackground
        source={{
          uri: item.image
            ? `https://staging.honestdocs.co/${item.image}`
            : 'https://naurok-test.nyc3.cdn.digitaloceanspaces.com/uploads/test/510013/91297/676499_1585497084.jpg',
        }}
        style={styles.backgroundImage}
      >
        <View style={styles.content}>
          <Text numberOfLines={2} ellipsizeMode="middle" style={styles.name}>
            {item.title}
          </Text>
        </View>
      </ImageBackground>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
  },
  content: {
    marginTop: 100,
    paddingBottom: 30,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    height: 90,
  },
  name: {
    paddingTop: 15,
    fontSize: 17,
    fontWeight: 'bold',
  },
});
