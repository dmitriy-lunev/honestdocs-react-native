import React, { useEffect } from 'react';
import { Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { array, bool, func } from 'prop-types';

import { loadTopArticlesFetch } from '../../../../redux/modules/Home/Blog/actions';
import { renderItem } from './renderItem';
import { Slider } from '../../../../components';

const { width } = Dimensions.get('window');

const NewsFeedSlider = ({ topArticles, loadTopArticles, ...rest }) => {
  useEffect(() => {
    loadTopArticles();
    return () => {};
  }, []);

  return (
    <Slider
      title="News Feed"
      path="BlogStackScreen"
      data={topArticles}
      itemWidth={width - 50}
      renderItem={renderItem}
      loop={true}
      {...rest}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    topArticles: state.home.blog.topArticles,
  };
};

const mapDispatchToProps = {
  loadTopArticles: loadTopArticlesFetch,
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeedSlider);

NewsFeedSlider.propTypes = {
  topArticles: array,
  loadTopArticles: func,
  withLink: bool,
};
NewsFeedSlider.defaultProps = {
  withLink: false,
};
