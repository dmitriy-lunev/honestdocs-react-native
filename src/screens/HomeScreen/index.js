export { default as Homepage } from './Homepage';
export { default as Hospitals } from './Hospitals';
export { default as Article } from './Blog/Article';
export { default as NewsFeed } from './Blog/NewsFeed';
