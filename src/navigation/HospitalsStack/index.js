import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Hospitals } from '../../screens/HomeScreen';
import Hospital from '../../screens/HomeScreen/Hospitals/Hospital';

const HospitalsStack = createStackNavigator();

const HospitalsStackScreen = ({ navigation }) => {
  return (
    <HospitalsStack.Navigator
      initialRouteName="Hospitals"
      screenOptions={{ headerShown: false }}
    >
      <HospitalsStack.Screen name="Hospitals" component={Hospitals} />
      <HospitalsStack.Screen name="Hospital" component={Hospital} />
    </HospitalsStack.Navigator>
  );
};

export default HospitalsStackScreen;
