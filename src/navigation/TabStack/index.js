import React from 'react';
import { Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeStackScreen from '../HomeStack';
import colors from '../../theme/colors';
import DoctorQAStackScreen from '../DoctorQAStack';
import DrugsStackScreen from '../DrugsStack';
import ConditionsStackScreen from '../ConditionsStack';

const TabsNavigator = createBottomTabNavigator();

const Component = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>It's tab screen!</Text>
    </View>
  );
};

const tabIcons = {
  HomeStackScreen: 'home',
  DoctorQAStackScreen: 'forum',
  HDmall: 'cart',
  DrugsStackScreen: 'pill',
  ConditionsStackScreen: 'heart',
};

const TabsStack = () => {
  return (
    <TabsNavigator.Navigator
      tabBarOptions={{
        activeTintColor: colors.green,
        inactiveTintColor: colors.greyText,
        labelStyle: {
          fontSize: 13,
        },
      }}
      screenOptions={({ route: { name } }) => ({
        tabBarIcon: ({ color, size }) => {
          return <Icon name={tabIcons[name]} color={color} size={size} />;
        },
        tabBarVisible: true,
      })}
      initialRouteName="HomeStackScreen"
    >
      <TabsNavigator.Screen
        name="HomeStackScreen"
        component={HomeStackScreen}
        options={{
          title: 'Home',
        }}
      />
      <TabsNavigator.Screen
        name="DoctorQAStackScreen"
        component={DoctorQAStackScreen}
        options={{
          title: 'Doctor Q&A',
        }}
      />
      <TabsNavigator.Screen
        name="HDmall"
        component={Component}
        options={{
          title: 'HDmall',
        }}
      />
      <TabsNavigator.Screen
        name="DrugsStackScreen"
        component={DrugsStackScreen}
        options={{
          title: 'Drugs',
        }}
      />
      <TabsNavigator.Screen
        name="ConditionsStackScreen"
        component={ConditionsStackScreen}
        options={{
          title: 'Conditions',
        }}
      />
    </TabsNavigator.Navigator>
  );
};

export default TabsStack;
