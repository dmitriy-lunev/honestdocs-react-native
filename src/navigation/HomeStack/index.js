import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Homepage } from '../../screens/HomeScreen';
import HospitalsStackScreen from '../HospitalsStack';
import BlogStackScreen from '../BlogStack';

const HomeStack = createStackNavigator();

const HomeStackScreen = ({ navigation }) => {
  return (
    <HomeStack.Navigator
      initialRouteName="Home"
      screenOptions={{ headerShown: false }}
    >
      <HomeStack.Screen name="Home" component={Homepage} />
      <HomeStack.Screen name="BlogStackScreen" component={BlogStackScreen} />
      <HomeStack.Screen
        name="HospitalsStackScreen"
        component={HospitalsStackScreen}
      />
    </HomeStack.Navigator>
  );
};

export default HomeStackScreen;
