import React from 'react';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import { QueryClient, QueryClientProvider } from 'react-query';
import { useSelector } from 'react-redux';
import TabsStack from '../TabStack';
import SearchStack from '../SearchStack';
import MiniAppsStack from '../MiniAppsStack';
import SingInStack from '../SingInStack';
import ProfileStack from '../ProfileStack';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/EvilIcons';
import colors from '../../theme/colors';
import { navigate, navigationRef } from '../../services/navigation';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import InformationMessage from '../../components/InformationMessage';

const RootStack = createStackNavigator();

const queryClient = new QueryClient();

const Root = () => {
  const isSignedIn = useSelector((state) => state.authentication.isSignedIn);

  const renderBackButton = (props) => {
    const tabs = [
      'Home',
      'DoctorQuestions',
      'HDmall',
      'Drugs',
      'Conditions',
      'ProfileStack',
      'SendOtp',
    ];
    const currentRoute = navigationRef.current?.getCurrentRoute();
    if (
      navigationRef.current?.canGoBack() &&
      !tabs.includes(currentRoute.name)
    ) {
      return (
        <HeaderBackButton
          {...props}
          label="Back"
          onPress={() => {
            navigationRef.current.goBack();
          }}
        />
      );
    }
  };

  return (
    <SafeAreaProvider>
      <QueryClientProvider client={queryClient}>
        <RootStack.Navigator
          initialRouteName="TabsStack"
          headerMode="screen"
          screenOptions={({ route }) => ({
            headerLeft: renderBackButton,
            headerRight: () => (
              <TouchableOpacity
                onPress={() => navigate('ProfileStack')}
                activeOpacity={0.7}
                style={{ marginRight: 15 }}
              >
                <Icon
                  name="user"
                  size={35}
                  color={
                    route.name === 'ProfileStack' ? colors.green : colors.black
                  }
                />
              </TouchableOpacity>
            ),
          })}
        >
          <RootStack.Screen name="TabsStack" component={TabsStack} />
          <RootStack.Screen name="SearchStack" component={SearchStack} />
          {isSignedIn ? (
            <>
              <RootStack.Screen
                name="MiniAppsStack"
                component={MiniAppsStack}
              />
              <RootStack.Screen name="ProfileStack" component={ProfileStack} />
            </>
          ) : (
            <>
              <RootStack.Screen
                name="SingInStack"
                options={{ headerShown: false }}
                component={SingInStack}
              />
              <RootStack.Screen
                name="MiniAppsStack"
                options={{ headerShown: false }}
                component={SingInStack}
              />
              <RootStack.Screen
                name="ProfileStack"
                options={{ headerShown: false }}
                component={SingInStack}
              />
            </>
          )}
        </RootStack.Navigator>
        <InformationMessage />
      </QueryClientProvider>
    </SafeAreaProvider>
  );
};

export default Root;
