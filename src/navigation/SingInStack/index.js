import React from 'react';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import SendOtp from '../../screens/SingInScreen/SendOtp';
import CheckOtp from '../../screens/SingInScreen/CheckOtp';

const SingInStack = createStackNavigator();

const SingInStackScreen = () => {
  return (
    <SingInStack.Navigator
      screenOptions={{
        headerLeft: (props) => <HeaderBackButton {...props} label="Back" />,
      }}
    >
      <SingInStack.Screen name="SendOtp" component={SendOtp} />
      <SingInStack.Screen name="CheckOtp" component={CheckOtp} />
    </SingInStack.Navigator>
  );
};

export default SingInStackScreen;
