import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import ProfileMenu from '../../screens/ProfileScreen/ProfileMenu';
import { Text, View } from 'react-native';

const Component = () => (
  <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
    <Text>Profile Section!</Text>
  </View>
);

const ProfileStack = createStackNavigator();

const ProfileStackScreen = () => {
  return (
    <ProfileStack.Navigator
      initialRouteName="ProfileMenu"
      screenOptions={{ headerShown: false }}
    >
      <ProfileStack.Screen name="ProfileMenu" component={ProfileMenu} />
      <ProfileStack.Screen name="Profile" component={Component} />
      <ProfileStack.Screen name="MedicalRecord" component={Component} />
      <ProfileStack.Screen name="HealthStats" component={Component} />
      <ProfileStack.Screen name="HealthCoins" component={Component} />
      <ProfileStack.Screen name="Questions" component={Component} />
      <ProfileStack.Screen name="Answers" component={Component} />
      <ProfileStack.Screen name="Bookmarks" component={Component} />
      <ProfileStack.Screen name="ProfileSettings" component={Component} />
      <ProfileStack.Screen name="OurPolicies" component={Component} />
    </ProfileStack.Navigator>
  );
};

export default ProfileStackScreen;
