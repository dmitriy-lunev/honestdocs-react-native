import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  Condition,
  ConditionsByCategory,
  MostSearchedConditions,
} from '../../screens/ConditionsScreen';

const ConditionsStack = createStackNavigator();

const ConditionsStackScreen = () => {
  return (
    <ConditionsStack.Navigator headerMode="none">
      <ConditionsStack.Screen
        name="MostSearchedConditions"
        component={MostSearchedConditions}
        options={{
          title: 'Most Searched Conditions',
        }}
      />
      <ConditionsStack.Screen
        name="ConditionsByCategory"
        component={ConditionsByCategory}
        options={{
          title: 'Conditions By Category',
        }}
      />
      <ConditionsStack.Screen
        name="Condition"
        component={Condition}
        options={{
          title: 'Condition',
        }}
      />
    </ConditionsStack.Navigator>
  );
};

export default ConditionsStackScreen;
