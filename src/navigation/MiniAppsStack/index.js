import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Text, View } from 'react-native';
import { useSelector } from 'react-redux';

const MiniAppsStack = createStackNavigator();

const Component = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Mini Apps</Text>
    </View>
  );
};

const MiniAppsStackScreen = () => {
  return (
    <MiniAppsStack.Navigator screenOptions={{ headerShown: false }}>
      <MiniAppsStack.Screen name="MiniApps" component={Component} />
    </MiniAppsStack.Navigator>
  );
};

export default MiniAppsStackScreen;
