import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Article, NewsFeed } from '../../screens/HomeScreen';
import TagsModal from '../../screens/HomeScreen/Blog/NewsFeed/TagsModal';

const BlogStack = createStackNavigator();

const BlogStackScreen = ({ navigation }) => {
  return (
    <BlogStack.Navigator
      screenOptions={{
        cardStyle: { backgroundColor: 'transparent' },
        animationEnabled: false,
        headerShown: false,
      }}
      initialRouteName="NewsFeed"
      mode="modal"
    >
      <BlogStack.Screen name="NewsFeed" component={NewsFeed} />
      <BlogStack.Screen name="Article" component={Article} />
      <BlogStack.Screen
        options={{
          cardStyle: { backgroundColor: 'rgba(0,0,0,0.3)' },
          headerShown: false,
          animationEnabled: true,
        }}
        name="Modal"
        component={TagsModal}
      />
    </BlogStack.Navigator>
  );
};

export default BlogStackScreen;
