// import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { useReduxDevToolsExtension } from '@react-navigation/devtools';

import Root from './RootStack';
import { isReadyRef, navigationRef } from '../services/navigation';

const config = {
  screens: {
    RootStack: {
      initialRouteName: 'TabsStack',
      screens: {
        TabsStack: {
          initialRouteName: 'HomeStackScreen',
          screens: {
            HomeStackScreen: {
              initialRouteName: 'Home',
              screens: {
                Home: {
                  path: 'home',
                },
                Newsfeed: {
                  path: 'newsfeed',
                },
                HospitalsStackScreen: {
                  initialRouteName: 'Hospitals',
                  screen: {
                    Hospitals: {
                      path: 'hospitals',
                    },
                    Hospital: {
                      path: 'hospital/:uuid',
                    },
                  },
                },
              },
            },
            DoctorQA: {
              path: 'doctorqa',
            },
            HDmall: {
              path: 'hdmall',
            },
            Drugs: {
              path: 'drugs',
            },
            Conditions: {
              path: 'conditions',
            },
          },
        },
        SearchStack: {
          initialRouteName: 'Search',
          screen: {
            Search: {
              path: 'search',
            },
          },
        },
        MiniAppsStack: {
          initialRouteName: 'MiniApps',
          screen: {
            MiniApps: {
              path: 'miniapps',
            },
          },
        },
      },
    },
  },
};

const linking = {
  prefixes: [],
  config,
};

const AppNavigation = () => {
  useReduxDevToolsExtension(navigationRef);
  return (
    <SafeAreaProvider>
      <NavigationContainer
        // linking={linking}
        ref={navigationRef}
        onReady={() => {
          isReadyRef.current = true;
        }}
      >
        <Root />
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default AppNavigation;
