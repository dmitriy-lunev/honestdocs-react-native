import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  DoctorQuestion,
  DoctorQuestions,
} from '../../screens/DoctorQuestionsScreen';

const DoctorQAStack = createStackNavigator();

const DoctorQAStackScreen = () => {
  return (
    <DoctorQAStack.Navigator headerMode="none">
      <DoctorQAStack.Screen
        name="DoctorQuestions"
        component={DoctorQuestions}
        options={{
          title: 'Doctor Q&A',
        }}
      />
      <DoctorQAStack.Screen
        name="DoctorQuestion"
        component={DoctorQuestion}
        options={{
          title: 'Doctor Q&A',
        }}
      />
    </DoctorQAStack.Navigator>
  );
};

export default DoctorQAStackScreen;
