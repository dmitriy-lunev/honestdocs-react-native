import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  MostSearchedDrugs,
  DrugsByCategory,
  Drug,
} from '../../screens/DrugsScreen';

const DrugsStack = createStackNavigator();

const DrugsStackScreen = () => {
  return (
    <DrugsStack.Navigator headerMode="none">
      <DrugsStack.Screen
        name="MostSearchedDrugs"
        component={MostSearchedDrugs}
        options={{
          title: 'Most Searched Drugs',
        }}
      />
      <DrugsStack.Screen
        name="DrugsByCategory"
        component={DrugsByCategory}
        options={{
          title: 'Drugs By Category',
        }}
      />
      <DrugsStack.Screen
        name="Drug"
        component={Drug}
        options={{
          title: 'Drug',
        }}
      />
    </DrugsStack.Navigator>
  );
};

export default DrugsStackScreen;
