export default {
  auth_bg: require('./images/auth_bg.png'),
  dotted_line: require('./images/dotted_line.png'),
  // no
  cta: require('./images/cta.png'),
  logo_rounded: require('./images/logo_rounded.png'),
  doctor_avatar: require('./images/doctor_avatar.png'),
  health_tools: require('./images/health_tools.png'),
  male: require('./images/male.png'),
  male_active: require('./images/male_active.png'),
  female: require('./images/female.png'),
  female_active: require('./images/female_active.png'),
  other_active: require('./images/other_active.png'),
  other: require('./images/other.png'),
  period_tracker_help_id: require('./images/period_tracker_help_id.png'),
  period_tracker_help_th: require('./images/period_tracker_help_th.png'),
  installment_partners: require('./images/installment_partners.png'),

  doctor: require('./images/doctor.png'),
  hospital: require('./images/hospital.png'),
  news_feed: require('./images/news_feed.png'),
};
