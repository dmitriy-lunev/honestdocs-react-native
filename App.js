import React from 'react';
import { Provider } from 'react-redux';
import { NativeModules, View } from 'react-native';

import { store } from './src/redux/store';
import AppNavigation from './src/navigation';
import InformationMessage from './src/components/InformationMessage';

// if (__DEV__) {
//   NativeModules.DevSettings.setIsDebuggingRemotely(true);
// }

const App = () => {
  return (
    <Provider store={store}>
      <AppNavigation />
    </Provider>
  );
};

export default App;
